angular.module('bcApp').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('html/cms/campagne.html',
    "<div class=\"menu-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container menu\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"cms-menu\">\r" +
    "\n" +
    "                        <ul>\r" +
    "\n" +
    "                            <li ng-click=\"selectView('onderzoek')\" ng-class=\"{'selected': selectedView == 'onderzoek'}\">\r" +
    "\n" +
    "                                <div class=\"cms-menu-button\"><p>Onderzoek</p></div>\r" +
    "\n" +
    "                            </li>\r" +
    "\n" +
    "                            <li ng-click=\"selectView('thema')\" ng-class=\"{'selected': selectedView == 'thema'}\">\r" +
    "\n" +
    "                                <div class=\"cms-menu-button\"><p>Vragen</p></div>\r" +
    "\n" +
    "                            </li>\r" +
    "\n" +
    "                            <li ng-click=\"selectView('resultaten')\"\r" +
    "\n" +
    "                                    ng-class=\"{'selected': selectedView == 'resultaten'}\">\r" +
    "\n" +
    "                                <div class=\"cms-menu-button\"><p>Resultaten</p></div>\r" +
    "\n" +
    "                            </li>\r" +
    "\n" +
    "                        </ul>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/campaign.html',
    "<div class=\"menu-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container menu\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div ng-show=\"!campaign\" class=\"row\">\r" +
    "\n" +
    "                    <p>Campagnes bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <ul>\r" +
    "\n" +
    "                        <li ng-repeat=\"campaign in campaigns\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"icon-container\">\r" +
    "\n" +
    "                                <div icon-font icon-class=\"icon-edit\" ng-click=\"editCampaign($index)\" class=\"icon\" title=\"Campagne bewerken\"></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"cms-button\" ng-click=\"selectCampaign($index)\" title=\"Campagne selecteren\"><p>{{ campaign.name }}</p></div>\r" +
    "\n" +
    "                        </li>\r" +
    "\n" +
    "                    </ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"newCampaign()\"><span>Campagne toevoegen</span></button>\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"campaign\" class=\"row\">\r" +
    "\n" +
    "                    <p ng-show=\"campaign.id && campaign.id != ''\">Campagne bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <p ng-show=\"!campaign.id\">Campagne toevoegen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <form name=\"campaign-form\" action=\"\">\r" +
    "\n" +
    "                        <fieldset>\r" +
    "\n" +
    "                            <label>\r" +
    "\n" +
    "                                <input ng-model=\"campaign.active\" type=\"radio\" value=\"1\"/>Actief\r" +
    "\n" +
    "                            </label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"name\" placeHolder=\"naam\"\r" +
    "\n" +
    "                                    ng-model=\"campaign.name\"/>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"save()\"><span>Opslaan</span></button>\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"cancel()\"><span>Terug</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/login.html',
    "<div class=\"login-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container login\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <p>Inloggen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <form name=\"login-form\" action=\"\">\r" +
    "\n" +
    "                        <fieldset>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"username\" placeHolder=\"username\"\r" +
    "\n" +
    "                                    ng-model=\"user.username\">\r" +
    "\n" +
    "                            <input type=\"password\" class=\"form-control\" name=\"password\" placeHolder=\"password\"\r" +
    "\n" +
    "                                    ng-model=\"user.password\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"login()\"><span>Inloggen</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div ng-show=\"user.id > 0\" class=\"row\">\r" +
    "\n" +
    "                    <h2>{{ user.name }}</h2>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/menu.html',
    "<div class=\"menu-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container menu\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <p>Kies een optie</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"menu-container\">\r" +
    "\n" +
    "                        <ul>\r" +
    "\n" +
    "                            <li ng-click=\"selectMenu('thema')\"><p title=\"Campagne instellen\">Vragen instellen</p></li>\r" +
    "\n" +
    "                            <li ng-click=\"selectMenu('resultaat')\"><p title=\"Resultaten\">Resultaten</p></li>\r" +
    "\n" +
    "                        </ul>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/cms/overview.html',
    "<div class=\"statement-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <p>Theme:</p>\r" +
    "\n" +
    "        <a href=\"#!/thema\">Terug</a>\r" +
    "\n" +
    "        <a href=\"#!/delen\">Delen</a>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/question.html',
    "<div class=\"statement-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container statement\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"!statement\" class=\"row\">\r" +
    "\n" +
    "                    <h1>Vragen voor het thema: {{ theme.name }}</h1>\r" +
    "\n" +
    "                    <ul>\r" +
    "\n" +
    "                        <li ng-repeat=\"statement in statements\">\r" +
    "\n" +
    "                            <div class=\"icon-container\">\r" +
    "\n" +
    "                                <div icon-font icon-class=\"icon-edit\" ng-click=\"editStatement($index)\" class=\"icon\" title=\"Vraag bewerken\"></div>\r" +
    "\n" +
    "                                <div icon-font icon-class=\"icon-delete\" ng-click=\"deleteStatement($index)\" class=\"icon\" title=\"Vraag verwijderen\"></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"statement-button\"><p>{{ statement.text }}</p></div>\r" +
    "\n" +
    "                        </li>\r" +
    "\n" +
    "                    </ul>\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"newStatement()\"><span>Vraag toevoegen</span></button>\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"statement\" class=\"row\">\r" +
    "\n" +
    "                    <p ng-show=\"statement.id && statement.id != ''\">Vraag bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <p ng-show=\"!statement.id\">Vraag toevoegen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <form name=\"statement-form\" action=\"\">\r" +
    "\n" +
    "                        <fieldset>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <label>Type:</label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <label>\r" +
    "\n" +
    "                                    <input ng-model=\"statement.type\" type=\"radio\" value=\"statement\"/>Statement\r" +
    "\n" +
    "                                </label>\r" +
    "\n" +
    "                                <label>\r" +
    "\n" +
    "                                    <input ng-model=\"statement.type\" type=\"radio\" value=\"multiple\"/>Multiple choice\r" +
    "\n" +
    "                                </label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <label>Vraag:</label>\r" +
    "\n" +
    "                            <input id=\"name\" type=\"text\" class=\"form-control\" name=\"name\" ng-model=\"statement.text\"\r" +
    "\n" +
    "                                    placeHolder=\"Stel een vraag\"/>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <label for=\"info\">Uitleg:</label>\r" +
    "\n" +
    "                            <textarea id=\"info\" type=\"text\" class=\"form-control\" name=\"informatie\" ng-model=\"statement.info\"\r" +
    "\n" +
    "                                    placeHolder=\"Geef uitleg over het antwoord\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <label>Antwoorden:</label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div ng-switch=\"statement.type\">\r" +
    "\n" +
    "                                <div ng-switch-when=\"statement\">\r" +
    "\n" +
    "                                    <div class=\"statement-radio\">\r" +
    "\n" +
    "                                        <label>\r" +
    "\n" +
    "                                            <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"0\"/>Goed\r" +
    "\n" +
    "                                        </label>\r" +
    "\n" +
    "                                        <label>\r" +
    "\n" +
    "                                            <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"1\"/>Fout\r" +
    "\n" +
    "                                        </label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div ng-switch-when=\"multiple\">\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"0\"/>\r" +
    "\n" +
    "                                        <input id=\"answer1\" type=\"text\" class=\"form-control\" name=\"answer1\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 1\" ng-model=\"statement.choices[0]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"1\"/>\r" +
    "\n" +
    "                                        <input id=\"answer2\" type=\"text\" class=\"form-control\" name=\"answer2\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 2\" ng-model=\"statement.choices[1]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"2\"/>\r" +
    "\n" +
    "                                        <input id=\"answer3\" type=\"text\" class=\"form-control\" name=\"answer3\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 3\" ng-model=\"statement.choices[2]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"3\"/>\r" +
    "\n" +
    "                                        <input id=\"answer4\" type=\"text\" class=\"form-control\" name=\"answer4\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 4\" ng-model=\"statement.choices[3]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"4\"/>\r" +
    "\n" +
    "                                        <input id=\"answer5\" type=\"text\" class=\"form-control\" name=\"answer5\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 5\" ng-model=\"statement.choices[4]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctChoice\" type=\"radio\" value=\"5\"/>\r" +
    "\n" +
    "                                        <input id=\"answer6\" type=\"text\" class=\"form-control\" name=\"answer6\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 6\" ng-model=\"statement.choices[5]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"save()\" ng-disabled=\"isSaving\"><span>Opslaan</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"image-container\" image-thumb image-id=\"statement.id\"\r" +
    "\n" +
    "                                        image-url=\"parseImageUrl(statement.id)\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <div upload-file question-id=\"statement.id\"></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"cancel()\"><span>Terug</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/result.html',
    "<div class=\"result-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container result\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <p>Resultaten</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"winner-container\">\r" +
    "\n" +
    "                        <p>Winnaar: {{ winner.email }}</p>\r" +
    "\n" +
    "                        <button ng-click=\"selectWinner()\" ng-disabled=\"winner\"><span>Kies een winnaar</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"person-container\">\r" +
    "\n" +
    "                        <p>Totaal aantal deelnemers: {{ scores.length }}</p>\r" +
    "\n" +
    "                        <ul>\r" +
    "\n" +
    "                            <li ng-repeat=\"score in scores | orderBy:'scoreCount':true\">\r" +
    "\n" +
    "                                <p title=\"{{score.person.name}}\">{{ score.name }}</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <p title=\"{{score.person.email}}\">{{ score.email }}</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <p title=\"{{score.person.score}}\">{{ score.scoreCount }}</p>\r" +
    "\n" +
    "                            </li>\r" +
    "\n" +
    "                        </ul>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/statement.html',
    "<div class=\"statement-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container statement\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"!statement\" class=\"row\">\r" +
    "\n" +
    "                    <h1>{{ theme.name }}</h1>\r" +
    "\n" +
    "                    <ul>\r" +
    "\n" +
    "                        <li ng-repeat=\"statement in statements\">\r" +
    "\n" +
    "                            <div class=\"icon-container\">\r" +
    "\n" +
    "                                <button ng-click=\"editStatement($index)\" class=\"icon\">\r" +
    "\n" +
    "                                    <span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span>\r" +
    "\n" +
    "                                </button>\r" +
    "\n" +
    "                                <!--<button ng-click=\"deleteStatement($index)\" class=\"icon\">\r" +
    "\n" +
    "                                    <span aria-hidden=\"true\" class=\"glyphicon glyphicon-remove\"></span>\r" +
    "\n" +
    "                                </button>-->\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <p>{{ statement.name }}</p>\r" +
    "\n" +
    "                        </li>\r" +
    "\n" +
    "                    </ul>\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"newStatement()\"><span>Vraag toevoegen</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"statement\" class=\"row\">\r" +
    "\n" +
    "                    <p ng-show=\"statement.id > 0\">Vraag bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <p ng-show=\"statement.id === 0\">Vraag toevoegen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <form name=\"statement-form\" action=\"\">\r" +
    "\n" +
    "                        <fieldset>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <label>Type:</label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <label>\r" +
    "\n" +
    "                                    <input ng-model=\"statement.type\" type=\"radio\" value=\"statement\"/>Statement\r" +
    "\n" +
    "                                </label>\r" +
    "\n" +
    "                                <label>\r" +
    "\n" +
    "                                    <input ng-model=\"statement.type\" type=\"radio\" value=\"multiple\"/>Multiple choice\r" +
    "\n" +
    "                                </label>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <!--<input id=\"keywords\" type=\"text\" class=\"form-control\" name=\"name\"\r" +
    "\n" +
    "                                    placeHolder=\"titel\"\r" +
    "\n" +
    "                                    ng-model=\"theme.name\"/>-->\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <label>Vraag:</label>\r" +
    "\n" +
    "                            <input id=\"name\" type=\"text\" class=\"form-control\" name=\"name\"\r" +
    "\n" +
    "                                    placeHolder=\"Stel een vraag\"\r" +
    "\n" +
    "                                    ng-model=\"statement.name\"/>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <label>Antwoorden:</label>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div ng-switch=\"statement.type\">\r" +
    "\n" +
    "                                <div ng-switch-when=\"statement\">\r" +
    "\n" +
    "                                    <div class=\"statement-radio\">\r" +
    "\n" +
    "                                        <label>\r" +
    "\n" +
    "                                            <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"0\"/>Goed\r" +
    "\n" +
    "                                        </label>\r" +
    "\n" +
    "                                        <label>\r" +
    "\n" +
    "                                            <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"1\"/>Fout\r" +
    "\n" +
    "                                        </label>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                                <div ng-switch-when=\"multiple\">\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"0\"/>\r" +
    "\n" +
    "                                        <input id=\"answer1\" type=\"text\" class=\"form-control\" name=\"answer1\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 1\" ng-model=\"statement.answers[0]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"1\"/>\r" +
    "\n" +
    "                                        <input id=\"answer2\" type=\"text\" class=\"form-control\" name=\"answer2\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 2\" ng-model=\"statement.answers[1]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"2\"/>\r" +
    "\n" +
    "                                        <input id=\"answer3\" type=\"text\" class=\"form-control\" name=\"answer3\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 3\" ng-model=\"statement.answers[2]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"3\"/>\r" +
    "\n" +
    "                                        <input id=\"answer4\" type=\"text\" class=\"form-control\" name=\"answer4\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 4\" ng-model=\"statement.answers[3]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"4\"/>\r" +
    "\n" +
    "                                        <input id=\"answer5\" type=\"text\" class=\"form-control\" name=\"answer5\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 5\" ng-model=\"statement.answers[4]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"multiple-radio\">\r" +
    "\n" +
    "                                        <input ng-model=\"statement.correctAnswer\" type=\"radio\" value=\"5\"/>\r" +
    "\n" +
    "                                        <input id=\"answer6\" type=\"text\" class=\"form-control\" name=\"answer6\"\r" +
    "\n" +
    "                                                placeHolder=\"Antwoord 6\" ng-model=\"statement.answers[5]\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"row\">\r" +
    "\n" +
    "                                <div class=\"image-container\" image-thumb image-id=\"statement.id\"\r" +
    "\n" +
    "                                        image-url=\"parseImageUrl(statement.id)\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                                <label>Afbeelding:</label>\r" +
    "\n" +
    "                                <div upload-file statement-id=\"statement.id\"></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <label for=\"info\">Uitleg:</label>\r" +
    "\n" +
    "                            <textarea id=\"info\" type=\"text\" class=\"form-control\" name=\"informatie\"\r" +
    "\n" +
    "                                    placeHolder=\"Geef uitleg over het antwoord\" ng-model=\"statement.description\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"save()\"><span>Opslaan</span></button>\r" +
    "\n" +
    "                                <p ng-show=\"isSaving\" class=\"save-feedback pull-right\">... opslaan</p>\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"cancel()\"><span>Terug</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/survey.html',
    "<div class=\"survey-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container survey\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <p>Onderzoek instellen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/theme.html',
    "<div class=\"theme-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container theme\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"!theme\" class=\"row\">\r" +
    "\n" +
    "                    <p>Thema's bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <ul>\r" +
    "\n" +
    "                        <li ng-repeat=\"theme in themes\">\r" +
    "\n" +
    "                            <div class=\"icon-container\">\r" +
    "\n" +
    "                                <button ng-click=\"selectTheme($index)\" class=\"icon\">\r" +
    "\n" +
    "                                    <span aria-hidden=\"true\" class=\"glyphicon glyphicon-edit\"></span>\r" +
    "\n" +
    "                                </button>\r" +
    "\n" +
    "                                <button ng-click=\"editTheme($index)\" class=\"icon\">\r" +
    "\n" +
    "                                    <span aria-hidden=\"true\" class=\"glyphicon glyphicon-open\"></span>\r" +
    "\n" +
    "                                </button>\r" +
    "\n" +
    "                                <!--<button ng-click=\"deleteTheme($index)\" class=\"icon\">\r" +
    "\n" +
    "                                    <span aria-hidden=\"true\" class=\"glyphicon glyphicon-remove\"></span>\r" +
    "\n" +
    "                                </button>-->\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <p>{{ theme.name }}</p>\r" +
    "\n" +
    "                        </li>\r" +
    "\n" +
    "                    </ul>\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"logout()\"><span>Terug</span></button>\r" +
    "\n" +
    "                        <!--<button type=\"button\" ng-click=\"newTheme()\"><span>Thema toevoegen</span></button>-->\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"theme\" class=\"row\">\r" +
    "\n" +
    "                    <p ng-show=\"theme.id > 0\">Thema bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <p ng-show=\"theme.id === 0\">Thema toevoegen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <form name=\"theme-form\" action=\"\">\r" +
    "\n" +
    "                        <fieldset>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"name\" placeHolder=\"naam\"\r" +
    "\n" +
    "                                   ng-model=\"theme.name\"/>\r" +
    "\n" +
    "                            <textarea type=\"text\" class=\"form-control\" name=\"description\" placeHolder=\"beschrijving\"\r" +
    "\n" +
    "                                      ng-model=\"theme.description\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"save()\"><span>Opslaan</span></button>\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"cancel()\"><span>Terug</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/cms/topic.html',
    "<div class=\"theme-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container theme\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"!theme\" class=\"row\">\r" +
    "\n" +
    "                    <p>Thema's bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <ul>\r" +
    "\n" +
    "                        <li ng-repeat=\"theme in themes\">\r" +
    "\n" +
    "                            <div class=\"icon-container\">\r" +
    "\n" +
    "                                <div icon-font icon-class=\"icon-edit\" ng-click=\"editTheme($index)\" class=\"icon\" title=\"Thema bewerken\"></div>\r" +
    "\n" +
    "                                <div icon-font icon-class=\"icon-delete\" ng-click=\"deleteTheme($index)\" class=\"icon\" title=\"Thema verwijderen\"></div>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"cms-button\" ng-click=\"selectTheme($index)\" title=\"Thema selecteren\"><p>{{ theme.name }}</p></div>\r" +
    "\n" +
    "                        </li>\r" +
    "\n" +
    "                    </ul>\r" +
    "\n" +
    "                    <div class=\"button-container\">\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"newTheme()\"><span>Thema toevoegen</span></button>\r" +
    "\n" +
    "                        <button type=\"button\" ng-click=\"goBack()\"><span>Terug</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-show=\"theme\" class=\"row\">\r" +
    "\n" +
    "                    <p ng-show=\"theme.id > 0\">Thema bewerken</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <p ng-show=\"theme.id === 0\">Thema toevoegen</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <form name=\"theme-form\" action=\"\">\r" +
    "\n" +
    "                        <fieldset>\r" +
    "\n" +
    "                            <input type=\"text\" class=\"form-control\" name=\"name\" placeHolder=\"naam\"\r" +
    "\n" +
    "                                   ng-model=\"theme.name\"/>\r" +
    "\n" +
    "                            <textarea type=\"text\" class=\"form-control\" name=\"description\" placeHolder=\"beschrijving\"\r" +
    "\n" +
    "                                      ng-model=\"theme.description\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"button-container\">\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"save()\"><span>Opslaan</span></button>\r" +
    "\n" +
    "                                <button type=\"button\" ng-click=\"cancel()\"><span>Terug</span></button>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </fieldset>\r" +
    "\n" +
    "                    </form>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/appTemplate.html',
    "<div id=\"page-holder\" ng-class=\"menu.url\">\r" +
    "\n" +
    "    <div ng-include=\"layoutPartial('header')\"></div>\r" +
    "\n" +
    "    <div ng-view></div>\r" +
    "\n" +
    "    <div ng-include src=\"'html/pages/popup.html'\"></div>\r" +
    "\n" +
    "    <div ng-include=\"layoutPartial('footer')\"></div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/bulletTemplate.html',
    "<div ng-show=\"menu.showHeader\" class=\"nav\">\r" +
    "\n" +
    "\t<div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"bullet-bar-container\" >\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"bullet-bread-bar\" ng-class=\"barStyle\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div ng-repeat=\"step in steps\" class=\"bullet-step\">\r" +
    "\n" +
    "                    <div ng-hide=\"$last\" class=\"bullet-bar-line\"></div>\r" +
    "\n" +
    "                    <div ng-hide=\"step.active\" class=\"bullet-circle\"></div>\r" +
    "\n" +
    "                    <div ng-show=\"step.active\" class=\"bullet-circle bullet-active\"></div>\r" +
    "\n" +
    "                    <div ng-show=\"step.active\" class=\"bullet-circle bullet-center\"></div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-broken-line\" class=\"broken-line\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\t\t<div class=\"count-container\">\r" +
    "\n" +
    "\t\t\t<p>{{ bulletIndex }} van {{ steps.length }}</p>\r" +
    "\n" +
    "\t\t</div>\r" +
    "\n" +
    "\t</div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/cmsTemplate.html',
    "<div id=\"page-holder\">\r" +
    "\n" +
    "    <div ng-include=\"layoutPartial('header')\"></div>\r" +
    "\n" +
    "    <div ng-view></div>\r" +
    "\n" +
    "    <!--<div ng-include=\"layoutPartial('footer')\"></div>-->\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/headerBurgerPopover.html',
    "<div ng-show=\"menu.showHeader\" class=\"burger-menu\" dropdown>\r" +
    "\n" +
    "    <div dropdown-toggle class=\"dropdown-toggle\">\r" +
    "\n" +
    "        <icon-font icon-class=\"icon-burger\" class=\"icon-burger menu\" href=\"#\" title=\"info\"\r" +
    "\n" +
    "            ng-class=\"{active: isOpen}\" ng-click=\"toggleMenu()\"></icon-font>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"dropdown-menu\" role=\"menu\">\r" +
    "\n" +
    "        <ul>\r" +
    "\n" +
    "            <li ng-repeat=\"item in items\" ng-click=\"select(item)\"><span>{{ item.title | translate }}</span></li>\r" +
    "\n" +
    "        </ul>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/headerLogo.html',
    "<div class=\"header-logo-container\">\r" +
    "\n" +
    "    <div class=\"header-logo\" ng-click=\"goHome()\"></div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/infoTemplate.html',
    "<div class=\"description-container\" ng-show=\"showInfo\" custom-scroll>\r" +
    "\n" +
    "    <div ng-if=\"info.correct == undefined\">\r" +
    "\n" +
    "        <div ng-click=\"close()\" icon-font icon-class=\"icon-close\" class=\"pull-right\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div text-format text=\"info.description | translate\" class=\"info\"></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-if=\"info.correct != undefined\">\r" +
    "\n" +
    "        <div class=\"title\">\r" +
    "\n" +
    "            <!--<div ng-if=\"info.correct\" icon-font icon-class=\"icon-correct\"></div>\r" +
    "\n" +
    "            <div ng-if=\"!info.correct\" icon-font icon-class=\"icon-incorrect\"></div>-->\r" +
    "\n" +
    "            <h1>{{ info.correct ? 'Goed' : 'Fout' }}</h1>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div text-format text=\"info.description | translate\" class=\"question\"></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/navigateTemplate.html',
    "<div class=\"nav\" ng-class=\"menu.url\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div ng-show=\"!menu.showVotes && !menu.showHome\">\r" +
    "\n" +
    "            <div ng-show=\"menu.showPrevious\">\r" +
    "\n" +
    "                <div icon-font icon-class=\"icon-left\" class=\"pull-left menu\"></div>\r" +
    "\n" +
    "                <div class=\"btn-ruler pull-left\"></div>\r" +
    "\n" +
    "                <div icon-font icon-class=\"icon-navigate-left\" class=\"pull-left\" title=\"{{ prevMenu.label | translate }}\"\r" +
    "\n" +
    "                        ng-click=\"selectPreviousMenu()\"></div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div ng-show=\"menu.showNext\">\r" +
    "\n" +
    "                <div icon-font icon-class=\"icon-right\" class=\"pull-right menu\"></div>\r" +
    "\n" +
    "                <div class=\"btn-ruler pull-right\"></div>\r" +
    "\n" +
    "                <div icon-font icon-class=\"icon-navigate-right\" class=\"pull-right\" ng-class=\"{disabled: !isValid}\" title=\"{{ nextMenu.label | translate }}\"\r" +
    "\n" +
    "                        ng-click=\"selectNextMenu()\" ng-enabled=\"isValid\"></div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div ng-show=\"menu.showVotes\">\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-left\" class=\"pull-left menu\"></div>\r" +
    "\n" +
    "            <div class=\"btn-ruler pull-left\"></div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-correct\" class=\"pull-left\" ng-click=\"rateStatement(true)\"></div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-right\" class=\"pull-right menu\"></div>\r" +
    "\n" +
    "            <div class=\"btn-ruler pull-right\"></div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-incorrect\" class=\"pull-right\" ng-click=\"rateStatement(false)\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div ng-show=\"menu.showHome\">\r" +
    "\n" +
    "            <div class=\"start-test-container\">\r" +
    "\n" +
    "                <div class=\"text\"><h3>{{ menu.label | translate }}</h3></div>\r" +
    "\n" +
    "                <div ng-show=\"menu.showNext\" icon-font icon-class=\"icon-navigate-right\" class=\"\" ng-class=\"{valid: (isValid === true)}\"\r" +
    "\n" +
    "                        ng-click=\"selectNextMenu()\" title=\"{{ nextMenu.label  | translate }}\"></div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/plugins/iconFontLibrary.html',
    "<div class=\"icon-menu\">\r" +
    "\n" +
    "    <svg xmlns=\"http://www.w3.org/2000/svg\" style=\"display: none;\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-navigate-left\" viewBox=\"0 0 512 512\" ><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m30-328l-112 112l112 112l32-32l-80-80l80-80z\"/></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-navigate-right  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-navigate-right\" viewBox=\"0 0 512 512\" ><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 400 400\" style=\"enable-background:new 0 0 400 400;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:none;stroke-width:18.983;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"201.1\" cy=\"200.1\" r=\"189\"/>\r" +
    "\n" +
    "<polyline style=\"fill:none;stroke-width:18.983;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"\r" +
    "\n" +
    "\t199.1,124.7 274.6,200.1 199.1,275.5 \"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke-width:18.983;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"265.2\" y1=\"200.1\" x2=\"99.7\" y2=\"200.1\"/>\r" +
    "\n" +
    "</svg>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- end icon-navigate-right  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-thumb-up  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-thumb-up\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 400 400\" style=\"enable-background:new 0 0 400 400;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:none;stroke:#1D1D1B;stroke-width:19.0348;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"200.3\" cy=\"200.3\" r=\"189.5\"/>\r" +
    "\n" +
    "<path style=\"fill:#1D1D1B;\" d=\"M294.2,199.3c-1,4.9-2.4,9.8-3,14.8c-0.3,2.1,0.9,4.4,1.4,6.7c1.5,8.1-0.5,15.6-3.8,23\r" +
    "\n" +
    "\tc-0.9,2.1-1.4,4.5-1.5,6.8c-0.1,8.4-2.8,15.9-7.1,23c-1.5,2.4-2.6,5.2-3.3,7.9c-3.4,13.4-12.7,20.8-25.1,25.2\r" +
    "\n" +
    "\tc-7.1,2.5-14.6,2.7-22,2.9c-31.7,0.8-63-1.9-94-8.1c-18.7-3.7-32.3-20-32.4-39.1c-0.1-20.2-0.1-40.4,0-60.6\r" +
    "\n" +
    "\tc0.1-17.4,11.6-34,27.9-40.2c10.1-3.8,20.4-7.1,30.5-10.9c2.5-0.9,4.8-2.2,7-3.7c7.6-5.1,14.4-11.2,20.3-18.2\r" +
    "\n" +
    "\tc5.1-6,8.4-12.7,9.5-20.6c2.1-14.5,11.3-24.7,24.4-27.3c11.6-2.3,21.6,1.6,30,9.4c14.8,13.8,20.3,30.9,16.5,50.9\r" +
    "\n" +
    "\tc-1.1,5.8-2.6,11.6-3.9,17.5c0.5,0.2,1,0.5,1.6,0.6c14.6,4.2,23.7,13.6,26.7,28.6c0.1,0.4,0.3,0.8,0.5,1.2\r" +
    "\n" +
    "\tC294.2,192.6,294.2,195.9,294.2,199.3z M124.5,232c0,9.8,0,19.7,0,29.5c0.1,9.2,6.5,17.5,15.2,19c13.3,2.2,26.6,4.4,39.9,5.9\r" +
    "\n" +
    "\tc18.6,2.1,37.3,2.6,56,1.7c4.9-0.2,9.7-1.1,14-3.6c3.1-1.8,5.9-4,6.4-7.7c0.7-5.5,3.4-9.9,6-14.6c1.8-3.2,2.9-6.8,3.9-10.3\r" +
    "\n" +
    "\tc0.5-1.7-0.4-3.8,0-5.5c1.2-4.4,2.5-8.8,4.2-13c1.7-4.1,1.9-8,0.8-12.4c-0.9-3.3-1-7-0.6-10.4c0.5-4.6,1.9-9,2.5-13.5\r" +
    "\n" +
    "\tc1.2-9-4-16.6-12.5-17.2c-7.5-0.5-15,0-22.5,0.3c-1.9,0.1-2.8-0.2-2.1-2.3c0.3-0.7,0.6-1.5,0.9-2.2c4.7-11.1,8.6-22.5,11.2-34.3\r" +
    "\n" +
    "\tc1.7-7.9,2.6-15.8-0.9-23.4c-3-6.7-7.1-12.5-14.2-15.4c-5.1-2.1-9.4-0.3-11.7,4.6c-0.9,2-1.2,4.2-1.9,6.3\r" +
    "\n" +
    "\tc-1.5,5.1-2.4,10.4-4.7,15.1c-6.7,14-17.6,24.6-30,33.5c-4.9,3.5-10.3,6.6-15.9,8.9c-9.3,3.8-19,6.7-28.5,10.2\r" +
    "\n" +
    "\tc-9.5,3.5-15.5,12.2-15.5,22.3C124.5,212.9,124.5,222.5,124.5,232z\"/>\r" +
    "\n" +
    "</svg>\r" +
    "\n" +
    "        </symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- end icon-thumb-up  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "       <!-- icon-thumb-down  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-thumb-down\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 400 400\" style=\"enable-background:new 0 0 400 400;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:none;stroke:#1D1D1B;stroke-width:18.9739;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"199.9\" cy=\"200.3\" r=\"188.9\"/>\r" +
    "\n" +
    "<path style=\"fill:#1D1D1B;\" d=\"M110.3,199.4c1-4.9,2.4-9.8,3-14.7c0.3-2.1-0.9-4.4-1.4-6.7c-1.5-8.1,0.5-15.5,3.8-22.9\r" +
    "\n" +
    "\tc0.9-2.1,1.4-4.5,1.5-6.8c0.1-8.4,2.8-15.9,7.1-23c1.5-2.4,2.6-5.2,3.3-7.9c3.4-13.3,12.7-20.8,25-25.1c7.1-2.5,14.5-2.7,22-2.9\r" +
    "\n" +
    "\tc31.6-0.8,62.8,1.9,93.7,8.1c18.6,3.7,32.2,19.9,32.3,39c0.1,20.1,0.1,40.3,0,60.4c-0.1,17.4-11.5,33.9-27.8,40\r" +
    "\n" +
    "\tc-10.1,3.8-20.3,7.1-30.4,10.8c-2.5,0.9-4.8,2.2-7,3.7c-7.6,5.1-14.4,11.1-20.3,18.1c-5.1,6-8.3,12.6-9.5,20.6\r" +
    "\n" +
    "\tc-2.1,14.4-11.3,24.6-24.3,27.2c-11.6,2.3-21.6-1.6-29.9-9.4c-14.8-13.8-20.2-30.8-16.4-50.7c1.1-5.8,2.6-11.5,3.9-17.4\r" +
    "\n" +
    "\tc-0.5-0.2-1-0.5-1.6-0.6c-14.5-4.2-23.6-13.5-26.6-28.5c-0.1-0.4-0.3-0.8-0.5-1.2C110.3,206.1,110.3,202.8,110.3,199.4z\r" +
    "\n" +
    "\t M279.5,166.8c0-9.8,0-19.6,0-29.4c-0.1-9.2-6.4-17.5-15.1-19c-13.2-2.2-26.5-4.4-39.8-5.9c-18.5-2.1-37.2-2.6-55.8-1.7\r" +
    "\n" +
    "\tc-4.9,0.2-9.7,1.1-14,3.6c-3.1,1.8-5.9,4-6.3,7.7c-0.7,5.5-3.4,9.9-6,14.5c-1.8,3.2-2.9,6.8-3.9,10.3c-0.5,1.7,0.4,3.8,0,5.5\r" +
    "\n" +
    "\tc-1.1,4.4-2.5,8.8-4.2,13c-1.7,4.1-1.9,8-0.8,12.4c0.9,3.3,1,7,0.6,10.4c-0.5,4.5-1.9,9-2.5,13.5c-1.2,9,4,16.6,12.5,17.2\r" +
    "\n" +
    "\tc7.4,0.5,15,0,22.5-0.3c1.9-0.1,2.8,0.2,2.1,2.3c-0.3,0.7-0.6,1.5-0.9,2.2c-4.7,11.1-8.6,22.4-11.2,34.2c-1.7,7.9-2.6,15.7,0.9,23.4\r" +
    "\n" +
    "\tc3,6.7,7.1,12.5,14.2,15.3c5,2.1,9.4,0.3,11.6-4.6c0.9-2,1.2-4.2,1.9-6.3c1.5-5,2.4-10.4,4.7-15.1c6.7-14,17.5-24.5,29.9-33.4\r" +
    "\n" +
    "\tc4.9-3.5,10.3-6.6,15.9-8.8c9.3-3.8,19-6.7,28.4-10.1c9.5-3.5,15.4-12.2,15.4-22.2C279.5,185.9,279.5,176.3,279.5,166.8z\"/>\r" +
    "\n" +
    "</svg>\r" +
    "\n" +
    "        </symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "       <!-- end icon-thumb-down -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-left\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<polyline style=\"fill:none;stroke:#1D1D1B;stroke-width:22.2265;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"\r" +
    "\n" +
    "\t91.9,188.3 180.2,99.9 91.9,11.6 \"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-right\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<polyline style=\"fill:none;stroke:#1D1D1B;stroke-width:21.8452;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"\r" +
    "\n" +
    "\t107.6,186.7 20.8,99.9 107.6,13.1 \"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-burger\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 100 100\" style=\"enable-background:new 0 0 100 100;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1A1A1A;stroke-width:5.3933;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"19.4\" y1=\"36.6\" x2=\"80.2\" y2=\"36.6\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1A1A1A;stroke-width:5.3933;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"19.4\" y1=\"50.7\" x2=\"80.2\" y2=\"50.7\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1A1A1A;stroke-width:5.3933;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"19.4\" y1=\"64.7\" x2=\"80.2\" y2=\"64.7\"/>\r" +
    "\n" +
    "<circle style=\"fill:none;stroke:#1A1A1A;stroke-width:5.3933;stroke-miterlimit:10;\" cx=\"49.8\" cy=\"50.3\" r=\"46.7\"/>\r" +
    "\n" +
    "</svg>\r" +
    "\n" +
    "        </symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-about\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m0-344c-71 0-128 57-128 128c0 71 57 128 128 128c71 0 128-57 128-128c0-71-57-128-128-128z m-5 194c-10 0-17-7-17-17c0-11 7-18 17-18c11 0 17 7 18 18c0 10-7 17-18 17z m27-72c-7 8-6 16-6 24l0-2l-33 0l0-2c0-10 3-18 11-29c7-7 12-13 12-19c0-7-5-11-14-11c-7 0-14 2-19 6l-7-21c7-4 18-8 32-8c25 0 39 14 39 30c0 15-8 24-15 32z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-person\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m70-166c-16-3-38-18-38-25l0-17c0-6 9-18 12-29c1-1 3 0 5-2c4-5 5-13 5-16c2-6-5-6-5-6c0 0 5-28 1-46c-6-25-38-37-50-37c-12 0-44 12-50 37c-4 18 1 46 1 46c0 0-6 0-5 6c1 3 2 11 6 16c2 2 3 1 4 2c3 11 12 23 12 29l0 17c0 7-22 22-38 25c-20 4-31 18-26 53c3 18 49 26 96 25c46 1 93-7 96-25c4-35-6-49-26-53z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-result\" viewBox=\"0 0 512 512\"><path d=\"m211 307l-51-51l-32 32l83 80l173-192l-32-32z m45-307c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-data\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m128-335c0-5-4-8-8-8l-240 0c-5 0-8 3-8 8l0 240c0 4 3 8 8 8l240 0c4 0 8-4 8-8z m-16 224c0 4-4 8-8 8l-208 0c-5 0-8-4-8-8l0-208c0-5 3-8 8-8l208 0c4 0 8 3 8 8z m-46-73l-46-96l-48 51l-41-43l-11 12l52 54l44-46l36 75c-2 3-3 5-3 9c0 9 7 16 16 16c8 0 15-7 15-16c0-9-6-16-14-16z\"/></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-gender\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m16-218c28-7 48-32 48-62c0-35-29-64-64-64c-35 0-64 29-64 64c0 30 20 55 48 62l0 66l-32 0l0 32l32 0l0 32l32 0l0-32l32 0l0-32l-32 0z m-16-30c-18 0-32-14-32-32c0-18 14-32 32-32c18 0 32 14 32 32c0 18-14 32-32 32z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-name\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m112-256c0-22-18-40-40-40l-144 0c-22 0-40 18-40 40l0 80c0 22 18 40 40 40l144 0c22 0 40-18 40-40z m-184-24l144 0c10 0 19 7 22 16l-189 0c3-9 12-16 23-16z m168 104c0 13-11 24-24 24l-144 0c-14 0-24-11-24-24l0-56l192 0z m-168-24l64 0c4 0 8-4 8-8c0-4-4-8-8-8l-64 0c-5 0-8 4-8 8c0 4 3 8 8 8z m128 0l16 0c4 0 8-4 8-8c0-4-4-8-8-8l-16 0c-5 0-8 4-8 8c0 4 3 8 8 8z m-128 32l32 0c4 0 8-4 8-8c0-4-4-8-8-8l-32 0c-5 0-8 4-8 8c0 4 3 8 8 8z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-age\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m-47-155c3 14 9 23 17 29c9 6 18 9 27 9c10 0 18-2 25-7c6-4 11-11 15-19c4-8 7-17 9-27c2-10 3-20 3-31c-8 11-16 18-25 22c-9 4-18 6-27 6c-9 0-18-2-27-6c-9-4-17-10-25-17c-7-7-13-16-18-26c-5-10-7-20-7-33c0-12 2-23 5-34c3-11 8-20 15-28c6-8 14-15 24-20c9-5 21-7 33-7c26 0 46 10 61 30c15 20 22 49 22 86c0 16-1 32-3 49c-3 16-7 31-13 45c-6 13-15 24-25 33c-11 9-25 13-42 13c-9 0-17-1-26-4c-9-2-16-6-23-11c-7-6-12-13-17-21c-4-9-7-19-8-31z m44-47c6 0 13-2 18-4c6-3 11-7 15-13c4-5 8-11 10-18c3-7 4-14 4-22c0-8-1-15-4-22c-2-7-6-13-10-18c-4-5-9-9-15-12c-5-3-12-4-18-4c-6 0-12 1-17 4c-6 3-11 7-15 12c-4 5-7 11-9 18c-3 7-4 14-4 22c0 8 1 15 4 22c2 7 5 13 9 18c4 6 9 10 15 13c5 2 11 4 17 4z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-location\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m0-344c-71 0-128 57-128 128c0 71 57 128 128 128c71 0 128-57 128-128c0-71-57-128-128-128z m-112 136l58 0c0 15 3 32 6 48l-49 0c-8-14-13-31-15-48z m58-16l-58 0c2-17 7-34 15-48l49 0c-3 16-6 33-6 48z m22-48l64 0c3 16 6 33 6 48l-76 0c0-15 3-32 6-48z m-6 64l76 0c0 15-3 32-6 48l-64 0c-3-16-6-33-6-48z m92 0l58 0c-2 17-7 34-15 48l-49 0c3-16 6-33 6-48z m0-16c0-15-3-32-6-48l49 0c8 14 13 31 15 48z m32-64l-42 0c-3-14-7-26-9-34c20 6 37 18 51 34z m-68-38c2 7 6 21 10 38l-56 0c4-17 8-31 10-38c6-1 12-2 18-2c6 0 12 1 18 2z m-53 4c-2 8-6 20-9 34l-42 0c14-16 31-28 51-34z m-51 178l42 0c3 14 7 26 9 34c-20-6-37-18-51-34z m68 38c-2-7-6-21-10-38l56 0c-4 17-8 31-10 38c-6 1-12 2-18 2c-6 0-12-1-18-2z m53-4c2-8 6-20 9-34l42 0c-14 16-31 28-51 34z\"/></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-facebook\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m61-255l-45 0l0-33c0-10 11-12 15-12c5 0 29 0 29 0l0-44l-33 0c-45 0-55 33-55 54l0 35l-33 0l0 45l33 0c0 58 0 122 0 122l44 0c0 0 0-65 0-122l38 0z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-twitter\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m128-301c-4 2-17 8-29 9c7-5 19-19 22-31c-8 5-25 13-33 13c0 0 0 0 0 0c-10-11-23-18-38-18c-30 0-53 25-53 57c0 4 0 8 1 13l0 0c-39-2-86-23-112-59c-16 29-2 63 16 75c-6 0-17-1-23-7c0 20 9 46 41 56c-6 3-17 2-22 2c2 16 23 38 47 38c-8 11-37 30-73 24c24 16 53 25 83 25c85 0 151-74 147-166c0 0 0 0 0 0c0-1 0-1 0-1c0 0 0-1 0-1c8-6 19-16 26-29z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-email\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m0-296l-128 0l0 160l256 0l0-160z m0 16l90 0l-90 64l-90-64z m-112 5l55 36l-55 60z m112 123l-92 0l45-79l47 31l47-31l45 79z m112-27l-54-60l54-36z\"/></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-broken-line\" viewBox=\"0 0 308.2 120.3\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 395.4 120.3\" style=\"enable-background:new 0 0 395.4 120.3;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<polyline style=\"fill:none;stroke:#FFFF00;stroke-width:12.6303;stroke-miterlimit:10;\" points=\"118.2,52.4 138.1,23.5 160.3,95.9\r" +
    "\n" +
    "\t179.8,23.5 204.9,95.9 229,23.5 244.9,59.7 396.4,59.7 \"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#FFFF00;stroke-width:12.6303;stroke-miterlimit:10;\" x1=\"-0.1\" y1=\"59.7\" x2=\"103.9\" y2=\"59.7\"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "        <!-- icon-starter  -->\r" +
    "\n" +
    "        <symbol id=\"icon-starter\" viewBox=\"0 0 512 512\">\r" +
    "\n" +
    "            <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:9.4051;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.9\" cy=\"99.4\" r=\"93.6\"/>\r" +
    "\n" +
    "<path style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM81.4,137.3l38.5-10.2c0,0,0-11.9,4.8-17.3c4.8-5.5,19.5-15.9,19.5-39.2s-20.5-40.9-44-40.9s-42,19-42,43.5c0,29,24,30.7,24,56.8\"/>\r" +
    "\n" +
    "<line style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"80\" y1=\"150.4\" x2=\"121.5\" y2=\"139.7\"/>\r" +
    "\n" +
    "<line style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"80\" y1=\"163.4\" x2=\"121.5\" y2=\"152.8\"/>\r" +
    "\n" +
    "<path style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM89.9,168.4c0,0,6.9,10.5,22.8,0\"/>\r" +
    "\n" +
    "<path style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM94.7,123.1c0,0,2.1-18.3-5.9-32.8c0,0,7.3-1.7,11.4,4.5c0,0,4.1-4.7,10.9-4.7c0,0-8.6,22.8-5,33\"/>\r" +
    "\n" +
    "<path style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM77.2,40.7h20.7c0,0-16.9,17.3-17.9,17.7c-1,0.4,22.6,0,22.6,0v15.5\"/>\r" +
    "\n" +
    "<line style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:6.9807;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"102.5\" y1=\"58.4\" x2=\"115.1\" y2=\"50\"/>\r" +
    "\n" +
    "</svg>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        </symbol>\r" +
    "\n" +
    "        <!-- end icon-starter  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-average  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-average\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:9.5268;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.9\" cy=\"99.9\" r=\"94.9\"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.071;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.6\" cy=\"99.9\" r=\"65.7\"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.071;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"72.6\" cy=\"76.9\" r=\"12.9\"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.071;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"128.1\" cy=\"76.9\" r=\"12.9\"/>\r" +
    "\n" +
    "<polyline style=\"fill:none;stroke:#1D1D1B;stroke-width:7.071;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"\r" +
    "\n" +
    "\t99.6,92 93.1,104.1 102.6,104.1 \"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.071;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM139.1,117.1c0,0-10.3,26.5-41.3,24.2\"/>\r" +
    "\n" +
    "</svg>\r" +
    "\n" +
    "        </symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- end icon-average  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-boss  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-boss\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:9.4355;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.9\" cy=\"99.5\" r=\"93.9\"/>\r" +
    "\n" +
    "<polygon style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.0033;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"\r" +
    "\n" +
    "\t57.8,133.8 144.5,133.8 157.4,76.4 133.9,103.9 120.4,69.3 101.6,103.2 82.5,69.3 69.6,101.6 48.4,75.7 \"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.0033;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"81.6\" cy=\"65.1\" r=\"8.7\"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.0033;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"121.3\" cy=\"65.1\" r=\"8.7\"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.0033;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"157.4\" cy=\"74.9\" r=\"8.7\"/>\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.0033;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"48.4\" cy=\"74.9\" r=\"8.7\"/>\r" +
    "\n" +
    "<line style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:7.0033;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"58.3\" y1=\"146.2\" x2=\"142.8\" y2=\"146.2\"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- end icon-boss  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-master -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-master\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:9.4964;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.9\" cy=\"100\" r=\"94.6\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM99.6,45.8c0,0-1.3-5.4-13.2-5.6s-16.7,19.3-5.2,18.4c11.9-0.9,11.3,21.7,3.7,21.4c-10.2-0.3-9,5.1-9,5.1\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM99.9,107.4c0,0-6.7,5.5-14.3,2.3c-7.6-3.2-5.2-8.4-5.4-14.1c-0.2-5.7-4.7-13-12.8-10.6c-4.9,1.5-10.1,7.6-5.4,14.3\r" +
    "\n" +
    "\tc4.7,6.7,0,18.9-10.7,18.7c-10.7-0.2-16.4-11.6-2.8-26.5c0,0-7.8-15.8,11.4-23.7\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM75,49.1c0,0-11,0.7-13.8,9.6c-2.8,8.9,6.9,16.7,6.9,16.7\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM49.1,117.8c0,0-8.7,29.8,22.5,23.9\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM99.9,46.9L99.6,144c-20.7,17.5-28.7-2.1-30.1-8c-1.5-5.8-3.2-15.1,8.8-15.3c12-0.2,8.9,12.2,8.9,12.2\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM100.1,45.8c0,0,1.3-5.4,13.2-5.6S130,58,118.5,58.6c-10.8,0.5-12,20.7-3.7,21.4c10.1,0.9,9,5.1,9,5.1\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM99.9,107.4c0,0,6.7,5.5,14.3,2.3c7.6-3.2,5.2-8.4,5.4-14.1c0.2-5.7,4.7-13,12.8-10.6c4.9,1.5,10.1,7.6,5.4,14.3s0,18.9,10.7,18.7\r" +
    "\n" +
    "\tc10.7-0.2,16.4-11.6,2.8-26.5c0,0,7.8-15.8-11.4-23.7\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM124.7,49.1c0,0,11,0.7,13.8,9.6c2.8,8.9-6.9,16.7-6.9,16.7\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM150.6,117.8c0,0,8.7,29.8-22.5,23.9\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0485;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM99.9,46.9l0.2,97.1c20.7,17.5,28.7-2.1,30.1-8c1.5-5.8,3.2-15.1-8.8-15.3c-12-0.2-8.9,12.2-8.9,12.2\"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "        <!-- end icon-master  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-endboss  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-endboss\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:#FFFF00;stroke:#1D1D1B;stroke-width:9.441;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.1\" cy=\"99.3\" r=\"94\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"153.3\" y1=\"111.1\" x2=\"170.2\" y2=\"111.1\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"154.1\" y1=\"89.4\" x2=\"168.8\" y2=\"83.1\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"168.1\" y1=\"137.6\" x2=\"154.1\" y2=\"131.6\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"46.3\" y1=\"111.1\" x2=\"30.3\" y2=\"111.1\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"45.4\" y1=\"89.4\" x2=\"30.7\" y2=\"83.1\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"30.3\" y1=\"138\" x2=\"45.4\" y2=\"131.6\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM72.6,90.1c0,0,6.4,5.2,12.3,9.3c5.9,4.2,10.1,6.6,18.7,2.7c8.6-3.9,2.2-9.8,2-10.6c-0.2-0.7-10.3-9.8-21.9-21.4s-20.4,1.2-20.4,1.2\r" +
    "\n" +
    "\ts0,0-11.8,11.3s0,18.9,0,18.9l18.2,19.9v38.8h59.2v-37.1c0,0,19.2-18.2,10.3-34.4\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM100.2,103.4c0,0,6.5,13.5-1.1,26\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM132.5,89.6L132.5,89.6c-3.9-0.7-6.5-4.5-5.8-8.4l4.7-25.6c0.7-3.9,4.5-6.5,8.4-5.8l0,0c3.9,0.7,6.5,4.5,5.8,8.4l-4.7,25.6\r" +
    "\n" +
    "\tC140.2,87.7,136.4,90.3,132.5,89.6z\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM116.9,86.7l-2-0.4c-4.7-0.9-7.9-5.5-7-10.2l4.8-26.1c0.9-4.7,5.5-7.9,10.2-7l2,0.4c4.7,0.9,7.9,5.5,7,10.2l-4.8,26.1\r" +
    "\n" +
    "\tC126.2,84.4,121.6,87.6,116.9,86.7z\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM84.6,70.6l4.8-26c1-5.4,6.2-8.9,11.6-7.9l3.9,0.7c5.4,1,8.9,6.2,7.9,11.6L108,75c-0.8,4.6-4.8,7.9-9.2,8.1\"/>\r" +
    "\n" +
    "<path style=\"fill:none;stroke:#1D1D1B;stroke-width:7.0074;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"\r" +
    "\n" +
    "\tM65,67.2l4.7-25.3c0.9-4.8,5.5-7.9,10.2-7l2.3,0.4c4.8,0.9,7.9,5.5,7,10.2l-4.7,25.3\"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "        <!-- end icon-endboss  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-close\" viewBox=\"0 0 512 512\"><path d=\"m256 33c124 0 224 100 224 224c0 124-100 224-224 224c-124 0-224-100-224-224c0-124 100-224 224-224z m108 299l-75-75l76-75c3-3 3-9 0-12l-22-21c-2-2-4-3-6-3c-2 0-3 1-5 3l-76 74l-76-74c-2-2-3-3-5-3c-2 0-4 1-6 3l-21 21c-3 3-3 9 0 12l76 75l-76 76c-2 1-2 3-2 5c0 2 0 4 2 6l21 22c2 2 4 2 6 2c2 0 4 0 6-2l75-76l76 75c2 2 3 3 5 3c2 0 4-1 6-3l21-21c2-1 3-4 3-6c0-2-1-4-3-6z\"/></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <!-- icon-correct  -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-correct\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 200 200\" style=\"enable-background:new 0 0 200 200;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:none;stroke:#1D1D1B;stroke-width:9.4503;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"99.6\" cy=\"99.1\" r=\"94.1\"/>\r" +
    "\n" +
    "<polyline style=\"fill:none;stroke:#1D1D1B;stroke-width:9.4503;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"\r" +
    "\n" +
    "\t152.3,50.3 54.6,148 54.6,77.2 \"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <!-- end icon-correct -->\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-incorrect\" viewBox=\"0 0 512 512\"><svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r" +
    "\n" +
    "\t viewBox=\"0 0 100 100\" style=\"enable-background:new 0 0 100 100;\" xml:space=\"preserve\">\r" +
    "\n" +
    "<circle style=\"fill:none;stroke:#1D1E1C;stroke-width:4.7643;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" cx=\"50\" cy=\"49.8\" r=\"47.4\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1E1C;stroke-width:4.7643;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"76.6\" y1=\"25.2\" x2=\"27.3\" y2=\"74.4\"/>\r" +
    "\n" +
    "<line style=\"fill:none;stroke:#1D1E1C;stroke-width:4.7643;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;\" x1=\"74.4\" y1=\"74.4\" x2=\"25.1\" y2=\"25.2\"/>\r" +
    "\n" +
    "</svg></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <symbol id=\"icon-select\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m24-248c0 0-59-1-101 26c-43 28-53 86-53 86c0 0 38-44 75-58c41-15 79-6 79-6l0 48l96-64l-96-80z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-edit\" viewBox=\"0 0 512 512\"><path d=\"m256 0c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z m-106-191c-16 16-26 37-20 57c1 2 22-6 22-6l11-24l29-5l9 8l8 9l-5 29l-24 11c0 0-8 21-6 22c20 6 41-4 57-20c17-17 21-43 12-64l134-122l7-21l-14-13l-13-14l-21 7l-122 134c-21-9-47-5-64 12z\"/></symbol>\r" +
    "\n" +
    "        <symbol id=\"icon-delete\" viewBox=\"0 0 512 512\"><path d=\"m335 154l-79 79l-79-79l-23 23l79 79l-79 79l23 23l79-79l79 79l23-23l-79-79l79-79z m-79-154c-141 0-256 115-256 256c0 141 115 256 256 256c141 0 256-115 256-256c0-141-115-256-256-256z m0 472c-119 0-216-97-216-216c0-119 97-216 216-216c119 0 216 97 216 216c0 119-97 216-216 216z\"/></symbol>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    </svg>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/postTemplate.html',
    "<div class=\"post-container\" ng-show=\"showInfo\">\r" +
    "\n" +
    "    <div ng-click=\"close()\" class=\"btn-popup pull-right\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"text\">\r" +
    "\n" +
    "        <h3>{{ post.title }}</h3>\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"social-user\">\r" +
    "\n" +
    "            <div ng-if=\"post.email != null\" class=\"row\">\r" +
    "\n" +
    "                <label for=\"email\">Emailadres: </label>\r" +
    "\n" +
    "                <input id=\"email\" class=\"form-control\" type=\"text\" ng-model=\"post.email\"/>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <div class=\"row\">\r" +
    "\n" +
    "                <div ng-if=\"post.message.picture\" class=\"image-container\"><img ng-src=\"{{ post.message.picture }}\"/></div>\r" +
    "\n" +
    "                <span ng-if=\"post.message.message\" ng-class=\"{'image': post.message.picture}\"> {{ post.message.message }} </span>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            <textarea ng-if=\"post.message.description\" class=\"form-control\" ng-model=\"post.message.description\"></textarea>\r" +
    "\n" +
    "            <textarea ng-if=\"post.message.status\" class=\"form-control\" ng-model=\"post.message.status\"></textarea>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div ng-if=\"post.user.name\" class=\"row\">\r" +
    "\n" +
    "                <p ng-class=\"{'image': post.user.thumb}\">{{ post.user.name }}</p>\r" +
    "\n" +
    "                <div ng-if=\"post.user.thumb\" class=\"image-container\"><img ng-if=\"post.user.thumb\" ng-src=\"{{ post.user.thumb }}\"/></div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <button type=\"button\" class=\"pull-right\" ng-click=\"sharePost()\"><span>Delen</span></button>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/questionTemplate.html',
    "<div class=\"toggle-question\" ng-class=\"{'no-icon': !question.icon}\">\r" +
    "\n" +
    "    <div ng-show=\"!important && question.icon\" icon-font icon-class=\"{{ question.icon }}\" class=\"icon-info\" ng-class=\"[activeClass]\"></div>\r" +
    "\n" +
    "    <div ng-show=\"important && question.icon\" icon-font icon-class=\"{{ question.icon }}\" class=\"icon-field\" ng-class=\"[activeClass]\"></div>\r" +
    "\n" +
    "    <div class=\"question\" ng-class=\"importantClass\">\r" +
    "\n" +
    "        <div ng-show=\"!isOpen\" class=\"btn-question\">\r" +
    "\n" +
    "            <h3 ng-click=\"toggleQuestion()\">{{ question.question | translate }}</h3>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div ng-show=\"isOpen\" class=\"btn-answer\">\r" +
    "\n" +
    "            <h3 ng-click=\"toggleQuestion()\">{{ question.question | translate }}</h3>\r" +
    "\n" +
    "            <p ng-show=\"question.answer\">{{ question.answer | translate }}</p>\r" +
    "\n" +
    "            <input type=\"text\" ng-show=\"question.value === ''\" ng-model=\"fieldValue\">\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/resultBarsTemplate.html',
    "<div class=\"row\">\r" +
    "\n" +
    "\t<div class=\"result-bars-container\">\r" +
    "\n" +
    "\t\t<div class=\"result-bar\" ng-repeat=\"bar in results | orderBy : 'agreeCount' : true\">\r" +
    "\n" +
    "\t\t\t<div class=\"total\"></div>\r" +
    "\n" +
    "\t\t\t<div class=\"percentage\" ng-style=\"{height: (bar.agreePercentage + '%')}\"></div>\r" +
    "\n" +
    "\t\t\t<div class=\"percentage mobile\" ng-style=\"{width: (bar.agreePercentage + '%')}\"></div>\r" +
    "\n" +
    "\t\t\t<p>{{bar.themeDto.name}}</p>\r" +
    "\n" +
    "\t\t</div>\r" +
    "\n" +
    "\t</div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/scoreTemplate.html',
    "<div class=\"votes\">\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"total\"></div>\r" +
    "\n" +
    "        <div class=\"percentage\" ng-style=\"{width: profile.userPercentage + '%'}\"></div>\r" +
    "\n" +
    "        <p ng-style=\"{'left': profile.userPercentage + '%'}\">{{ profile.userCount }}</p>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/socialTemplate.html',
    "<div class=\"nav\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"social-wrapper\">\r" +
    "\n" +
    "            <div class=\"share-title pull-left\"><h3>{{ getTitleForMenu() }}</h3></div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-facebook\" ng-click=\"shareFacebook()\"></div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-twitter\" ng-click=\"shareTwitter()\"></div>\r" +
    "\n" +
    "            <div icon-font icon-class=\"icon-email\" ng-click=\"shareEmail()\"></div>\r" +
    "\n" +
    "            <!--<div class=\"btn-whatsapp\"></div>-->\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/directives/statementTemplate.html',
    "<div class=\"statement\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div class=\"image-container\" image-thumb image-id=\"answer.questionId\" image-url=\"parseImageUrl(answer.questionId)\"></div>\r" +
    "\n" +
    "    <div class=\"gradient-container\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-switch=\"answer.type\">\r" +
    "\n" +
    "        <div ng-switch-when=\"statement\">\r" +
    "\n" +
    "            <div class=\"text-container\"><h1>{{ answer.text }}</h1></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div ng-switch-when=\"multiple\">\r" +
    "\n" +
    "            <div class=\"text-container\"><h1>{{ answer.text }}</h1></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <ul>\r" +
    "\n" +
    "                <li class=\"jtinder-answer\" ng-repeat=\"choice in answer.choices\" ng-click=\"selectAnswer(answer, $index)\">\r" +
    "\n" +
    "                    <h2 class=\"jtinder-answer\" ng-class=\"{'selected': $index == currentAnswer}\">{{ choice }}</h2>\r" +
    "\n" +
    "                </li>\r" +
    "\n" +
    "            </ul>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <div ng-if=\"answer.correct != undefined\" class=\"popup-container\">\r" +
    "\n" +
    "        <div class=\"wrapper\">\r" +
    "\n" +
    "            <div class=\"popup\">\r" +
    "\n" +
    "                <div toggle-info info=\"info\" type=\"description\"></div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/themeTemplate.html',
    "<li class=\"theme\">\r" +
    "\n" +
    "    <input type=\"checkbox\" ng-model=\"checked\">\r" +
    "\n" +
    "    <p>{{ theme.name }}</p>\r" +
    "\n" +
    "    <input type=\"radio\" ng-model=\"$parent.favouriteTheme\" name=\"ratingValue\" value=\"{{ themeRating.id }}\">\r" +
    "\n" +
    "</li>"
  );


  $templateCache.put('html/directives/uploadFileTemplate.html',
    "<div class=\"form-upload-container\" >\r" +
    "\n" +
    "    <!-- The file upload form used as target for the file upload widget -->\r" +
    "\n" +
    "    <!--<form id=\"fileupload\" action=\"{{ uploadUrl }}\" method=\"post\" enctype=\"multipart/form-data\" target=\"_blank\">\r" +
    "\n" +
    "        &lt;!&ndash; Redirect browsers with JavaScript disabled to the origin page\r" +
    "\n" +
    "        <noscript><input type=\"hidden\" name=\"redirect\" value=\"http://blueimp.github.com/jQuery-File-Upload/\"></noscript>&ndash;&gt;\r" +
    "\n" +
    "        &lt;!&ndash; The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload &ndash;&gt;\r" +
    "\n" +
    "        <div class=\"fileupload-buttonbar\">\r" +
    "\n" +
    "            <div>\r" +
    "\n" +
    "                <input id=\"file_select\" type=\"file\" name=\"uploaded_file\">\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "            &lt;!&ndash; The global progress information &ndash;&gt;\r" +
    "\n" +
    "            <div class=\"span5 fileupload-progress fade\">\r" +
    "\n" +
    "                &lt;!&ndash; The global progress bar &ndash;&gt;\r" +
    "\n" +
    "                <div class=\"progress progress-success progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:110px; height:10px;\">\r" +
    "\n" +
    "                    <div class=\"bar\" style=\"width:0%;\" ng-style=\"{width : ( getPercentage() + '%' ) }\"></div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                &lt;!&ndash; The extended global progress information\r" +
    "\n" +
    "                <div class=\"progress-extended\">&nbsp;</div> &ndash;&gt;\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        &lt;!&ndash; The loading indicator is shown during file processing &ndash;&gt;\r" +
    "\n" +
    "        <div class=\"fileupload-loading\"></div>\r" +
    "\n" +
    "        <br>\r" +
    "\n" +
    "        &lt;!&ndash; The table listing the files available for upload/download\r" +
    "\n" +
    "        <table role=\"presentation\" class=\"table table-striped\"><tbody class=\"files\" data-toggle=\"modal-gallery\" data-target=\"#modal-gallery\"></tbody></table> &ndash;&gt;\r" +
    "\n" +
    "    </form>-->\r" +
    "\n" +
    "\r" +
    "\n" +
    "    <form enctype=\"multipart/form-data\" action=\"{{ uploadUrl }}\" method=\"post\" target=\"_blank\">\r" +
    "\n" +
    "        <input type=\"file\" accept=\".jpg, .png\" name=\"uploaded_file\">\r" +
    "\n" +
    "        <button ng-click=\"refreshPage()\">Uploaden</button>\r" +
    "\n" +
    "    </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "</div>"
  );


  $templateCache.put('html/directives/votersTemplate.html',
    "<div class=\"votes\">\r" +
    "\n" +
    "    <div class=\"count-container\" ng-class=\"{right: (numberOfCoolPeople == numberOfScores)}\" ng-style=\"{left: votePercentage + '%'}\">\r" +
    "\n" +
    "        <div class=\"count-people\"><h3>{{ active ? numberOfCoolPeople : numberOfScores }}</h3></div>\r" +
    "\n" +
    "        <div class=\"icon-people\" ng-class=\"{right: (numberOfCoolPeople == numberOfScores)}\"></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "    <div class=\"row\">\r" +
    "\n" +
    "        <div class=\"total\"></div>\r" +
    "\n" +
    "        <div class=\"percentage\" ng-style=\"{width: votePercentage + '%'}\"></div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/layout/cms-footer.html',
    "<footer class=\"primary\">\r" +
    "\n" +
    "    <div class=\"full-width-sm\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div footer-navigate class=\"nav-container\" ng-class=\"{'no-social': (menu.showSocial === false)}\" menu=\"home\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"row\" ng-if=\"menu.showSocial\">\r" +
    "\n" +
    "            <div social-share class=\"social-container\" menu=\"menu\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</footer>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    "
  );


  $templateCache.put('html/layout/cms-header.html',
    "<header class=\"primary\">\r" +
    "\n" +
    "    <div class=\"full-width-sm\">\r" +
    "\n" +
    "        <div class=\"row header-container\">\r" +
    "\n" +
    "            <div class=\"logo-coen\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</header>\r" +
    "\n"
  );


  $templateCache.put('html/layout/footer.html',
    "<footer class=\"primary\">\r" +
    "\n" +
    "    <div class=\"full-width-sm\">\r" +
    "\n" +
    "        <div class=\"row\">\r" +
    "\n" +
    "            <div footer-navigate class=\"nav-container\" ng-class=\"{'no-social': (menu.showSocial === false)}\" menu=\"home\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"row\" ng-if=\"menu.showSocial\">\r" +
    "\n" +
    "            <div social-share class=\"social-container\" menu=\"menu\"></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</footer>\r" +
    "\n" +
    "\r" +
    "\n" +
    "    "
  );


  $templateCache.put('html/layout/header.html',
    "<header class=\"primary\">\r" +
    "\n" +
    "    <div class=\"full-width-sm\">\r" +
    "\n" +
    "        <div class=\"row header-container\">\r" +
    "\n" +
    "            <div header-logo></div>\r" +
    "\n" +
    "            <div header-popover></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "        <div class=\"row bullet-navigate-container\">\r" +
    "\n" +
    "            <div bullet-navigate></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</header>\r" +
    "\n"
  );


  $templateCache.put('html/pages/about.html',
    "<div class=\"home-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container home about\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <h1>{{ 'about.title' | translate }}</h1>\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"column-question\">\r" +
    "\n" +
    "                        <div toggle-question class=\"question-right\" question=\"question1\"></div>\r" +
    "\n" +
    "                        <div toggle-question class=\"question-right\" question=\"question2\"></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"column-question\">\r" +
    "\n" +
    "                        <div toggle-question class=\"question-left\" question=\"question3\"></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/charts.html',
    "<div class=\"overview-container home-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container overview home no-padding\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row chart white\">\r" +
    "\n" +
    "                    <h3 class=\"chart-title\">Onderwerpen</h3>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div result-bars class=\"result-bars\" results=\"allResults\"></div>\r" +
    "\n" +
    "                    <div result-bars class=\"result-bars mobile\" results=\"allResults\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"chart-legend\" ng-show=\"false\">\r" +
    "\n" +
    "                        <div class=\"chart-icons\">\r" +
    "\n" +
    "                            <div class=\"icon-field like\">\r" +
    "\n" +
    "                                <h3>Eens</h3>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div class=\"icon-field dislike\">\r" +
    "\n" +
    "                                <h3>Oneens</h3>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <h2>Zoveel mensen hebben de app ook ingevuld.</h2>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div cool-voters active=\"false\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <h2>Daarvan gaan deze mensen stemmen.</h2>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div cool-voters active=\"true\"></div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"column-button\">\r" +
    "\n" +
    "                        <div toggle-question class=\"question-red\" question=\"question1\" button-class=\"'data'\" important=\"true\"></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <h3>Voor meer informatie ga naar <a ng-href=\"http://www.brabant.nl/jongeren\">brabant.nl/jongeren</a></h3>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/einde.html',
    "<div class=\"home-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container home\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"col-sm-12 \">\r" +
    "\n" +
    "                    <div class=\"text-container full\">\r" +
    "\n" +
    "                        <h1>{{ 'end.thanks.text' | translate }} {{ user.name }}</h1>\r" +
    "\n" +
    "                        <!--<h2 ng-bind-html=\"trustAsHtml('end.website.text')\"></h2>-->\r" +
    "\n" +
    "                        </br></br>\r" +
    "\n" +
    "                        <h2 ng-bind-html=\"trustAsHtml('end.social.text')\"></h2>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"text-container sponsor\">\r" +
    "\n" +
    "                        <a href='http://www.vitaalvechtdal.nl/' target='_blank'><div class=\"logo-vitaal-vechtdal\"></div></a>\r" +
    "\n" +
    "                        <h2 ng-bind-html=\"trustAsHtml('end.sponsor.text')\"></h2>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/election.html',
    "<div class=\"election-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container election\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div id=\"tinderslide\" tinder-animation animation-speed=\"100\" animation-revert-speed=\"100\" threshold=\"2\" menu=\"'stemmen'\">\r" +
    "\n" +
    "                    <ul>\r" +
    "\n" +
    "                        <li>\r" +
    "\n" +
    "                            <div class=\"image-container\" image-thumb image-url=\"'Foto-ga-jij-stemmen-.jpg'\"></div>\r" +
    "\n" +
    "                            <div class=\"gradient-container\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <div class=\"text-container\"><h1>En nu de vraag aan jou, {{ user.name }}: Ga jij 18 maart stemmen voor de provinciale statenverkiezingen?</h1></div>\r" +
    "\n" +
    "                        </li>\r" +
    "\n" +
    "                    </ul>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"info-container\" ng-show=\"false\">\r" +
    "\n" +
    "                        <div id=\"button_info\" class=\"btn-info\" ng-click=\"getInfo()\"><span id=\"span_info\">i</span></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/home.html',
    "<div class=\"home-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container home\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"col-sm-12 \">\r" +
    "\n" +
    "                    <div class=\"home-icon-container-holder\">\r" +
    "\n" +
    "                        <div class=\"icon-container\">\r" +
    "\n" +
    "                            <div icon-font icon-class=\"icon-drugs\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"icon-container\">\r" +
    "\n" +
    "                            <div icon-font icon-class=\"icon-alcohol\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"icon-container\">\r" +
    "\n" +
    "                            <div icon-font icon-class=\"icon-smoking\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"icon-container\">\r" +
    "\n" +
    "                            <div icon-font icon-class=\"icon-intro\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"text-container\">\r" +
    "\n" +
    "                        <div class=\"col-sm-12\">\r" +
    "\n" +
    "                            <h2 class=\"low\">{{ 'home.teaser' | translate }}</h2>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/info.html',
    "<div class=\"user-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container user\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"form-container\">\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <h1>{{ 'info.title' | translate }}</h1>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"column-button\">\r" +
    "\n" +
    "                            <form name=\"userForm\" novalidate>\r" +
    "\n" +
    "                                <fieldset>\r" +
    "\n" +
    "                                    <div class=\"field-container\">\r" +
    "\n" +
    "                                        <div icon-font icon-class=\"icon-name\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                                        <input type=\"text\" placeholder=\"{{'info.form.placeholder.name' | translate}}\" ng-model=\"user.name\"/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"field-container\">\r" +
    "\n" +
    "                                        <div icon-font icon-class=\"icon-email\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                                        <input type=\"text\" placeholder=\"{{'info.form.placeholder.email' | translate}}\" ng-model=\"user.email\" required/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"field-container\">\r" +
    "\n" +
    "                                        <div icon-font icon-class=\"icon-age\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                                        <input type=\"text\" placeholder=\"{{'info.form.placeholder.age' | translate}}\" ng-model=\"user.age\" min=\"0\" max=\"99\" ng-pattern=\"/^[0-9]{1,3}$/\" required/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"field-container\">\r" +
    "\n" +
    "                                        <div icon-font icon-class=\"icon-gender\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                                        <button class=\"gender-button\" type=\"button\" ng-click=\"selectGender('male')\" ng-class=\"{active: (user.gender === 'male')}\"><span>{{'info.form.placeholder.gender.male' | translate}}</span></button>\r" +
    "\n" +
    "                                        <button class=\"gender-button female\" type=\"button\" ng-click=\"selectGender('female')\" ng-class=\"{active: (user.gender === 'female')}\"><span>{{'info.form.placeholder.gender.female' | translate}}</span></button>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                    <div class=\"field-container\">\r" +
    "\n" +
    "                                        <div icon-font icon-class=\"icon-location\" class=\"icon-field\"></div>\r" +
    "\n" +
    "                                        <input type=\"text\" placeholder=\"{{'info.form.placeholder.city' | translate}}\" ng-model=\"user.city\" required/>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </fieldset>\r" +
    "\n" +
    "                            </form>\r" +
    "\n" +
    "                            <div toggle-question question=\"question1\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/login.html',
    "<div class=\"login-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container login\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <h1>Login met je social account</h1>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"column-button\">\r" +
    "\n" +
    "                        <button class=\"text-center\" ng-click=\"loginFacebook()\"><span>Facebook</span></button>\r" +
    "\n" +
    "                        <button class=\"text-center\" ng-click=\"loginTwitter()\"><span>Twitter</span></button>\r" +
    "\n" +
    "                        <button class=\"text-center\" ng-click=\"loginInstagram()\"><span>Instagram</span></button>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/opinion.html',
    "<div class=\"user-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container user opinion\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"form-container\">\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <h1>Wil jij nog iets anders kwijt?</h1>\r" +
    "\n" +
    "                        <p>Naast de onderwerpen die je net voorbij zag komen wil je misschien nog iets anders meegeven!</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"row\">\r" +
    "\n" +
    "                        <div class=\"column-button\">\r" +
    "\n" +
    "                            <form name=\"opinionForm\" novalidate>\r" +
    "\n" +
    "                                <fieldset>\r" +
    "\n" +
    "                                    <div class=\"textarea-container\">\r" +
    "\n" +
    "                                        <textarea type=\"text\" placeholder=\"Plaats een opmerking\" ng-model=\"user.remarks\"></textarea>\r" +
    "\n" +
    "                                    </div>\r" +
    "\n" +
    "                                </fieldset>\r" +
    "\n" +
    "                            </form>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                            <!--<div toggle-question class=\"question-left\" question=\"question1\" button-class=\"'skip'\"></div>-->\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/overview.html',
    "<div class=\"overview-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container overview\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <div class=\"row info\">\r" +
    "\n" +
    "                    <h1>Bedankt {{ user.name }} voor het meedoen.</br>Hieronder een overzicht van wat jij belangrijk\r" +
    "\n" +
    "                        vindt.</h1>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <p class=\"text\">Je kunt drie onderwerpen die je extra belangrijk vindt aanvinken. Dit doe je door middel van de sterren voor het onderwerp.</p>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div class=\"statement-container\">\r" +
    "\n" +
    "                        <div ng-repeat=\"vote in likedVotes\">\r" +
    "\n" +
    "                            <div class=\"rating-button\" ng-click=\"toggleVoteImportant(vote)\">\r" +
    "\n" +
    "                                <div class=\"btn-rating\" ng-class=\"{active: vote.important}\"></div>\r" +
    "\n" +
    "                                <h2 ng-class=\"{even: $event, odd: $odd}\">{{ vote.statement.name }}</h2>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <!--<h2>Wil jij meebeslissen over de onderwerpen die jij belangrijk vindt? Ga dan naar <a\r" +
    "\n" +
    "                            ng-href=\"http://www.brabant.nl/verkiezingen\">www.brabant.nl/verkiezingen</a></h2>-->\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"column-button\">\r" +
    "\n" +
    "                        <div toggle-question question=\"question1\" button-class=\"'data'\" important=\"true\"></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/popup.html',
    "<div class=\"popup-container popup\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container popup\">\r" +
    "\n" +
    "            <div toggle-info info=\"info\" type=\"burger\"></div>\r" +
    "\n" +
    "            <div social-post></div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/reminder.html',
    "<div class=\"home-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container home reminder\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "                <h1>Wil je een reminder instellen voor de provinciale staten verkiezingen?</h1>\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <div class=\"column-question\">\r" +
    "\n" +
    "                        <div toggle-question class=\"question-right\" question=\"question1\" button-class=\"'email'\" field-value=\"user.email\"></div>\r" +
    "\n" +
    "                        <div toggle-question class=\"question-right\" question=\"question3\" button-class=\"'phone'\" field-value=\"user.phone\"></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                    <div class=\"column-question\">\r" +
    "\n" +
    "                        <div toggle-question class=\"question-left\" question=\"question2\" button-class=\"'agenda'\"></div>\r" +
    "\n" +
    "                        <div toggle-question class=\"question-left\" question=\"question4\" button-class=\"'skip'\"></div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/result.html',
    "<div class=\"result-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container result\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row home-background\">\r" +
    "\n" +
    "                    <div icon-font icon-class=\"{{ profile.icon }}\"></div>\r" +
    "\n" +
    "                    <div class=\"text\">\r" +
    "\n" +
    "                        <h1>{{ profile.label  }}</h1>\r" +
    "\n" +
    "                        <h2>{{ numCorrectAnswers }} van de {{ numQuestions }} goed.</h2>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row background\">\r" +
    "\n" +
    "                    <h3>{{ 'result.intro.teaser.text' | translate }}</h3>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div social-share class=\"social-container\" menu=\"menu\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <h2 ng-bind-html=\"trustAsHtml('result.intro.share.text')\"></h2>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row white\">\r" +
    "\n" +
    "                    <h2>{{ 'result.all.profiles.text' | translate }}</h2>\r" +
    "\n" +
    "                    <div ng-repeat=\"profile in profiles\" class=\"result-block\">\r" +
    "\n" +
    "                        <div class=\"result-block-icon\">\r" +
    "\n" +
    "                            <div icon-font icon-class=\"{{ profile.icon }}\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                        <div class=\"result-block-text\">\r" +
    "\n" +
    "                            <div class=\"text\">\r" +
    "\n" +
    "                                <h2>{{ profile.label }}</h2>\r" +
    "\n" +
    "                                <h3>{{ profile.description }}</h3>\r" +
    "\n" +
    "                            </div>\r" +
    "\n" +
    "                            <div ng-if=\"profile.userCount > 0\" profile-score profile=\"profile\"></div>\r" +
    "\n" +
    "                        </div>\r" +
    "\n" +
    "                    </div>\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/statement.html',
    "<div class=\"statement-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "        <div class=\"container statement-wrapper\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div id=\"tinderslide\" tinder-animation ng-show=\"currentIndex < answers.length\" animation-speed=\"100\" animation-revert-speed=\"100\" threshold=\"1\" menu=\"'onderwerp'\">\r" +
    "\n" +
    "                <ul>\r" +
    "\n" +
    "                    <li id=\"answerB\" class=\"statementLi\" ng-show=\"answerB\" statement-view answer=\"answerB\"></li>\r" +
    "\n" +
    "                    <li id=\"answerA\" class=\"statementLi\" ng-show=\"answerA\" statement-view answer=\"answerA\"></li>\r" +
    "\n" +
    "                </ul>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );


  $templateCache.put('html/pages/total.html',
    "<div class=\"overview-container\">\r" +
    "\n" +
    "    <div class=\"wrapper clearfix\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "        <div class=\"container overview\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "            <div class=\"full-width-sm\">\r" +
    "\n" +
    "\r" +
    "\n" +
    "                <div class=\"row\">\r" +
    "\n" +
    "                    <h2>Zoveel mensen hebben de app ook ingevuld.</h2>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div cool-voters active=\"false\"></div>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <!--<h2>Daarvan gaan deze mensen stemmen.</h2>\r" +
    "\n" +
    "\r" +
    "\n" +
    "                    <div cool-voters active=\"true\"></div>-->\r" +
    "\n" +
    "                </div>\r" +
    "\n" +
    "            </div>\r" +
    "\n" +
    "        </div>\r" +
    "\n" +
    "    </div>\r" +
    "\n" +
    "</div>\t\r" +
    "\n" +
    "\r" +
    "\n"
  );

}]);
