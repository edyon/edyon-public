"use strict";

/********* CONSTANTS *********/
var bcConstants = angular.module('bc.constants', []);
var bcServices = angular.module('bc.services', []);
var bcControllers = angular.module('bc.controllers', []);
var bcDirectives = angular.module('bc.directives', []);
var bcAnimations = angular.module('bc.animations', []);
var bcFilters = angular.module('bc.filters', []);

var bcApp = angular.module('bcApp', [
    'ngRoute',
    'ngResource',
    'ngCookies',
    'ngAnimate',
    'ngTouch',
    'pascalprecht.translate',
    'bc.constants',
    'bc.services',
    'bc.controllers',
    'bc.directives',
    'bc.filters',
    'bc.animations',
    'ui.bootstrap'
]);

bcApp.config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', '$translateProvider', '$translatePartialLoaderProvider',
    function ($routeProvider, $locationProvider, $sceDelegateProvider, $translateProvider, $translatePartialLoaderProvider) {

        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'http://api.coen.scuro.nl/*'
        ]);

        var mainLayout = {
            header: 'html/layout/header.html',
            footer: 'html/layout/footer.html'
        };

        var checkUser = ['DataService', function (DataService) {
            return DataService.checkUser();
        }];

        var checkSurvey = ['DataService', function (DataService) {
            return DataService.checkSurvey();
        }];


        $routeProvider.
            when('/home', _.extend({
                    templateUrl: 'html/pages/home.html',
                    controller: 'HomeController',
                    name: 'home'
                },
                mainLayout)).
            when('/about', _.extend({
                    templateUrl: 'html/pages/about.html',
                    controller: 'AboutController',
                    name: 'about'
                },
                mainLayout)).
            when('/gegevens', _.extend({
                    templateUrl: 'html/pages/info.html',
                    controller: 'InfoController',
                    name: 'gegevens'
                },
                mainLayout)).
            when('/onderwerp', _.extend({
                    templateUrl: 'html/pages/statement.html',
                    controller: 'StatementController',
                    name: 'onderwerp',
                    resolve: {
                        checkUser: checkUser,
                        checkSurvey: checkSurvey
                    }
                },
                mainLayout)).
            when('/onderwerp/:statementId', _.extend({
                    templateUrl: 'html/pages/statement.html',
                    controller: 'StatementController',
                    name: 'onderwerp',
                    resolve: {
                        checkUser: checkUser,
                        checkSurvey: checkSurvey
                    }
                },
                mainLayout)).
            when('/resultaat', _.extend({
                    templateUrl: 'html/pages/result.html',
                    controller: 'ResultController',
                    name: 'resultaat',
                    resolve: {
                        checkUser: checkUser
                    }
                },
                mainLayout)).
            when('/einde', _.extend({
                    templateUrl: 'html/pages/einde.html',
                    controller: 'EndController',
                    name: 'einde',
                    resolve: {
                        checkUser: checkUser
                    }
                },
                mainLayout)).
            otherwise({
                redirectTo: '/home'
            });

        $locationProvider.html5Mode(false);

        $translateProvider.useSanitizeValueStrategy('escaped');
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: 'locale/{lang}/{part}.json'
        });

        $translateProvider.preferredLanguage("nl");
        $translatePartialLoaderProvider.addPart('hardenberg');


    }]).run(['$rootScope', '$route', '$httpBackend', function ($rootScope, $route, $httpBackend) { // app is running

    $rootScope.layoutPartial = function (partialName) {
        if ($route && $route.current) {
            return $route.current[partialName];
        }
    };
}]);

;(function () {
    'use strict';
    bcApp.controller('AboutController', ['$log', '$scope', 'DataService', 'MenuService', 'RestService',
        function ($log, $scope, DataService, MenuService, RestService) {
            $scope.user = null;

            $scope.questions = MenuService.getQuestionsByMenu("about");
            $scope.question1 = $scope.questions[0];
            $scope.question2 = $scope.questions[1];
            $scope.question3 = $scope.questions[2];
            $scope.question4 = $scope.questions[3];
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('ChartsController', ['$log', '$scope', '$routeParams', '$location', 'RestService', 'DataService', 'HighchartService', 'MenuService',
        function ($log, $scope, $routeParams, $location, RestService, DataService, HighchartService, MenuService) {

            $scope.results = [];
            $scope.allResults = [];
            $scope.likedVotes = [];

            $scope.questions = MenuService.getQuestionsByMenu("resultaat");
            $scope.question1 = $scope.questions[0];

            var getResults = function () {

                RestService.getCompleteSurvey().then(function (questionnaire) {
//console.log("complete survey = ",  survey);
                    if (survey) {
                        RestService.getThemeScore(survey.id).then(function (scores) {
                            var allResults = scores;
//console.log("average score results = ", $scope.allResults);
                            if (allResults) {
                                _.sortBy(allResults, function(result) {
                                    return result.agreeCount;
                                });

                                $scope.allResults = allResults;
                            }
                        });
                    }
                }, function () {
                    // handle 401 error
                    RestService.getThemeScore(0).then(function (scores) {
                        var allResults = scores;
//console.log("average score results = ", $scope.allResults);
                        if (allResults) {
                            _.sortBy(allResults, function(result) {
                                return result.agreeCount;
                            });

                            $scope.allResults = allResults;
                        }
                    });
                });
            };

            getResults();
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('EndController', ['$scope', '$rootScope', '$routeParams', '$filter', '$sce', 'checkUser', 'RestService', 'MenuService',
        function ($scope, $rootScope, $routeParams, $filter, $sce, checkUser, RestService, MenuService) {
            $scope.user = checkUser;

            $scope.trustAsHtml = function (html) {
                var translated = $filter('translate')(html);
                return $sce.trustAsHtml(translated);
            };

        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('ElectionController', ['$scope', '$rootScope', '$routeParams', '$location', 'checkUser', 'RestService', 'MenuService',
        function ($scope, $rootScope, $routeParams, $location, checkUser, RestService, MenuService) {
            $scope.user = checkUser;


            var saveElectionUser = function () {
                RestService.savePerson($scope.user).then(function () {
                    $location.path("overzicht");
                }, function () {
                    $location.path("overzicht");
                });
            };

            $scope.$on('election:agree', function(event, args) {
                $scope.user.fulfillsDuty = true;
                saveElectionUser();
            });

            $scope.$on('election:disagree', function(event, args) {
                $scope.user.fulfillsDuty = false;
                saveElectionUser();
            });


        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('HomeController', ['$log', '$scope', 'DataService', 'RestService',
        function ($log, $scope, DataService, RestService) {
            $scope.user = null;

            DataService.setQuestionnaire(null);
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('InfoController', ['$log', '$scope', '$rootScope', '$location', 'RestService', 'MenuService', 'DataService',
        function ($log, $scope, $rootScope, $location, RestService, MenuService, DataService) {
            $scope.user = {};

            $scope.questions = MenuService.getQuestionsByMenu("gegevens");
            $scope.question1 = $scope.questions[0];

            var getUser = function () {
                RestService.getLoggedUser().then(function (user) {
                    var success = user && user != "null";
                    if (success) {
                        $scope.user = user;
                    } else {
                       $scope.user = {};
                    }
                }, function () { // handle 401 not authorised
                    $scope.user = {};
                });
            };

            getUser();

            /**
             * watch if the form is valid
             */
            $scope.$watch('userForm.$valid', function(newVal) {
                $scope.isValid = newVal;
                updateValidation($scope.isValid);
            });

            $scope.selectGender = function (gender) {
                $scope.user.gender = gender;
                updateValidation($scope.userForm.$valid);
            };

            var updateValidation = function (valid) {
                $rootScope.$broadcast('person:validate', {valid: valid}); // send event to parent scopes
            };

            updateValidation($scope.isValid);

            /**
             * save person when pressing navigation next button
             */
            $scope.$on("save-person", function () {
                if (!$scope.user.id) {
                    RestService.loginPerson($scope.user).then(function (user) { // create a new user
                        $scope.user = user;
                        $rootScope.$broadcast('person-complete');
                    });
                } else {
                    $rootScope.$broadcast('person-complete');
                }
            });
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('LoginController', ['$log', '$scope', '$location', 'checkUser', 'RestService',
        function ($log, $scope, $location, checkUser, RestService) {
            $scope.user = checkUser;

            $scope.loginFacebook = function () {
                RestService.getLoginWindow('facebook').then(function (data) {
                    if (data && data.url) {
                        var url = data.url;
                        window.open(url, "_self");
                    }
                });
            };

            $scope.loginTwitter = function () {

            };

            $scope.loginInstagram = function () {

            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('OpinionController', ['$log', '$scope', '$rootScope', '$location', 'checkUser', 'RestService', 'MenuService',
        function ($log, $scope, $rootScope, $location, checkUser, RestService, MenuService) {
            $scope.user = checkUser;

            $scope.$watch('opinionForm.$valid', function(newVal) {
//console.log("update user = ", $scope.user, newVal);
                updateValidation(newVal);
            });

            $scope.$on("save-opinion", function () {
                if ($scope.user.remarks != "") {
                    RestService.savePerson($scope.user).then(function (user) {

                        //$scope.user = user;
//console.log("saved user", user);

                    });
                }
            });

            var updateValidation = function (valid) {
                $rootScope.$broadcast('person:validate', {valid: valid}); // send event to parent scopes
            };

            updateValidation(false);


            $scope.questions = MenuService.getQuestionsByMenu("opinion");
            $scope.question1 = $scope.questions[0];

        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('OverviewController', ['$log', '$scope', '$routeParams', '$location', 'checkUser', 'RestService', 'DataService', 'HighchartService', 'MenuService',
        function ($log, $scope, $routeParams, $location, checkUser, RestService, DataService, HighchartService, MenuService) {
            $scope.user = checkUser;
            $scope.results = [];
            $scope.allResults = [];
            $scope.likedVotes = [];

            $scope.questions = MenuService.getQuestionsByMenu("overzicht");
            $scope.question1 = $scope.questions[0];

            $scope.themeInfo = {
                name: ''
            };
            $scope.showTheInfo = true;

            var selectThemeInfo = function (name) {
                if ($scope.themeInfo.name != name) {
                    var scoreInfo = _.find($scope.results, function (score, i) {
                        return score.themeDto.name  === name;
                    });
                    $scope.themeInfo = {
                        name: scoreInfo.themeDto.name,
                        description: info
                    };

                    $scope.showTheInfo = true;
//console.log("open theme info in controller = ", $scope.themeInfo, " - show me now = ", $scope.showTheInfo);
                }
            };

            //var info = '<p>Je geeft aan dat {{ theme.name }} voor jou een belangrijk onderwerp is. Naast de onderwerpen die je voorbij hebt zien komen spelen er natuurlijk nog meet zaken rondom onderwijs. Wil je weten welke partijen onderwijs ook erg belangrijk vinden? Vul de stemwijzer in en ontdek welke partijen het beste bij jou passen.</p>';
            var info = "test";
            $scope.$on('chart:info', function (event, data) {
                var name = data.category;
                selectThemeInfo(name);
            });

            $scope.numImportant = 0;
            $scope.toggleVoteImportant = function (vote) {
//console.log("toggle vote important = ", vote.important, " -- num ", $scope.numImportant);
                if (vote.important) {
                    if ($scope.numImportant > 0) {
                        $scope.numImportant--;
                        toggleVote(vote);
                    }
                } else {
                    if ($scope.numImportant < 3) {
                        $scope.numImportant++;
                        toggleVote(vote);
                    }
                }
            };

            var toggleVote = function (vote) {
                vote.important = vote.important != null ? !vote.important : true;

                RestService.answerQuestion(vote).then(function (vote) {
                    // save vote complete
                });
            };

            var getResults = function () {
                $scope.showTheInfo = false;

                var dataAgree;
                var dataDisagree;

                RestService.getCompleteSurvey().then(function (questionnaire) {
console.log(survey);
                    $scope.likedVotes = _.filter(survey.answers, function(vote) {
                        return vote.agree === true;
                    });

                    $scope.numImportant = 0;
                    _.each(survey.answers, function(vote) {
                        if (vote.agree === true) {
                            if (vote.important === true) {
                                $scope.numImportant++;
                            }
                        }
                    });


//console.log("complete survey = ",  survey);
                    if (survey) {
                        RestService.getThemeScore(survey.id).then(function (scores) {
                            var allResults = scores;
console.log("average score results = ", $scope.allResults);
                            if (allResults) {

                                _.sortBy(allResults, function(result) {
                                    return result.agreeCount;
                                });

                                $scope.allResults = allResults;
                                dataAgree = HighchartService.convertToScore($scope.allResults, true);
                                dataDisagree = HighchartService.convertToScore($scope.allResults, false);
                                $scope.chartConfigAll = HighchartService.getChartConfig(dataAgree, dataDisagree, false, "column");
                                $scope.chartConfigAllMobile = HighchartService.getChartConfig(dataAgree, dataDisagree, false, "bar");
                            }
                        });
                    }
                }, function () {
                    // handle 401 error
                    $location.path("home");
                });
            };

            getResults();


        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('ReminderController', ['$log', '$scope', 'checkUser', 'MenuService', 'RestService',
        function ($log, $scope, checkUser, MenuService, RestService) {
            $scope.user = checkUser;

            var savePerson = function () {
//console.log("save person = ", $scope.user);
                RestService.savePerson($scope.user).then(function () {

                });
            };

            $scope.$on('save-reminder', function () {
                savePerson();
            });


            $scope.questions = MenuService.getQuestionsByMenu("reminder");
            $scope.question1 = $scope.questions[0];
            $scope.question2 = $scope.questions[1];
            $scope.question3 = $scope.questions[2];
            $scope.question4 = $scope.questions[3];
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('ResultController', ['$log', '$scope', '$routeParams', '$location', '$filter', '$sce', 'RestService', 'DataService', 'HighchartService', 'MenuService',
        function ($log, $scope, $routeParams, $location, $filter, $sce, RestService, DataService, HighchartService, MenuService) {
            $scope.results = [];
            $scope.allResults = [];
            $scope.likedVotes = [];

            $scope.menus = MenuService.getMenu();
            $scope.menu = $scope.menus[$scope.menus.length - 1];

            var getResults = function () {

                RestService.getCompleteSurvey().then(function (survey) {
                    $scope.survey = survey;

                    if (survey) {
                        setUserProfile();
                    }
                }, function (error) { // handle 401 error

                });
            };

            var setUserProfile = function () {
                var correctAnswers = _.filter($scope.survey.answers, function (answer) {
                    return answer.correct === true;
                });
                $scope.numQuestions = $scope.survey.answers.length;
                $scope.numCorrectAnswers = correctAnswers.length;
                $scope.profileScore = MenuService.getProfileScore($scope.numCorrectAnswers, $scope.numQuestions);
//console.log("set results, correct answers = ", $scope.numCorrectAnswers, " out off = ", $scope.numQuestions);

                getAllResults();
            };

            var getAllResults = function () {
                $scope.scores = [];

                RestService.getScore(0, 0).then(function (score) {
                    $scope.scores.push(score);
                    $scope.userCount = Number(score);

                    RestService.getScore(0, 4).then(function (score) {
                        $scope.scores.push(score);
                        $scope.userCount += Number(score);

                        RestService.getScore(5, 9).then(function (score) {
                            $scope.scores.push(score);
                            $scope.userCount += Number(score);

                            RestService.getScore(10, 14).then(function (score) {
                                $scope.scores.push(score);
                                $scope.userCount += Number(score);

                                RestService.getScore(15, 15).then(function (score) {
                                    $scope.scores.push(score);
                                    $scope.userCount += Number(score);

                                    setAllProfiles();
                                });
                            });
                        });
                    });
                });
            };

            var setAllProfiles = function () {
                $scope.profile = MenuService.getProfileByScore($scope.profileScore, $scope.profiles);
                MenuService.setShareText($scope.profile, $scope.numQuestions, $scope.numCorrectAnswers);

                DataService.setProfile($scope.profile);
//console.log("set personal profile = ", $scope.profileScore, " profile = ", $scope.profile);

                var profiles = MenuService.getProfiles();
                $scope.profiles = MenuService.getProfilesWithScore(profiles, $scope.userCount, $scope.scores);
//console.log("set all person profiles = ", $scope.profiles, " out of all persons = ", $scope.scores.length);
            };

            getResults();

            $scope.trustAsHtml = function (html) {
                var translated = $filter('translate')(html);
                return $sce.trustAsHtml(translated);
            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('StatementController', ['$log', '$scope', '$rootScope', '$routeParams', '$location', '$timeout', 'checkUser', 'checkSurvey', 'RestService', 'DataService', 'MenuService',
        function ($log, $scope, $rootScope, $routeParams, $location, $timeout, checkUser, checkSurvey, RestService, DataService, MenuService) {
            $scope.user = checkUser;
            $scope.survey = checkSurvey;
            $scope.answers = null;
            $scope.answerA = null;
            $scope.answerB = null;
            $scope.currentAnswer = null;
            $scope.currentIndex = 0;
            $scope.currentPane = 0;
            $scope.testSelected = false;

            // reset the pane
            $scope.resetPane = function (current) {
                $('#answerA').css('transform', 'translate(0px, 0px)');
                $('#answerA').css('display', 'block');
                $('#answerB').css('transform', 'translate(0px, 0px)');
                $('#answerB').css('display', 'block');

//console.log("reset pane ", current.questionId, " == ", $scope.answerA.questionId, $scope.answerB.questionId);
                if ($scope.answerA && current == $scope.answerA) {
                    $('#answerB').css('z-index', 0);
                    $('#answerA').css('z-index', 1);
                }
                else if ($scope.answerB && current == $scope.answerB) {
                    $('#answerA').css('z-index', 0);
                    $('#answerB').css('z-index', 1);
                }
            };

            var checkSurveyComplete = function () {
                if ($scope.currentIndex === $scope.answers.length) { // test complete
                    var nextMenu = MenuService.getAfterQuestionnaireMenu();
                    $location.path(nextMenu.url);
                    return true;
                }
                return false
            };

            /**
             * handle selecting current answer
             *
             * @param index
             */
            var selectCurrentAnswer = function (index) {
                $scope.currentIndex = index;
                if (!checkSurveyComplete()) {
                    $scope.nextAnswer = $scope.answers[index + 1];
                    $scope.answerA = $scope.answers[index];
                    $scope.currentAnswer = $scope.answerA;
                    $scope.answerB = $scope.nextAnswer;
                    $scope.resetPane($scope.currentAnswer);
//$log.log(">>> select current answer = ", $scope.currentAnswer, " index = ", $scope.currentIndex);
                    $scope.currentPane = 0;

                    if ($scope.currentAnswer.correct != null) {
                        $rootScope.$broadcast('question:answered');
                    }

                    DataService.setAnswer($scope.currentAnswer, $scope.currentIndex);
                }
            };

            /**
             * handle animating the current answer
             *
             * @param index
             */
            var animateCurrentAnswer = function (index) {
                $scope.currentIndex = index;

                if (!checkSurveyComplete()) {
                    if ($scope.answers) {
                        if (index < $scope.answers.length) {
                            $scope.nextAnswer = $scope.answers[index + 1];
                            if (!$scope.currentAnswer) { // initial situation
                                $scope.answerA = $scope.answers[0];
                                $scope.currentAnswer = $scope.answerA;
                                $scope.answerB = $scope.nextAnswer;
                                $scope.currentPane = 0;
                            }
                            else if ($scope.currentAnswer == $scope.answerA) {
                                $scope.answerA = $scope.nextAnswer;
                                $scope.currentAnswer = $scope.answerB;
                                $scope.currentPane = 1;
                            }
                            else {
                                $scope.answerB = $scope.nextAnswer;
                                $scope.currentAnswer = $scope.answerA;
                                $scope.currentPane = 0;
                            }
                        }
//$log.log(">>> animate current answer = ", $scope.currentAnswer, " index = ", $scope.currentIndex);
                        if ($scope.currentAnswer.correct != null) {
                            $rootScope.$broadcast('question:answered');
                        }

                        DataService.setAnswer($scope.currentAnswer, $scope.currentIndex, $scope.currentPane);
                    }
                }
            };

            var navigatePath = function (url) {
                $timeout(function() {
                    $location.path(url);
                }, 100);
            };

            // select the answer from route
            var setAnswerFromRoute = function () {
                $scope.answers = $scope.survey.answers;
                $scope.currentIndex = 0;

//console.log("set answer from route = ", $routeParams, " survey = ", $scope.survey);
                if ($routeParams.statementId != undefined) {
                    if (!$scope.currentAnswer) {
                        for (var i = 0; i < $scope.answers.length; i++) {
                            var answer = $scope.answers[i];
                            if (answer.questionId == $routeParams.statementId) {
                                $scope.currentIndex = i;
                                break;
                            }
                        }
                        selectCurrentAnswer($scope.currentIndex);
                    }
                } else {
                    for (var i = 0; i < $scope.answers.length; i++) {
                        if (!$scope.answers[i].correct) {
                            $scope.currentIndex = i;
                            break;
                        }
                    }
//$log.log("statement index found = ", $scope.currentIndex, " --- answers = ",  $scope.answers.length);
                    var currentAnswer = $scope.answers[$scope.currentIndex];
                    navigatePath("onderwerp/" + currentAnswer.questionId);
                }
            };

            setAnswerFromRoute();

            /**
             * go to the next answer
             */
            var goToNextAnswer = function (animate) {
                if ($scope.currentIndex <= $scope.answers.length) {
                    $scope.currentIndex++;
                }

                if (animate === true) {
                    animateCurrentAnswer($scope.currentIndex);
                } else {
                    selectCurrentAnswer($scope.currentIndex);
                }
            };

            $scope.voteStatement = function (animate) {
                goToNextAnswer(animate);

                var current = $scope.currentAnswer;
                if (!checkSurveyComplete) {
                    $timeout(function () {
                        $location.path('onderwerp/' + current.questionId);
                    }, 100);

                }
            };

            /**
             * save the vote and get the correct answer
             */
            var saveVote = function (index) {
                var answer = {answer: index};

                RestService.answerQuestion($scope.survey.id, $scope.currentAnswer.questionId, answer).then(function (data) {
                    $scope.currentAnswer.correct = (data.result === true);
//console.log(">>>>>>> question is answered is correct = ", $scope.currentAnswer);
                    $rootScope.$broadcast('question:answered');

                });
            };

            /**
             * handle thumb like/dislike from navigate directive
             */
            $scope.$on('save-question', function (event, args) {
                var isAnswered = $scope.currentAnswer.correct;
//console.log("save question, answer = ", $scope.currentAnswer, isAnswered);
                if (isAnswered == null) { // go answer the question
                    var answer = args.answer;
                    var delay = args.delay;
                    var timeout = delay ? 300 : 0;

                    $timeout(function () {
                        saveVote(answer);
                    }, timeout);

                } else {
                    $rootScope.$broadcast('question-complete');
                }
            });

            var resetCurrentStatement = function () {
                $timeout(function() {
                    selectCurrentAnswer($scope.currentIndex);
                });
            };

            /**
             * handle swipe left and right
             */
            $scope.$on('swipe-question', function (event, args) {
                var isAnswered = $scope.currentAnswer.correct;
//console.log("swipe question, answer = ", $scope.currentAnswer, isAnswered);
                if (isAnswered == null) { // go answer the question
                    if ($scope.currentAnswer.type == "multiple") {
                        resetCurrentStatement();
                    } else  if ($scope.currentAnswer.type == "statement") {
                        resetCurrentStatement();

                        $timeout(function () {
                            var answer = args.answer;
                            saveVote(answer);
                        }, 400);
                    }
                } else {
                    $rootScope.$broadcast('question-complete');
                }
            });

        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('TotalController', ['$log', '$scope', '$routeParams', 'checkUser', 'MenuService',
        function ($log, $scope, $routeParams, checkUser, MenuService) {
            $scope.user = checkUser;

        }
    ]);
}());
;(function () {
    'use strict';
    bcDirectives.directive('coenApp', function () {
        return {
            restrict: 'AE',
            replace: true,
            controller: ['$scope', '$rootScope', '$route', '$routeParams', '$window',
                function ($scope, $rootScope, $route, $routeParams, $window) {

                    $rootScope.$on('$routeChangeStart', function (evt, absNewUrl, absOldUrl) {
                        if ($route.current) {
//console.log("start = ", $route.current.$$route, $routeParams);
                        }
                        $window.scrollTo(0, 0);
                    });

                    $rootScope.$on('$routeChangeSuccess', function () {
//console.log("success = ", $route, $routeParams);
                        if ($route.current && $route.current.$$route) {
                            var routeParam = $route.current.$$route.name;
                            if ($route.current.params && $route.current.params.statementId) {
                                routeParam = routeParam + "/" + $route.current.params.statementId;
                            }
//console.log("go select menu from route = ", routeParam);
                            $rootScope.$broadcast('navigate', routeParam);
                        }
                    });

                    $scope.menu = null;
                    $scope.$on('menu:select', function (event, data) {
                        var menu = data.menu;
                        $scope.menu = menu;
//console.log("app selected menu = ", $scope.menu);
                    });

                }

            ],
            templateUrl: 'html/directives/appTemplate.html'

        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('bulletNavigate', ['$window', function ($window) {
        return {
            restrict: 'A',
            replace: false,
            scope: true,
            link: function (scope, element, attrs) {

                $($window).resize(function() {
                    if (scope.steps) {
                        scope.setBulletBarWidth(scope.steps.length);
                        scope.setBulletBarLeft(scope.bulletIndex, scope.steps.length);
                    }
                });

                scope.setBulletBarWidth = function (numBullets) {
                    var totalWidth = $(document).width();
                    var maxWidth = totalWidth * 1.83;
                    if (totalWidth < 768) {
                        totalWidth = maxWidth;
                    }
                    scope.totalWidth = totalWidth;
                    $(".bullet-bread-bar").css('width', totalWidth);

                    totalWidth = totalWidth - 11;

                    var stepWidth = totalWidth * (1 / numBullets);
                    scope.stepWidth = stepWidth;
                    $(".bullet-step").css('width', scope.stepWidth);
                    $(".bullet-bar-line").css('width', scope.stepWidth);
//console.log("bullet bar *** set width = ", stepWidth);
                };

                scope.setBulletBarLeft = function (bulletIndex, numBullets) {
                    var totalWidth = $(document).width();
                    var maxWidth = totalWidth * 1.83;
                    scope.totalWidth = maxWidth;
                    if (totalWidth < 768) {
                        scope.barLeft = (bulletIndex > (numBullets / 2)) ? (totalWidth - maxWidth) : 0;
                        $(".bullet-bread-bar").css('margin-left', scope.barLeft);

                        scope.moreLeft = (bulletIndex > (numBullets / 2)) ? 10 : (totalWidth - 34);
                        $(".icon-broken-line").css('left', scope.moreLeft);
//console.log("set left = ", scope.barLeft, scope.moreLeft, bulletIndex, numBullets);
                    } else {
                        $(".bullet-bread-bar").css('margin-left', 0);
                    }
                };

                if (!scope.survey) scope.setSteps(scope.survey);
            },
            controller: ['$log', '$scope', '$rootScope', '$route', '$timeout', 'DataService', 'MenuService',
                function ($log, $scope, $rootScope, $route, $timeout, DataService, MenuService) {
                    $scope.bulletIndex = 0;
                    $scope.survey = DataService.getSurvey();

                    // This function actually activates a bullet for the provided title
                    var setNextStepStyle = function (url) {
                        if (!url || !$scope.steps)
                            return;

                        _.each($scope.steps, function (step) {
                            step.active = (step.url === url);
                            if (step.active) {
                                $scope.bulletIndex = step.index + 1;
                            }
                        });

//console.log('---- BULLET display step: ', url, " bullet index = ", $scope.bulletIndex);
                        renderBulletBar();
                    };

                    var renderBulletBar = function () {
                        $timeout(function () {
                            $scope.setBulletBarWidth($scope.steps.length);
                            $scope.setBulletBarLeft($scope.bulletIndex, $scope.steps.length);
                        }, 1000);
                    };

                    // In case the app is reloaded while the current page is /onderwerp,
                    // find the first correct == undefined and use that index to mark the current step
                    var startFromRoute = function () {
                        var routeParam = $route.current ? $route.current.$$route.name : "";
                        if (routeParam == "onderwerp") {
                            var q = DataService.getSurvey();
                            if (q && q.answers) {
                                for (var i = 0; i < q.answers.length; i++) {
                                    if (q.answers[i].correct == undefined) {
                                        $scope.currentIndex = i;
                                        setNextStepStyle('onderwerp/' + q.answers[i].questionId);
                                        break;
                                    }
                                }
                            }
                        }
                        // otherwise use the current route to mark the current step
                        else {
                            setNextStepStyle(routeParam);
                        }
                    };

                    $scope.setSteps = function (survey) {
                        $scope.survey = survey;
                        var answers = $scope.survey ? $scope.survey.answers : [];
                        $scope.steps = MenuService.getMenuWithSteps(answers);
                        startFromRoute();
                    };

                    $rootScope.$on('survey', function (event, args) {
                        $scope.setSteps(args.survey);
                    });

                    // handle navigating between routes
                    $scope.$on('navigate', function (event, args) {
//console.log('------------------ navigate '+ args);
                        setNextStepStyle(args);
                    });

                    $scope.$on('menu:select', function (event, args) {
                        $scope.menu = args.menu;
                    });

                    // handle navigate from statement
                    $scope.$on('next-statement', function (event, args) {
                        var answer = args.answer;
                        setNextStepStyle('onderwerp/' + answer.questionId);
                    });

                }
            ],
            templateUrl: 'html/directives/bulletTemplate.html'

        };
    }]);
}());;(function () {
    bcDirectives.directive('headerPopover', ['$compile', 'MenuService', function ($compile, MenuService) {

        return {
            restrict: "EA",
            transclude: true,
            replace: false,
            templateUrl: "html/directives/headerBurgerPopover.html",
            link: function (scope, element, attrs) {

                $('body').on('click', function (e) {
                    //only buttons
                    if ($(e.target).data('toggle') !== 'popover' && $(e.target).parents('.bc-popover.in').length === 0) {

                    }
                });

            },
            controller: ['$log', '$scope', '$rootScope', 'MenuService', function ($log, $scope, $rootScope, MenuService) {
                $scope.items = MenuService.getBurgerItems();

                $scope.toggleMenu = function () {
                    $rootScope.$broadcast('toggle:popup');
                };

                $scope.select = function (item) {
                    $rootScope.$broadcast('toggle:popup', {type: 'burger', info: item});
                };

                $scope.$on('menu:select', function (event, args) {
                    $scope.menu = args.menu;
                });

            }]
        };
    }
    ]);
}());
;(function () {
    bcDirectives.directive('headerLogo', ['$compile', 'MenuService', function ($compile, MenuService) {

        return {
            restrict: "EA",
            transclude: true,
            replace: false,
            templateUrl: "html/directives/headerLogo.html",
            controller: ['$log', '$scope', '$location', 'MenuService', function ($log, $scope, $location, MenuService) {

                $scope.goHome = function (item) {
                    $location.path("/home");
                };

                $scope.$on('menu:select', function (event, args) {
                    $scope.menu = args.menu;
                });

            }]
        };
    }
    ]);
}());
;(function () {
    // Original source: https://github.com/pablojim/highcharts-ng

    'use strict';
    bcDirectives.directive('highchart', ['$rootScope', 'HighchartService', function ($rootScope, HighchartService) {


        return {
            restrict: 'EAC',
            replace: true,
            template: '<div></div>',
            scope: {
                config: '=',
                barType: '='
            },
            link: function (scope, element, attrs) {
                var chart = null;
                var currentType = null;
                var viewWidth = 0;

                $(window).resize(function () {
                    initChart();
                });


                var getTypeForScreen = function () {
                    viewWidth = document.documentElement.clientWidth;
                    if (viewWidth > 0) {
                        var barType = viewWidth >= 768 ? "column" : "bar";
                    }
                    return barType;
                };

                var setChartCssProps = function () {
                    viewWidth = document.documentElement.clientWidth;
                    $('.chart-info.bar').css('width', viewWidth);
                };

                scope.$on('chart:animated', function (event, data) {
                    setChartCssProps();
                });

                /**
                 * set highchart theme
                 */
                var initChart = function () {
                    if (scope.config) {
                        var barType = getTypeForScreen();
                        if (currentType != barType && scope.barType === barType) {
//console.log("init chart = ", barType, " == ", scope.barType, " config = ", scope.config);

                            currentType = barType;
                            if (chart) {
                                chart.destroy();
                            }

                            var theme = HighchartService.getChartTheme(scope.barType);
                            Highcharts.theme = theme;
                            Highcharts.setOptions(theme);

                            var containerId = attrs.id;
                            scope.config.chart = {};
                            scope.config.chart.renderTo = containerId;
                            scope.config.chart.type = scope.barType;
                            chart = HighchartService.initialiseChart(scope, element, scope.config, theme);
                        }
                    }
                };

                initChart();

                scope.$watch("config.series", function (newSeries, oldSeries) {
                    if (chart) {
                        HighchartService.processSeries(chart, newSeries);
                        chart.redraw();
                    }
                }, true);


                scope.$watch("config.options", function (newOptions, oldOptions, scope) {
                    initChart();
                }, true);

                var handleInfoClick = function (event) {
                    //console.log("click info button id = ", event.currentTarget.id);

                    $rootScope.$broadcast('chart:info', {category: event.currentTarget.id});
                };

                scope.$on('chart:complete', function () {
                    $('.btn-info-chart').bind('click', handleInfoClick);
                })

                scope.$on('chart:animated', function () {
                    $('.btn-info-chart').bind('click', handleInfoClick);
                })

            },
            controller: ['$log', '$scope', function ($log, $scope) {

            }]
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('toggleInfo', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                info: '=',
                type: '@type'
            },
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', '$rootScope', function ($log, $scope, $rootScope) {
                $scope.showInfo = false;

                $scope.close = function () {
                    $scope.showInfo = false;
                    $rootScope.$broadcast("close-info-popup");
                };

                $scope.$on('toggle:popup', function(event, args) {
                    if (args) {
                        if (args.type == $scope.type) {
                            $scope.info = args.info;
                            $scope.showInfo = true;
                            return;
                        }
                    }

                    $scope.info = {};
                    $scope.showInfo = false;
                });

                $scope.$watch('info', function () {
                    if ($scope.info && $scope.info.description) {
                        $scope.showInfo = true;
                    } else {
                        $scope.showInfo = false;
                    }
                });
            }],
            templateUrl: 'html/directives/infoTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('footerNavigate', function () {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                'menu': '='
            },
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', '$rootScope', '$location', '$route', '$timeout', 'DataService', 'MenuService',
                function ($log, $scope, $rootScope, $location, $route, $timeout, DataService, MenuService) {
                    $scope.survey = DataService.getSurvey();

                    /**
                     * set the selected menu
                     *
                     * @param index
                     */
                    var setMenu = function (index) {
                        $scope.prevMenu = index - 1 > -1 ? $scope.steps[index - 1] : null;
                        $scope.menu = $scope.steps[index];
                        $scope.nextMenu = index + 1 < $scope.steps.length ? $scope.steps[index + 1] : $scope.steps[0];
//$log.log("navigate menu: set current menu = ", $scope.prevMenu, " || ", $scope.menu, " || ", $scope.nextMenu, " index = ", index);

                        if ($scope.menu) {
                            if ($scope.menu.title === 'gegevens' || $scope.menu.title === 'onderwerp') {
                                $scope.isValid = false;
                            } else {
                                $scope.isValid = true;
                            }
                            $rootScope.$broadcast('menu:select', {menu: $scope.menu});
                        }
                    };

                    var setRoute = function (routeParam) {
                        var menu = _.find($scope.steps, function (item) {
                            return item.url === routeParam;
                        });
                        $scope.index = _.indexOf($scope.steps, menu);
//console.log("set from route = ", $scope.steps, $scope.index, menu);
                        setMenu($scope.index);
                    };

                    var startFromRoute = function () {
                        if ($route.current) {
                            var routeParam = $route.current.$$route.name;
                            if ($route.current.params && $route.current.params.statementId) {
                                routeParam = routeParam + "/" + $route.current.params.statementId;
                            }
//console.log("route = ", routeParam);
                            setRoute(routeParam);
                        }
                    };

                    var setSteps = function (survey) {
                        $scope.survey = survey;
                        var answers = $scope.survey ? $scope.survey.answers : [];
                        $scope.steps = MenuService.getMenuWithSteps(answers);

                        startFromRoute();
                    };
                    if (!$scope.survey) setSteps($scope.survey);

                    $rootScope.$on('survey', function (event, args) {
                        setSteps(args.survey);
                    });

                    $scope.$watch('menu', function () {

                    });

                    var navigatePath = function (url) {
                        $timeout(function() {
                            $location.path(url);
                        }, 100);
                    };

                    $scope.selectPreviousMenu = function () {
                        navigateToPreviousMenu();
                    };

                    var navigateToPreviousMenu = function () {
                        if ($scope.index >= 0) {
                            $scope.index--;
                        }
                        setMenu($scope.index);
                        navigatePath($scope.menu.url);
                    };

                    /**
                     * handle selecting the next button
                     */
                    $scope.selectNextMenu = function () {
                        if ($scope.isValid) {
                            if ($scope.menu.title == "gegevens") {
                                $rootScope.$broadcast('save-person');
                                return;
                            }

                            navigateToNextMenu();
                        }
                    };

                    var navigateToNextMenu = function () {
                        if ($scope.index < ($scope.steps.length - 1)) {
                            $scope.index++;
                        } else {
                            $scope.index = 0;
                        }
                        setMenu($scope.index);
                        navigatePath($scope.menu.url);
                    };

                    $rootScope.$on('question-complete', function (event, data) {
                        navigateToNextMenu();
                    });

                    $rootScope.$on('person-complete', function (event, data) {
                        navigateToNextMenu();
                    });

                    /**
                     * handle navigate event
                     */
                    $scope.$on('navigate', function (event, data) {
                        var routeParam = data;
                        setRoute(routeParam);
                    });

                    $scope.rateStatement = function (agree) {
                        var routeParam = $route.current.$$route.name;
                        if (routeParam == "onderwerp") {
                            if (agree === true) {
                                $rootScope.$broadcast('save-question', {answer: 0, delay: false});
                            } else {
                                $rootScope.$broadcast('save-question', {answer: 1, delay: false});
                            }
                        }
                    };

                    $scope.$on('question:answered', function (event, data) {
                        $scope.menu.showNext = true;
                        $scope.menu.showVotes = false;
                        $scope.isValid = true;
//console.log("navigate: question answer = ", $scope.menu);
                    });

                    $scope.$on('person:validate', function (event, data) {
                        $scope.isValid = data.valid;
//console.log("navigate valid = ", $scope.isValid);
                    });

                }
            ],
            templateUrl: 'html/directives/navigateTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('iconFontLibrary', ['DataService', function (DataService) {
        return {
            restrict: 'AE',
            replace: false,
            templateUrl: 'html/directives/plugins/iconFontLibrary.html'
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('iconFont', ['DataService', function (DataService) {
        return {
            restrict: 'AE',
            replace: false,
            scope: {
                iconClass: "@"
            },
            controller: ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {
                $scope.$watch('iconClass', function () {
                    $scope.iconClass = $scope.iconClass || "";
                    $scope.iconLink = "#" + $scope.iconClass;
                });
            }],
            template: '<svg class="icon" ng-class="iconClass" ><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{iconLink}}"></use></svg>'
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('resizeIframe', function () {
        return {
            restrict: 'AE',
            replace: false,
            scope: false,
            link: function (scope, element, attrs, ngModel) {

// Find all iframes
                var $iframes = $( "iframe" );
console.log($iframes);
// Find &#x26; save the aspect ratio for all iframes
                $iframes.each(function () {
                    $( this ).data( "ratio", this.height / this.width )
                        // Remove the hardcoded width &#x26; height attributes
                        .removeAttr( "width" )
                        .removeAttr( "height" );
                });

// Resize the iframes when the window is resized
                $( window ).resize( function () {
                    $iframes.each( function() {
                        // Get the parent container&#x27;s width
                        var width = $( this ).parent().width();
                        $( this ).width( width )
                            .height( width * $( this ).data( "ratio" ) );
                    });
// Resize to fix all iframes on page load.
                }).resize();

            }
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('customScroll', function () {
        return {
            restrict: 'AE',
            replace: false,
            scope: false,
            link: function (scope, element, attrs, ngModel) {


                var initScroll = function () {
                    $(element).mCustomScrollbar({
//				scrollInertia:0, // put on to stop animation
                        verticalScroll: true,
                        mouseWheel: true,
                        scrollButtons: {
                            enable: true,
                            scrollSpeed: 80
                        },
                        autoHideScrollbar: false,
                        scrollInertia: 0,
//				contentTouchScroll: true,
                        theme: "fs",
                        advanced: {
                            updateOnContentResize: true,
                            autoExpandHorizontalScroll: true
                        },
                        callbacks: {
                            onScrollStart: function (scroll) {
                                // handle scroll start
                            },
                            onScroll: function (scroll) {

                            },
                            whileScrolling: function (scroll) {
                                //scope.updateScrollPosition(-scroll.left);
                            }
                        }
                    });
                };

                initScroll();

                var _scrollPosition = 0;
                scope.updateScrollPosition = function (pos) {
                    _scrollPosition = pos;
                };

                scope.updateScroll = function () {
                    $(element).mCustomScrollbar("update");
                };

                scope.scrollToDiv = function (divId) {
                    $(element).mCustomScrollbar("scrollTo", divId);
                };

                scope.scrollToPosition = function (pos) {
                    $(element).mCustomScrollbar("scrollTo", pos);
                };

            },
            controller: ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {

            }]
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('toggleQuestion', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                'question': '=',
                'buttonClass': '@',
                'important': '=',
                'fieldValue': '='
            },
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', '$rootScope', '$location', '$sce', function ($log, $scope, $rootScope, $location, $sce) {
                $scope.isOpen = false;
                $scope.activeClass = ($scope.isOpen === true) ? "active" : "";
                $scope.importantClass = ($scope.important === true) ? "important" : "";

                $scope.toggleQuestion = function () {
                    if (!$scope.isOpen) {
                        if ($scope.question.link) {
                            $location.path($scope.question.link);
                            return;
                        }
                    }


                    if (!$scope.isOpen) { // go open this question
                        $rootScope.$broadcast("question:reset");
                    }

//console.log("open this question");
                    $scope.isOpen = !$scope.isOpen;
                    $scope.activeClass = ($scope.isOpen === true) ? "active" : "";

                };

                //$scope.getHtmlAnswer = function (answer) {
                //    return $sce.trustAsHtml(answer);
                //};

                $scope.$on("question:reset", function() {
                    $scope.isOpen = false;
                    $scope.activeClass = ($scope.isOpen === true) ? "active" : "";
//console.log("reset other questions");
                });

            }],
            templateUrl: 'html/directives/questionTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('repeatReady', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                if (scope.$last === true) {
                    element.ready(function () {
                        scope.$emit('ngRepeatReady'); // send event to parent scopes
                    });
                }
            }
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('resultBars', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                results: "="
            },
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', function ($log, $scope) {

                $scope.$watch('results', function () {

                });

            }],
            templateUrl: 'html/directives/resultBarsTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('profileScore', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                'profile': '='
            },
            controller: ['$log', '$scope', '$location', '$route', 'RestService', 'DataService',
                function ($log, $scope, $location, $route, RestService, DataService) {
                    $scope.user = DataService.checkUser();

                }
            ],
            templateUrl: 'html/directives/scoreTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('socialShare', function () {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                'menu': '='
            },
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', '$rootScope', '$location', '$route', 'DataService', 'FacebookService', 'TwitterService', 'EmailService',
                function ($log, $scope, $rootScope, $location, $route, DataService, FacebookService, TwitterService, EmailService) {
                    $scope.user = DataService.getUser();
                    $scope.currentAnswer = DataService.getAnswer();
                    $scope.currentProfile = DataService.getProfile();
                    $scope.currentMenu = null;

                    $scope.socialTitles = [
                        {url: "onderwerp", title: "onderwerp"},
                        {url: "stemmen", title: "onderwerp"},
                        {url: "overzicht", title: "onderwerp"},
                        {url: "resultaat", title: "resultaat"},
                        {url: "einde", title: "einde"}
                    ];

                    $scope.getTitleForMenu = function () {
                        var shareTitle = _.find($scope.socialTitles, function(socialItem) {
                            return socialItem.title === $scope.currentMenu;
                        });
                        return shareTitle ? shareTitle.title : "";
                    };


                    // handle navigating between routes
                    $scope.$on('menu:select', function (event, args) {
//console.log('------------------ social directive navigate '+ args.menu);
                        var menu = args.menu.url;
                        $scope.currentMenu = menu;
                    });

                    var routeParam = $route.current.$$route.name;
                    $scope.currentMenu = routeParam;

                    $scope.$on('next-statement', function(event, args) {
                        var answer = args.answer;
                        $scope.currentAnswer = answer;
                    });

                    $scope.$on('profile', function(event, args) {
                        var profile = args.profile;
                        $scope.currentProfile = profile;
                    });

                    /**
                     * facebook
                     */
                    $scope.shareFacebook = function () {
                        //loginFacebook();

                        var socialPost = {};
                        if ($scope.currentMenu === "onderwerp") {
                            socialPost = FacebookService.getAnswerPost($scope.user, $scope.currentAnswer);
                        } else if ($scope.currentMenu === "resultaat") {
                            socialPost = FacebookService.getProfilePost($scope.user, $scope.currentProfile);
                        }

                        var message = FacebookService.getPostLight(socialPost);
                        FacebookService.publishPostLight(message);
                    };

                    var loginFacebook = function () {
                        FacebookService.checkLoginStatus().then(function (response) {
                            if (response) {
                                publishToFacebook(response);
                            } else {
                                FacebookService.goLogin().then(publishToFacebook);
                            }
                        });
                    };

                    var publishToFacebook = function (user) {
//console.log("go publish statement = ", $scope.currentAnswer, " - menu = ", $scope.currentMenu);
                        var socialPost = FacebookService.getPost(user, $scope.currentMenu, $scope.currentAnswer);
                        $rootScope.$broadcast('toggle:social-post', {post: socialPost});
                    };

                    /**
                     * twitter
                     */
                    $scope.shareTwitter = function () {
                        //loginTwitter();

                        var socialPost = {};
                        if ($scope.currentMenu === "onderwerp") {
                            socialPost = TwitterService.getAnswerPost($scope.user, $scope.currentAnswer);
                        } else if ($scope.currentMenu === "resultaat") {
                            socialPost = TwitterService.getProfilePost($scope.user, $scope.currentProfile);
                        }

                        var tweet = TwitterService.getPostLight(socialPost);
                        TwitterService.publishPostLight(tweet);
                    };

                    var loginTwitter = function () {
                        TwitterService.connectTwitter().then(function(response) {
                            if (TwitterService.isReady()) {
                                publishToTwitter(response);
                            }
                        });
                    };

                    var publishToTwitter = function (user) {
                        var socialPost = TwitterService.getPost(user, $scope.currentMenu, $scope.currentAnswer);
                        $rootScope.$broadcast('toggle:social-post', {post: socialPost});
                    };

                    /**
                     * email
                     */
                    $scope.shareEmail = function () {
                        //shareEmail();
                        shareEmailLight();
                    };

                    var shareEmail = function () {
                        var socialPost = EmailService.getPost(null, $scope.currentMenu, $scope.currentAnswer);
                        $rootScope.$broadcast('toggle:social-post', {post: socialPost});
                    };

                    var shareEmailLight = function () {
                        var socialPost = {};
                        if ($scope.currentMenu === "onderwerp") {
                            socialPost = EmailService.getAnswerPost($scope.user, $scope.currentAnswer);
                        } else if ($scope.currentMenu === "resultaat") {
                            socialPost = EmailService.getProfilePost($scope.user, $scope.currentProfile);
                        }

                        var email = EmailService.getEmailLight(socialPost);
                        EmailService.sendEmailLight(email);
                    };


                }
            ],
            templateUrl: 'html/directives/socialTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('socialPost', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: true,
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', 'FacebookService', 'TwitterService', 'RestService', function ($log, $scope, FacebookService, TwitterService, RestService) {
                $scope.showInfo = false;

                $scope.sharePost = function () {

                    if ($scope.post.platform == "facebook") {
                        FacebookService.publishPost($scope.post.message).then(function (response) {
//console.log("post to facebook done, response = ", response);
                            if (response.id) {
                                $scope.close();
                            }
                        });
                    } else if ($scope.post.platform == "twitter") {
                        TwitterService.publishPost($scope.post.message).then(function (response) {
//console.log("post to twitter done, response = ", response);
                            if (response.id) {
                                $scope.close();
                            }
                        });
                    } else if ($scope.post.platform == "email") {
                        var email = {
                            "emailAdress": $scope.post.email,
                            "subject": $scope.post.message.name,
                            "valueMap": {
                                "imgUrl": $scope.post.message.picture,
                                "body": $scope.post.message.name + "</br>" + $scope.post.message.description
                            }
                        };

                        RestService.sendEmail(email).then(function (response) {
console.log("email done, response = ", response);
                            $scope.close();
                        });
                    }
                };

                $scope.close = function () {
                    $scope.showInfo = false;
                };

                $scope.$on('toggle:social-post', function (event, data) {
                    $scope.post = data.post;
                    $scope.showInfo = true;
//console.log("toggle post = ", $scope.post, $scope.showInfo);
                });

            }],
            templateUrl: 'html/directives/postTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('statementView', function () {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                answer: '='
            },
            link: function (scope, element, attrs, ngModel) {

            },
            controller: ['$log', '$scope', '$rootScope', function ($log, $scope, $rootScope) {

                var setCorrectInfo = function () {
                    if ($scope.answer) {
                        $scope.info = {
                            description: $scope.answer.info,
                            correct: $scope.answer.correct
                        };
//console.log("------------------- set answer correct = ", $scope.answer, $scope.info);
                    }
                };

                $scope.$watch('statement', function () {
                    $scope.currentAnswer = null;
                    setCorrectInfo();
                });
                $scope.$watch('answer.correct', setCorrectInfo);


                $scope.getInfo = function () {
                    $rootScope.$broadcast('toggle:popup', {type: 'description', info: {description: $scope.answer.info}});
                };

                // handle navigate from statement
                $scope.$on('toggle:info', function (event, args) {
                    $scope.getInfo();
                });

                // handle navigate from statement
                $scope.$on('next-statement', function (event, args) {
                    //$rootScope.$broadcast('toggle:popup');
                });


                $scope.selectAnswer = function(statement, index) {
                    $scope.currentAnswer = index;
                    $rootScope.$broadcast('save-question', {answer: index, delay: true});
                };

                $scope.parseImageUrl = function (imageId) {
                    var imageUrl = imageId ? "question-" + imageId + ".jpg" : "";
                    return imageUrl;
                };
            }],
            templateUrl: 'html/directives/statementTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('textFormat', function () {

        return {
            restrict: 'A',
            replace: true,
            scope: {
                'text': '='
            },
            controller: ['$scope', '$sce', function ($scope, $sce) {
                $scope.$watch('text', function (newValue) {
                    if (newValue) {
                        // Display "< & >" in text.
                        //newValue = newValue.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/&/g, '&amp;');
                        //$scope.formatText = newValue.replace(/\n/g, "<br/>");


                        var urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/ig;

                        if (newValue.search(urlRegex) > -1) {
                            newValue = newValue.replace(urlRegex, function (url) {

                                var target = "_blank";
                                if (url.search('www') > -1) {
                                    var refTitle = "";
                                    var refLabel = url;
                                    if (url.search('http://') > -1) {
                                        return '<a title="' + refTitle + '" href="' + url + '" target="' + target + '">' + refLabel + '</a>';
                                    } else {
                                        return '<a title="' + refTitle + '" href="http://' + url + '" target="' + target + '">' + refLabel + '</a>';
                                    }
                                }
                            });
                        }
                        $scope.formatText = $sce.trustAsHtml(newValue);
                    }
//console.log($scope.formatText);
                });


            }],
            template: '<div class="text" ng-bind-html="formatText"></div>'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('themeView', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                'themeRating': '=',
                'favourite': '='
            },
            controller: ['$log', '$scope', 'RestService', function ($log, $scope, RestService) {
                $scope.checked = false;

                RestService.getTheme($scope.themeRating.themeId).then(function (theme) {
                    $scope.theme = theme;
$log.log("theme in directive = ", $scope.theme);
                    $scope.checked = $scope.themeRating.ratingValue >= 2;
$log.log("rating = ", $scope.themeRating, $scope.checked);
                });

                $scope.$watch('checked', function () {
$log.log("watch checked = ", $scope.checked, $scope.themeRating);
                    if (!$scope.themeRating.ratingValue === 3) {
                        $scope.themeRating.ratingValue = 2;
                    }
                });

                $scope.$watch('favourite', function () {
$log.log("watch favourite = ", $scope.favourite, $scope.themeRating);
                    if ($scope.favourite == $scope.themeRating.id) {
                        $scope.themeRating.ratingValue = 3;
                    }
                });
            }],
            templateUrl: 'html/directives/themeTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('imageThumb', function () {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                'imageUrl': '=',
                'imageId': '=',
                'altText': '@?',
                'title': '@?'
            },
            link: function (scope, element, attrs, ngModel) {

                scope.setBackground = function () {
//console.log(scope.parsedUrl);

                    $(element).find('#image').css('background', 'url('+scope.parsedUrl+') no-repeat bottom center');
                    $(element).find('#image').css('background-size', 'cover');
                    $(element).find('#image').css('-o-background-size', 'cover');
                    $(element).find('#image').css('-moz-background-size', 'cover');
                    $(element).find('#image').css('-webkit-background-size', 'cover');
                    $(element).find('#image').css('background-position', 'center bottom');
                };
            },
            controller: ['$scope', 'RestService', function ($scope, RestService) {

                $scope.$watch('imageUrl',function(){
                    if ($scope.imageUrl) {
                        $scope.parsedUrl = Config.webUrl + Config.imageUrl + imageSize() + "/" + $scope.imageUrl;
                        $scope.parsedId = "image" + $scope.imageId;
                        $scope.setBackground();
                    }
                });

                var imageSize = function () {
                    var windowWidth = $(window).width();
                    if (windowWidth <= 320) {
                        return 480;
                    } else if (windowWidth <= 480) {
                        return 768;
                    } else if (windowWidth <= 768) {
                        return 992;
                    } else if (windowWidth <= 992) {
                        return 1200;
                    } else {
                        return 1200;
                    }
                }

            }],
            template: "<img id='image' alt='{{ altText }}' title='{{ title }}'/>"
        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('tinderAnimation', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                repeatReady: "=",
                animationSpeed: "=",
                animationRevertSpeed: "=",
                threshold: "=",
                animateLike: "=",
                animateDislike: "=",
                menu: "="
            },
            link: function (scope, element) {
//console.log("init tinder element = ",  $(element));
                scope.divTinder = $(element).jTinder({
                    onDislike: function (item) {
                        if (scope.menu === "onderwerp") {
                            scope.$emit('swipe-question', {answer: 1}); // send event to parent scopes
                        }
                    },
                    onLike: function (item) {
                        if (scope.menu === "onderwerp") {
                            scope.$emit('swipe-question', {answer: 0}); // send event to parent scopes
                        }
                    },
                    animationRevertSpeed: scope.animationRevertSpeed,
                    animationSpeed: scope.animationSpeed,
                    threshold: scope.threshold,
                    likeSelector: '.like',
                    dislikeSelector: '.dislike'
                });

                /**
                 * Set button action to trigger jTinder like & dislike.
                 */
                $(element).find('.btn_like, .btn_dislike').bind("click", function (e) {
                    e.preventDefault();
                    var divId = e.currentTarget.id;
console.log("handle tinder like = ", divId);
                    if (divId === "btn_like") {
                        $(element).jTinder('like');
                    } else if (divId === "btn_dislike") {
                        $(element).jTinder('dislike');
                    }
                });

                /**
                 * Set button action to trigger jTinder multiple choice answers
                 */
                $(element).find('.statement ul li').bind("click", function (e) {
                    e.preventDefault();
                    var divId = e.currentTarget.id;
console.log("handle tinder click = ", divId);

                });

                /**
                 * Set info button action to trigger
                 */
                $('#answer-1, #answer-2, #answer-3, #answer-4, #answer-5, #answer-6').bind("click", function (e) {
                    e.preventDefault();
                    var divId = e.currentTarget.id;
console.log("handle tindersssssssss info click = ", divId);
                    scope.$emit('toggle:info'); // send event to parent scopes
                });

            },
            controller: ['$log', '$scope', function ($log, $scope) {

                $scope.$watch('animateLike', function () {
                    if ($scope.animateLike === true) {
$log.log("handle tinder like");
                        $scope.divTinder.jTinder('like');
                    }
                });

                $scope.$watch('animateDislike', function () {
                    if ($scope.animateDislike === true) {
$log.log("handle tinder dislike");
                        $scope.divTinder.jTinder('dislike');
                    }
                });

                // handle navigate from statement
                $scope.$on('next-statement', function(event, args) {
                    var pane = args.currentPane;
                    if (pane == 0) {
                        $scope.divTinder.jTinder('setFirstPane');
                    } else {
                        $scope.divTinder.jTinder('setLastPane');
                    }
                });

            }]
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('coolVoters', function () {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                'active': '='
            },
            link: function (scope, element, attrs) {

            },
            controller: ['$log', '$scope', '$location', '$route', 'RestService', 'DataService',
                function ($log, $scope, $location, $route, RestService, DataService) {
                    $scope.user = DataService.checkUser();
                    $scope.votePercentage = 100;

                    RestService.getPeopleCount().then(function(scores){
                        $scope.numberOfScores = scores.count;
                        $scope.numberOfCoolPeople = scores.count;

                        RestService.getCoolCount().then(function (voters) {
//console.log("show voters = ", $scope.active);
                            if ($scope.active === true) {
                                $scope.numberOfCoolPeople = voters.count;
//console.log("voters found = ", $scope.numberOfCoolPeople, " --- scores found = ", $scope.numberOfScores);

                                var score = ($scope.numberOfCoolPeople / $scope.numberOfScores) * 100;
                                $scope.votePercentage = score;
                            }
                        });
                    });

                    $scope.showRight = function () {
                        return ($scope.numberOfCoolPeople == $scope.numberOfScores);
                    }
                }
            ],
            templateUrl: 'html/directives/votersTemplate.html'
        };
    });
}());;(function () {
    'use strict';
    bcServices.factory('DataService', ['$log', '$rootScope', '$q', '$location', '$route', 'RestService',
        function ($log, $rootScope, $q, $location, $route, RestService) {

            /******************************************************
             * check if user is logged or redirect to home
             *******************************************************/
            var _user = null;
            var checkUser = function () {
                var deferred = $q.defer();
//$log.log("data service: checking for user = ", _user);
                if (!_user) {
                    RestService.getLoggedUser().then(function (user) {
                        var success = user && user != "null";
                        if (success) {
                            setUser(user);
                            deferred.resolve(user);
                        } else {
                            setUser(null);
                            deferred.reject(null);
                            $location.path("home");
                        }
                    }, function () { // handle 401 not authorised
                        setUser(null);
                        deferred.reject(null);
                        $location.path("home");
                    });
                } else {
                    deferred.resolve(_user);
                }
                return deferred.promise;
            };

            var setUser = function (user) {
                _user = user;
                $rootScope.$broadcast('user', {user: _user});
            };

            var getUser = function () {
                return _user;
            };

            /******************************************************
             * set the user survey
             *******************************************************/
            var _survey = null;
            var checkSurvey = function () {
                var deferred = $q.defer();
//$log.log("data service: checking for survey = ", _survey);
                if (!_survey) {
                    RestService.getSurvey().then(function (survey) {
                        if (survey) {
                            setSurvey(survey);
                            deferred.resolve(survey);
                        } else {
                            RestService.startSurvey().then(function (survey) {
                                setSurvey(survey);
                                deferred.resolve(survey);
                            });
                        }
                    }, function (data) { // handle 401 not authorised
                        setSurvey(null);
                        deferred.reject();
                        //$location.path("home");
                    });
                } else {
                    deferred.resolve(_survey);
                }
                return deferred.promise;
            };

            var setSurvey = function (survey) {
                _survey = survey;
//$log.log("data service: set survey = ", _survey);
                $rootScope.$broadcast('survey', {survey: _survey});
            };

            var getSurvey = function () {
                return _survey;
            };

            /******************************************************
             * set the user scores
             *******************************************************/
            var _themeScores = null;
            var checkThemeScores = function () {
                var deferred = $q.defer();
//$log.log("data service: checking for theme scores = ", _themeScores);
                if (!_themeScores) {
                    if (_survey) {
                        RestService.getThemeScore(_survey.id).then(function (themeScores) {
                            setThemeScores(themeScores);
                            deferred.resolve(themeScores);
                        }, function () { // handle 401 not authorised
                            setThemeScores(null);
                            deferred.reject();
                            $location.path("home");
                        });
                    }
                    else { // no active survey found
                        setThemeScores(null);
                        deferred.resolve();
                        $location.path("onderwerp");
                    }
                } else {
                    deferred.resolve(_themeScores);
                }
                return deferred.promise;
            };

            var setThemeScores = function (themeScores) {
                _themeScores = themeScores;
//$log.log("data service: set theme scores = ", _themeScores);
                $rootScope.$broadcast('themeScores', {themeScores: _themeScores});
            };

            var getThemeScores = function () {
                return _themeScores;
            };

            /******************************************************
             * set the total scores
             *******************************************************/
            var _totalScores = null;
            var checkTotalScores = function () {
                var deferred = $q.defer();
//$log.log("data service: checking for total scores = ", _totalScores);
                if (!_totalScores) {
                    if (_survey) {
                        RestService.getTotalScore(_survey.id).then(function (totalScores) {
                            setTotalScores(totalScores);
                            deferred.resolve(totalScores);
                            $location.path("about");
                        }, function () { // handle 401 not authorised
                            setTotalScores(null);
                            deferred.reject();
                            $location.path("home");
                        });
                    }
                    else { // no active survey found
                        setTotalScores(null);
                        deferred.resolve();
                        $location.path("onderwerp");
                    }
                } else {
                    deferred.resolve(_totalScores);
                }
                return deferred.promise;
            };

            var setTotalScores = function (totalScores) {
                _totalScores = totalScores;
//$log.log("data service: set theme scores = ", _themeScores);
                $rootScope.$broadcast('totalScores', {totalScores: _totalScores});
            };

            var getTotalScores = function () {
                return _totalScores;
            };

            var _answer;
            var setAnswer = function (answer, currentIndex, currentPane) {
                _answer = answer;
//$log.log("data service: set answer = ", _answer, answer.questionId);
                $rootScope.$broadcast('next-statement', {answer: _answer, currentIndex: currentIndex, currentPane: currentPane});
            };

            var getAnswer = function () {
                return _answer;
            };

            var _themes = [];
            var setThemes = function (themes) {
                _themes = themes;
            };


            var _votes = [];
            var setVotes = function (votes) {
                _votes = answers;
            };

            var getVotes = function () {
                return _votes;
            };

            var _themeVotes = [];
            var setThemeVotes = function (themeVotes) {
                _themeVotes = themeVotes;
            };

            var getThemeVotes = function () {
                return _themeVotes;
            };

            var _profiles = [];
            var setProfiles = function (profiles) {
                _profiles = profiles;
            };

            var getProfiles = function () {
              return _profiles;
            };

            var getProfileByScore = function (score, profiles) {
                var selectedProfile = _.find(profiles, function(profile) {
                    return profile.score === score;
                });
                return selectedProfile ? selectedProfile : profiles[0];
            };

            var _profile = null;
            var setProfile = function (profile) {
                _profile = profile;
                $rootScope.$broadcast('profile', {profile: _profile});
            };

            var getProfile = function () {
                return _profile;
            };


            return {
                checkUser: checkUser,
                getUser: getUser,
                checkSurvey: checkSurvey,
                setQuestionnaire: setSurvey,
                getSurvey: getSurvey,
                checkThemeScores: checkThemeScores,
                getThemeScores: getThemeScores,
                setThemes: setThemes,
                setVotes: setVotes,
                getVotes: getVotes,
                setThemeVotes: setThemeVotes,
                checkTotalScores: checkTotalScores,
                getTotalScores: getTotalScores,
                getThemeVotes: getThemeVotes,
                setAnswer: setAnswer,
                getAnswer: getAnswer,
                setProfiles: setProfiles,
                getProfiles: getProfiles,
                getProfileByScore: getProfileByScore,
                setProfile: setProfile,
                getProfile: getProfile
            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcServices.factory('EmailService', ['$http', '$q', '$route', '$window',
        function ($http, $q, $route, $window) {


            var getEmailLight = function (socialPost) {
                var email = "mailto:?to=";
                email += "&subject=" + encodeURIComponent(socialPost.message.message);
                email += "&body=" + encodeURIComponent(socialPost.message.description + " " + Config.shareUrl);
                //email += "&target=_self";
                return email;
            };

            var sendEmailLight = function (email) {
                $window.location.href = email;
            };

            var getAnswerPost = function (user, answer) {
                var post = {};
                post.platform = "email";
                post.user = user;
                post.email = "";

                var message = {};
                if (answer) {
                    message.message = answer.text;
                    message.name = "";
                    message.picture = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
                    message.description = "Doe ook mee aan de test voor de gemeente Hardenberg";
                }
                post.message = message;
                post.title = "Kennistest. Alcohol, drugs & roken";
                return post;
            };

            var getProfilePost = function (user, profile) {
                var post = {};
                post.platform = "email";
                post.user = user;
                post.email = "";

                var message = {};

                if (profile) {
                    message.message = profile.shareText;
                    message.name = "";
                    message.picture = Config.webUrl + Config.profileUrl + profile.label + ".jpg";
                    message.description = profile.teaserText;
                }

                post.message = message;
                post.title = "Kennistest. Alcohol, drugs & roken";
                return post;
            };


            return {
                getEmailLight: getEmailLight,
                sendEmailLight: sendEmailLight,
                getAnswerPost: getAnswerPost,
                getProfilePost: getProfilePost
            };
        }
    ]);
}());;window.fbAsyncInit = function () {
    FB.init({
        appId: Config.fbKey,
        xfbml: true,
        version: 'v2.1'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


(function () {
    'use strict';
    bcServices.factory('FacebookService', ['$http', '$q', '$route', '$window',
        function ($http, $q, $route, $window) {

            var _userId = null;
            var _accessToken = null;
            var _isConnected = false;


            var getPostLight = function (socialPost) {
                var routeParam = $route.current.$$route.name;
                var url = encodeURIComponent(Config.shareUrl);
                var redirect = url + encodeURIComponent("/#/" + routeParam + "?");
//console.log("facebook redirect = ", redirect);

                var fbPost = "https://www.facebook.com/dialog/feed?";
                fbPost += "app_id=" + Config.fbKey;
                fbPost += "&display=popup";
                fbPost += "&caption=" + url;
                fbPost += "&name=" + socialPost.message.message;
                fbPost += "&description=" + socialPost.message.description;
                fbPost += "&link=" + Config.shareUrl;
                fbPost += "&picture=" + encodeURIComponent(socialPost.message.picture);
                fbPost += "&redirect_uri=" + redirect;
                return fbPost;
            };

            var publishPostLight = function (message) {
//console.log("share on facebook = ", message);
                $window.open(message, "_blank");
            };

            var checkLoginStatus = function () {
                var deferred = $q.defer();

                FB.getLoginStatus(function (response) {
//console.log('check login status = ' + response.status);

                    if (response.status === 'connected') {
                        setAuth(response);

                        getUserData().then(function (me) {
                            deferred.resolve(me);
                        });
                    } else if (response.status === 'not_authorized') {
                        deferred.resolve(null);
                    } else {
                        // the user isn't logged in to Facebook.
                        deferred.resolve(null);
                    }
                });

                return deferred.promise;
            };

            var setAuth = function (response) {
                if (response.authResponse) {
                    _userId = response.authResponse.userID;
                    _accessToken = response.authResponse.accessToken;
                }
            };

            /**
             * Login methods
             *
             */
            var goLogin = function () {
                var deferred = $q.defer();

//console.log("go login with facebook");
                FB.login(function (response) {
//console.log('facebook login response = ', response);
                    if (response.status === 'connected') {
                        setAuth(response);

                        getUserData().then(function (me) {
                            deferred.resolve(me);
                        });
                    }
                    else if (response.status == "not_authorized") {
                        deferred.resolve(null);
                    }
                    else {
                        deferred.reject();
                    }
                }, {scope: 'user_about_me, publish_actions'});

                return deferred.promise;
            };

            var getUserData = function () {
                var deferred = $q.defer();

                FB.api('/me', function (response) {
                    _isConnected = true;

                    var username = (response.name != undefined) ? response.name : _userId;
                    var me = {};
                    me.userId = _userId;
                    me.token = _accessToken;
                    me.name = username;
                    me.first_name = response.first_name;
                    me.last_name = response.last_name;
                    me.gender = response.gender;
                    me.thumb = "http://graph.facebook.com/" + _userId + "/picture?type=square";
                    deferred.resolve(me);
                });

                return deferred.promise;
            };

            var getAnswerPost = function (user, answer) {
                var post = {};
                post.platform = "facebook";
                post.user = user;

                var message = getDefaultMessage();
                if (answer) {
                    message.message = answer.text;
                    message.name = "";
                    message.picture = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
                    message.description = "Doe ook mee aan de test voor de gemeente Hardenberg";
                }

                post.message = message;
                post.title = "Kennistest. Alcohol, drugs & roken";
                return post;
            };

            var getProfilePost = function (user, profile) {
                var post = {};
                post.platform = "facebook";
                post.user = user;

                var message = getDefaultMessage();
                if (profile) {
                    message.message = profile.shareText;
                    message.name = "";
                    message.picture = Config.webUrl + Config.profileUrl + profile.label + ".jpg";
                    message.description = profile.teaserText;
                }

                post.message = message;
                post.title = "Kennistest. Alcohol, drugs & roken";
                return post;
            };

            var getDefaultMessage = function () {
                var message = {
                    display: 'iframe',
                    actions: [{name: 'action_links text!', link: Config.shareUrl}],
                    caption: Config.shareUrl,
                    link: Config.shareUrl  // Go here if user click the picture
                };
                return message;
            };

            var publishPost = function (message) {
                var deferred = $q.defer();

                FB.api("/me/feed", "POST", message,
                    function (response) {
                        if (response && !response.error) {
                            deferred.resolve(response);
                        }
                        else {
                            var message = response.error.message;
                            if (message == "The user hasn't authorized the application to perform this action") {
                                goLogin();
                                deferred.reject();
                            } else {
                                deferred.reject();
                            }
                        }
                    }
                );

                return deferred.promise;
            };

            return {
                getPostLight: getPostLight,
                publishPostLight: publishPostLight,
                checkLoginStatus: checkLoginStatus,
                goLogin: goLogin,
                getUserData: getUserData,
                publishPost: publishPost,
                getAnswerPost: getAnswerPost,
                getProfilePost: getProfilePost
            };
        }
    ]);
}());;(function () {
    'use strict';
    bcServices.factory('HighchartService', ['$rootScope', '$log', '$window',
        function ($rootScope, $log, $window) {


            /**
             * initialise the chart
             */
            var initialiseChart = function (scope, element, config, theme) {
                config = config || {};
                var mergedOptions = getMergedOptions(scope, element, config);
//console.log("init chart with options = ", mergedOptions, " -- theme = ", theme);
                var chart = config.useHighStocks ? new Highcharts.StockChart(mergedOptions) : new Highcharts.Chart(Highcharts.merge(mergedOptions, theme));

                for (var i = 0; i < axisNames.length; i++) {
                    if (config[axisNames[i]]) {
                        processExtremes(chart, config[axisNames[i]], axisNames[i]);
                    }
                }
                if (config.series && config.series.length > 0) {
                    processSeries(chart, config.series);
                }
                if (config.loading) {
                    chart.showLoading();
                }
                chart.redraw();
                return chart;
            };

            /**
             * convert theme scores to chart data
             */
            var convertToScore = function (results, agree) {
                var chartData = [];

                _.each(results, function (score) {
                    var chartValue = agree ? (isNaN(score.agreePercentage) ? 0 : parseFloat(score.agreePercentage)) : (isNaN(score.disagreePercentage) ? 0 : parseFloat(score.disagreePercentage));
                    chartValue = Math.ceil((chartValue / 100) * 3);
                    chartData.push([score.themeDto.name, chartValue]);
                });

$log.log(">>>>>>>>>>>>>>>>>>>>>>>>> ", chartData);
                return chartData;
            };

            var getChartConfig = function (dataAgree, dataDisagree, bFull, barType) {
                var chartConfig = {
                    series: [
                        {
                            type: 'column',
                            data: dataDisagree,
                            name: '-',
                            tooltip: {enabled: false},
                            borderColor: '#7f1016',
                            dataLabels: {

                            }
                        },
                        {
                            type: 'column',
                            data: dataAgree,
                            name: '+',
                            tooltip: {enabled: false},
                            marker: {
                                symbol: 'triangle'
                            },
                            borderColor: '#E20613',
                            dataLabels: {
                                x: 30,
                                y: 30
                            }
                        }

                    ],
                    yAxis: {
                        title: {
                            text: "Aantal Vragen",
                            align: barType === "column" ? "middle" : "high",
                            margin: 20,
                            style: {
                                fontSize: '20px',
                                fontFamily: 'Lato, sans-serif',
                                'text-transform': 'uppercase',
                                fontWeight: 'bold'
                            },
                            opposite: barType === "column" ? true : false
                        },
                        min: 0,
                        max: 3
                    },
                    xAxis: {
                        type: 'category',
                        title: {
                            text: "Thema's",
                            align: barType === "column" ? "high" : "middle",
                            style: {
                                fontSize: '20px',
                                fontFamily: 'Lato, sans-serif',
                                'text-transform': 'uppercase',
                                fontWeight: 'bold',
                                textAlign: 'right'
                            }
                        },
                        offset: 0,
                        labels: {
                            useHTML: true,
                            formatter: function () {
                                return parseChartInfo(this.value, bFull, barType);
                            }
                        }
                    },
                    useHighStocks: false,
                    options: {
                        chart: {
                            spacingTop: 30
                        },
                        bar: {
                            borderWidth: 0
                        },
                        navigator: {
                            enabled: false
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal'
                            },
                            series: {
                                cursor: 'default',
                                point: {
                                    events: {
                                        click: function () {
                                            //openChartInfo(this.category, bFull);
                                        }
                                    }
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        }
                    }
                };

                return chartConfig;
            };

            function parseChartInfo(themeName, bFull, barType) {
                var infoClass = "chart-info " + barType;
                var divInfo = '<div class="' + infoClass +'">';
                var btn = "btn-info-chart btn-" + barType;
                if (bFull) divInfo += '<div id="' + themeName + '" class="' + btn + '"><span>i</span></div>';
                divInfo += '<div class="chart-theme"><h3>' + themeName + '</h3></div>';
                divInfo += '</div>';
                return divInfo;
            }

            function openChartInfo(category, bFull) {
                if (bFull) {
                    $rootScope.$broadcast("chart:info", {'category': category});
                }
            }

            function animationComplete() {
                $rootScope.$broadcast("chart:animated");
            }

            function chartComplete() {
                $rootScope.$broadcast("chart:complete");
            }

            /**
             * get the highcharts theme
             *
             */
            var getChartTheme = function (barType) {

                /***
                 @coen-red: #e52520;
                 @coen-lightred: #FFDEE1;
                 @coen-darkred: #7e1016;
                 @coen-link: #f5f5f5;
                 @coen-green: #91c21b;

                 @coen-darkgrey: #262626;
                 @coen-grey: #4b3b3c;
                 @coen-lightgrey: #F2F2F2;
                 @coen-white: #ffffff;
                 @coen-black: #000000;
                 ****/

                var theme = {
                    colors: ["#7e1016", "#e52520", "#e52520", "#7CAAD6", "#96B439", "#903060", "#F09000", "#55BF3B", "#DF5353", "#7798BF", "#D8DA86"],

                    chart: {
                        backgroundColor:"rgba(255, 255, 255, 0)",
                        type: barType,
                        style: {
                            fontFamily: "Lato, sans-serif"
                        },
                        events: {
                            redraw: function(event) {
                                animationComplete();
                            },
                            load: function(event) {
                                chartComplete();
                            }
                        }
                    },
                    title: {
                        style: {
                            color: '#262626'
                        },
                        text: ''
                    },
                    subtitle: {
                        style: {
                            color: '#DDD'
                        }
                    },
                    tooltip: {enabled: false},
                    plotOptions: {
                        series: {
                            pointWidth: (barType === "column" ? 40 : 40) //width of the column bars irrespective of the chart size
                        }
                    },
                    xAxis: {
                        gridLineWidth: 0,
                        lineColor: '#999',
                        tickColor: '#191919',
                        tickWidth: 0,
                        tickPosition: 'inside',
                        labels: {
                            style: {
                                color: '#999',
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            style: {
                                color: '#AAA',
                                fontSize: '16px'
                            }
                        },
                        events: {
                            afterSetExtremes: function () {
                                //initAnnotations();
                            }
                        }
                    },
                    yAxis: {
                        alternateGridColor: null,
                        tickInterval: 1,
                        minorTickInterval: 1,
                        gridLineColor: 'rgba(0, 0, 0, 0)',
                        minorGridLineColor: 'rgba(0, 0, 0, 0)',
                        lineColor: '#999',
                        lineWidth: 1,
                        tickColor: '#191919',
                        tickWidth: 1,
                        tickPosition: 'inside',
                        labels: {
                            style: {
                                color: '#999',
                                fontWeight: 'bold',
                                fontSize: '16px'
                            }
                        },
                        title: {
                            style: {
                                color: '#AAA',
                                font: '16px'
                            }
                        }
                    },
                    legend: {
                        enabled: false
                    }
                };

                return theme;
            };



            //IE8 support
            var indexOf = function (arr, find, i /*opt*/) {
                if (i === undefined) {
                    i = 0;
                }
                if (i < 0) {
                    i += arr.length;
                }
                if (i < 0) {
                    i = 0;
                }
                for (var n = arr.length; i < n; i++) {
                    if (i in arr && arr[i] === find) {
                        return i;
                    }
                }
                return -1;
            };


            function prependMethod(obj, method, func) {
                var original = obj[method];
                obj[method] = function () {
                    var args = Array.prototype.slice.call(arguments);
                    func.apply(this, args);
                    if (original) {
                        return original.apply(this, args);
                    } else {
                        return;
                    }

                };
            }

            function deepExtend(destination, source) {
                for (var property in source) {
                    if (source[property] && source[property].constructor &&
                        source[property].constructor === Object) {
                        destination[property] = destination[property] || {};
                        deepExtend(destination[property], source[property]);
                    } else {
                        destination[property] = source[property];
                    }
                }
                return destination;
            }

            var seriesId = 0;
            var ensureIds = function (series) {
                angular.forEach(series, function (s) {
                    if (!angular.isDefined(s.id)) {
                        s.id = "series-" + seriesId++;
                    }
                });
            };

            var axisNames = [ "xAxis", "yAxis" ];
            var getAxisNames = function () {
                return axisNames;
            };

            var getMergedOptions = function (scope, element, config) {
                var mergedOptions = {};

                var defaultOptions = {
                    chart: {
                        events: {},
                        options: {}
                    },
                    title: {},
                    subtitle: {},
                    series: [],
                    credits: {},
                    plotOptions: {},
                    navigator: {enabled: false}
                };

                if (config.options) {
                    mergedOptions = deepExtend(defaultOptions, config.options);
                } else {
                    mergedOptions = defaultOptions;
                }
                mergedOptions.chart.renderTo = element[0];
                axisNames.forEach(function (axisName) {
                    if (config[axisName]) {
                        prependMethod(mergedOptions.chart.events, 'selection', function (e) {
                            var thisChart = this;
                            if (e[axisName]) {
                                scope.$apply(function () {
                                    scope.config[axisName].currentMin = e[axisName][0].min;
                                    scope.config[axisName].currentMax = e[axisName][0].max;
                                });
                            } else {
                                //handle reset button - zoom out to all
                                scope.$apply(function () {
                                    scope.config[axisName].currentMin = thisChart[axisName][0].dataMin;
                                    scope.config[axisName].currentMax = thisChart[axisName][0].dataMax;
                                });
                            }
                        });

                        prependMethod(mergedOptions.chart.events, 'addSeries', function (e) {
                            scope.config[axisName].currentMin = this[axisName][0].min || scope.config[axisName].currentMin;
                            scope.config[axisName].currentMax = this[axisName][0].max || scope.config[axisName].currentMax;
                        });

                        mergedOptions[axisName] = angular.copy(config[axisName]);
                    }
                });

                if (config.title) {
                    mergedOptions.title = config.title;
                }

                if (config.subtitle) {
                    mergedOptions.subtitle = config.subtitle;
                }

                if (config.credits) {
                    mergedOptions.credits = config.credits;
                }
                return mergedOptions;
            };

            var updateZoom = function (axis, modelAxis) {
                var extremes = axis.getExtremes();
                if (modelAxis.currentMin !== extremes.dataMin || modelAxis.currentMax !== extremes.dataMax) {
                    axis.setExtremes(modelAxis.currentMin, modelAxis.currentMax, false);
                }
            };

            var processExtremes = function (chart, axis, axisName) {
                if (axis.currentMin || axis.currentMax) {
                    chart[axisName][0].setExtremes(axis.currentMin, axis.currentMax, true);
                }
            };

            var chartOptionsWithoutEasyOptions = function (options) {
                return angular.extend({}, options, {data: null, visible: null});
            };

            var prevOptions = {};

            var processSeries = function (chart, series) {
                var ids = [];
                if (series) {
                    ensureIds(series);

                    //Find series to add or update
                    angular.forEach(series, function (s) {

                        ids.push(s.id);
                        var chartSeries = chart.get(s.id);
                        if (chartSeries) {
                            if (!angular.equals(prevOptions[s.id], chartOptionsWithoutEasyOptions(s))) {
                                chartSeries.update(angular.copy(s), false);
                            } else {
                                if (s.visible !== undefined && chartSeries.visible !== s.visible) {
                                    chartSeries.setVisible(s.visible, false);
                                }
                                if (chartSeries.options.data !== s.data) {
                                    chartSeries.setData(s.data, false);
                                }
                            }
                        } else {
                            chart.addSeries(angular.copy(s), false);
                        }
                        prevOptions[s.id] = chartOptionsWithoutEasyOptions(s);
                    });
                }

                //Now remove any missing series
                for (var i = chart.series.length - 1; i >= 0; i--) {
                    var s = chart.series[i];
                    if (indexOf(ids, s.options.id) < 0) {
                        s.remove(false);
                    }
                }
            };

            var beforeInit = function () {
                var orgHighchartsRangeSelectorPrototypeRender = Highcharts.RangeSelector.prototype.render;
                Highcharts.RangeSelector.prototype.render = function (min, max) {
                    orgHighchartsRangeSelectorPrototypeRender.apply(this, [min, max]);
                    var leftPosition = this.chart.plotLeft,
                        topPosition = this.chart.plotTop+5,
                        space = 2;
                    this.zoomText.attr({
                        x: leftPosition,
                        y: topPosition - 50
                    });
                    leftPosition += this.zoomText.getBBox().width;
                    for (var i = 0; i < this.buttons.length; i++) {
                        this.buttons[i].attr({
                            x: leftPosition,
                            y: topPosition - 65
                        });
                        leftPosition += this.buttons[i].width + space;
                    }
                };
            };

            return {
                getChartConfig: getChartConfig,
                getChartTheme: getChartTheme,
                initialiseChart: initialiseChart,
                getAxisNames: getAxisNames,
                processSeries: processSeries,
                convertToScore: convertToScore
            };
        }
    ]);
}());;(function () {
    'use strict';
    bcServices.factory('MenuService', ['$log', '$q', '$filter', function ($log, $q, $filter) {


        var getBurgerItems = function () {
            var items = [
                {
                    title: "menu.burger.item1.title",
                    description: "menu.burger.item1.description"
                },
                {
                    title: 'menu.burger.item2.title',
                    description: "menu.burger.item2.description"
                }
            ];

            return items;
        };

        var getQuestionsByMenu = function (menu) {
            var questions = [];
            switch (menu) {
                case "about":
                    questions = [
                        {
                            question: "about.question.question1",
                            answer: "about.question.question1.answer"
                        },
                        {
                            question: "about.question.question2",
                            answer: "about.question.question2.answer"
                        },
                        {
                            question: "about.question.question3",
                            answer: "about.question.question3.answer"
                        }
                    ];
                    break;
                case "gegevens":
                    questions = [
                        {
                            icon: "icon-person",
                            question: "about.question.question3",
                            answer: "about.question.question3.answer"
                        }
                    ];
                    break;
            }

            return questions;
        };


        /******************************************************
         * get the navigation menu
         *******************************************************/
        var menu = [
            {
                index: 0,
                bulletIndex: 0,
                label: "menu.home.label",
                title: "home",
                url: "home",
                showNext: true,
                showPrevious: false,
                showHome: true,
                showSocial: false,
                showHeader: false
            },
            {
                index: 1,
                bulletIndex: 1,
                label: "menu.about.label",
                title: "about",
                url: "about",
                showNext: true,
                showPrevious: false,
                showVotes: false,
                showSocial: false,
                showHeader: true
            },
            {
                index: 2,
                bulletIndex: 2,
                label: "menu.info.label",
                title: "gegevens",
                url: "gegevens",
                showNext: true,
                showPrevious: false,
                showVotes: false,
                showSocial: false,
                showHeader: true
            },
            {
                index: 3,
                bulletIndex: 3,
                label: "menu.question.label",
                title: "onderwerp",
                url: "onderwerp",
                showNext: true,
                showPrevious: false,
                showVotes: true,
                showSocial: true,
                showHeader: true
            },
            {
                index: 4,
                bulletIndex: 4,
                label: "menu.result.label",
                title: "resultaat",
                url: "resultaat",
                showNext: true,
                showPrevious: false,
                showVotes: false,
                showSocial: false,
                showHeader: true
            },
            {
                index: 5,
                bulletIndex: 5,
                label: "menu.end.label",
                title: "einde",
                url: "einde",
                showNext: false,
                showPrevious: false,
                showVotes: false,
                showSocial: false,
                showHeader: false
            }
        ];

        var getMenu = function () {
            return menu;
        };

        var getAfterQuestionnaireMenu = function () {
            var menuItems = getMenu();
            var menu = _.find(menuItems, function (item) {
                return item.title === "onderwerp";
            });

            var index = _.indexOf(menuItems, menu);
            var nextMenu = menuItems[index + 1];
            return nextMenu ? nextMenu : menuItems[0];
        };

        var createStatementSteps = function (globalIndex, menuSteps, answers) {
            var surveyLength = answers.length > 1 ? answers.length : 15;
            for (var i = 0; i < surveyLength; i++) {
                var answer = answers[i];
                if (answer) {
                    menuSteps.push(
                        {
                            index: globalIndex++,
                            label: 'menu.onderwerp.label',
                            title: 'onderwerp',
                            url: 'onderwerp/' + answer.questionId,
                            active: false,
                            showVotes: answer.type == "statement",
                            showNext: answer.type == "multiple",
                            showHeader: true
                        });
                } else {
                    menuSteps.push(
                        {
                            index: globalIndex++,
                            label: 'menu.onderwerp.label',
                            title: 'onderwerp',
                            url: 'onderwerp',
                            active: false,
                            showHeader: true
                        });
                }
            }
            return globalIndex;
        };

        var getMenuWithSteps = function (answers) {
            var menuSteps = [];
            var menuItems = getMenu();
            var index = 0;
            _.each(menuItems, function (menuItem) {
                if (menuItem.title === 'onderwerp') {
                    index = createStatementSteps(index, menuSteps, answers);
                }
                else {
                    menuItem.index = index;
                    menuItem.active = (index === 0);
                    menuSteps.push(menuItem);
                    index++;
                }
            });
            return menuSteps;
        };

        var profiles = [
            {
                label: "Beginner",
                icon: "icon-starter",
                description: "0 van de 15 goed",
                score: 1
            },
            {
                label: "Middelmaatje",
                icon: "icon-average",
                description: "0-5 van de 15 goed",
                score: 2
            },
            {
                label: "Baas",
                icon: "icon-boss",
                description: "5-10 van de 15 goed",
                score: 3
            },
            {
                label: "Meesterbrein",
                icon: "icon-master",
                description: "10-15 van de 15 goed",
                score: 4
            },
            {
                label: "Eindbaas",
                icon: "icon-endboss",
                description: "15 van de 15 goed",
                score: 5
            }
        ];


        var getProfiles = function () {
            return profiles;
        };

        var getProfileByScore = function (score) {
            var profile = _.find(profiles, function(profile) {
                return profile.score === score;
            });
            return profile ? profile : profiles[0];
        };

        var getProfilesWithScore = function (profiles, userCount, scores) {

            _.each(profiles, function(profile) {
                profile.userCount = scores[profile.score - 1];
                profile.userPercentage = (profile.userCount / userCount) * 100;
            });
            return profiles;
        };

        var getProfileScore = function (numCorrectAnswers, numQuestions)  {
            var modQuestions = numQuestions % 3;
            var numProfiles = Math.floor(numQuestions / 3);

            var score = Math.ceil(numCorrectAnswers / numProfiles);
            score = score + 1;
            var modProfile = numCorrectAnswers % numProfiles;
            var addScore = modProfile == 0 && numCorrectAnswers > 0;
            score = score + (addScore ? 1 : 0);
            return score;
        };

        var setShareText = function (profile, numQuestions, numCorrectAnswers) {
            var shareText = $filter('translate')('social.share.result.text');
            if (numCorrectAnswers) shareText = shareText.replace('%NUM_CORRECT%', numCorrectAnswers);
            if (numQuestions) shareText = shareText.replace('%NUM_QUESTIONS%', numQuestions);
            profile.shareText = shareText;
            profile.teaserText = $filter('translate')('social.share.teaser.text');
        };

        return {
            getMenu: getMenu,
            getMenuWithSteps: getMenuWithSteps,
            getAfterQuestionnaireMenu: getAfterQuestionnaireMenu,
            getQuestionsByMenu: getQuestionsByMenu,
            getBurgerItems: getBurgerItems,
            getProfiles: getProfiles,
            getProfileByScore: getProfileByScore,
            getProfilesWithScore: getProfilesWithScore,
            getProfileScore: getProfileScore,
            setShareText: setShareText
        };
    }
    ]);
}());
;(function () {
    'use strict';
    bcServices.factory('RestService', ['$http', '$q',
        function ($http, $q) {

            // get facebook login window
            var getLoginWindow = function (platform) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/person/authentication-url?platform="+platform, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // check user
            var getLoggedUser = function (user) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/person", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // login person
            var loginPerson = function (person) {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/person", person, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject();
                });
                return deferred.promise;
            };

            // save person
            var savePerson = function (person) {
                var deferred = $q.defer();
                $http.put(Config.apiUrl + "/person", person, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject();
                });
                return deferred.promise;
            };

            // start survey
            var startSurvey = function () {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/survey", {}, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get incomplete questionnaires
            var getSurvey = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/survey", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get complete questionnaires
            var getCompleteSurvey = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/survey/complete", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get survey
            var getSurveyById = function (questionnaireId) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/survey/" + questionnaireId, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get statements
            var getStatements = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/question", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get statement
            var getStatement = function (statementId) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/question/" + statementId, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // answer question
            var answerQuestion = function (surveyId, questionId, answer) {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/survey/" + surveyId + "/question/" + questionId, answer, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get theme score
            var getThemeScore = function (questionnaireId) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/theme-scores/" + questionnaireId, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get total score
            var getTotalScore = function (questionnaireId) {
                var deferred = $q.defer();
                    $http.get(Config.apiUrl + "/survey/" + questionnaireId + "/theme-scores", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get theme scores
            var getThemeScoresCount = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/theme-scores/count", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get people count
            var getPeopleCount = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/person/count", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get cool people score
            var getCoolCount = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/person/cool-people/count", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get profiles
            var getProfiles = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/profiles", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get persons
            var getPersons = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/person/all", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get score
            var getScore = function (min, max) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/survey/scorecount?min=" + min + "&max=" + max, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            var sendEmail = function (email) {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/share", email, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            return {
                getLoginWindow: getLoginWindow,
                getLoggedUser: getLoggedUser,
                loginPerson: loginPerson,
                savePerson: savePerson,
                getStatements: getStatements,
                getStatement: getStatement,
                startSurvey: startSurvey,
                getSurvey: getSurvey,
                getCompleteSurvey: getCompleteSurvey,
                getSurveyById: getSurveyById,
                answerQuestion: answerQuestion,
                getThemeScore: getThemeScore,
                getProfiles: getProfiles,
                getScore: getScore,
                getTotalScore: getTotalScore,
                getThemeScoresCount: getThemeScoresCount,
                getPeopleCount: getPeopleCount,
                getCoolCount: getCoolCount,
                sendEmail: sendEmail
            };
        }
    ]);
}());;(function () {
    'use strict';
    bcServices.factory('TwitterService', ['$http', '$q', '$route', '$window',
        function ($http, $q, $route, $window) {

            var getPostLight = function (socialPost) {
                var tweet = "https://twitter.com/share?url=" + Config.shareUrl;
                tweet += "&text=" + socialPost.message.message + " " + socialPost.message.description;
                //tweet += "&hashtags=Kennistest";
                return tweet;
            };

            var publishPostLight = function (tweet) {
                $window.open(tweet);
            };


            var _userId = null;
            var _accessToken = null;

            var authorizationResult = false;

            var isReady = function () {
                return (authorizationResult);
            };

            var getAnswerPost = function (user, answer) {
                var post = {};
                post.platform = "twitter";
                post.user = user;

                var message = {};
                if (answer) {
                    var imageUrl = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
                    //convertImgToBase64(imageUrl, function(base64Img) {
                    //    var boundary = getBoundary(base64Img);
                    //};

                    message.message = answer.text;
                    message.name = "";
                    message.picture = imageUrl;
                    message.description = "Doe ook mee aan de test voor de gemeente Hardenberg";
                }

                post.message = message;
                post.title = "Kennistest. Alcohol, drugs & roken";
                return post;
            };

            var getProfilePost = function (user, profile) {
                var post = {};
                post.platform = "twitter";
                post.user = user;

                var message = {};
                if (profile) {
                    message.message = profile.shareText;
                    message.name = "";
                    message.picture = Config.webUrl + Config.profileUrl + profile.label + ".jpg";
                    message.description = profile.teaserText;
                }

                post.message = message;
                post.title = "Kennistest. Alcohol, drugs & roken";
                return post;
            };

            var convertImgToBase64 = function(url, callback, outputFormat){
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var img = new Image;
                img.crossOrigin = 'Anonymous';
                img.onload = function(){
                    canvas.height = img.height;
                    canvas.width = img.width;
                    ctx.drawImage(img,0,0);
                    var dataURL = canvas.toDataURL(outputFormat || 'image/png');
                    callback.call(this, dataURL);
                    // Clean up
                    canvas = null;
                };
                img.src = url;
            };

            var getBoundary = function (base64Img) {
                var boundaryString = '------------------------------';
                var dataUrl = base64Img.substr(base64Img.indexOf("base64,",0)+7);

                var cr = "\r\n";
                var boundary = "--"+ boundaryString;
                var body = boundary + cr;
                body+="Content-Disposition: form-data; name=\"key\""+cr+cr;
                body+="key"+cr;
                body+=boundary+cr;
                body+="Content-Disposition: form-data; name=\"fileupload\"; filename=\"statement.png\""+cr;
                body+="Content-Type: image/png"+cr;
                body+="Content-Transfer-Encoding: base64"+cr+cr;
                body+=dataUrl+ cr;
                body+=boundary+"--"+cr;
                return body;
            };

            var publishPost = function (message) {
                var deferred = $q.defer();


                OAuth.popup("twitter", function (error, result) {
                    if (error) {

                    } else {
                        result.post({
                            url: '/1.1/statuses/update.json',
                            data: message
                        }).done(function (data) {
                            deferred.resolve(data);
                        });
                    }
                });

                return deferred.promise;
            };

            var publishPostWithImage = function (message) {
                var deferred = $q.defer();


                OAuth.popup("twitter", function (error, result) {
                    if (error) {

                    } else {
                        result.post({
                            url: '/1.1/statuses/update_with_media.json',
                            data: {status: message.message, media: message.picture},
                            contentType: 'multipart/form-data'
                        }).done(function (res) {
                            console.log("finally uploaded an image = ", res);
                        });
                    }
                });

                return deferred.promise;
            };

            var clearCache = function () {
                OAuth.clearCache('twitter');
                authorizationResult = false;
            };

            var getLatestTweets = function () {
                //create a deferred object using Angular's $q service
                var deferred = $q.defer();
                var promise = authorizationResult.get('/1.1/statuses/home_timeline.json').done(function (data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
                    //when the data is retrieved resolved the deferred object
                    deferred.resolve(data)
                });
                //return the promise of the deferred object
                return deferred.promise;
            };


            return {
                getPostLight: getPostLight,
                publishPostLight: publishPostLight,
                isReady: isReady,
                getLatestTweets: getLatestTweets,
                clearCache: clearCache,
                publishPost: publishPost,
                publishPostWithImage: publishPostWithImage,
                getAnswerPost: getAnswerPost,
                getProfilePost: getProfilePost
            };
        }
    ]);
}());;(function () {
    'use strict';
    bcAnimations.animation('.slide-animation', function () {
        return {
            beforeAddClass: function (element, className, done) {
                var scope = element.scope();
                if (className == 'ng-hide') {
                    var finishPoint = element.parent().width();
                    if(scope.direction !== 'right') {
                        finishPoint = -finishPoint;
                    }
                    TweenMax.to(element, 0.5, {left: finishPoint, onComplete: done });
                }
                else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                var scope = element.scope();

                if (className == 'ng-hide') {
                    element.removeClass('ng-hide');

                    var startPoint = element.parent().width();
                    if(scope.direction === 'right') {
                        startPoint = -startPoint;
                    }
                    TweenMax.fromTo(element, 0.5, { left: startPoint }, {left: 0, onComplete: done });
                }
                else {
                    done();
                }
            }
        };
    });
}());