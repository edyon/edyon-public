"use strict";

/********* CONSTANTS *********/
var bcConstants = angular.module('bc.constants', []);
var bcServices = angular.module('bc.services', []);
var bcControllers = angular.module('bc.controllers', []);
var bcDirectives = angular.module('bc.directives', []);
var bcFilters = angular.module('bc.filters', []);

var bcApp = angular.module('bcApp', [
    'ngRoute',
    'ngResource',
    'ngCookies',
    'bc.constants',
    'bc.services',
    'bc.controllers',
    'bc.directives',
    'bc.filters',
    'ui.bootstrap'
]);

bcApp.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        var mainLayout = {
            header: 'html/layout/cms-header.html',
            footer: 'html/layout/cms-footer.html'
        };

        var checkAdmin = ['DataService', function (DataService) {
            return DataService.checkAdmin();
        }];

        $routeProvider.
            when('/inloggen', _.extend({
                    templateUrl: 'html/cms/login.html',
                    controller: 'CmsLoginController',
                    name: 'inloggen'
                },
                mainLayout)).
            when('/campagne', _.extend({
                    templateUrl: 'html/cms/campaign.html',
                    controller: 'CmsCampaignController',
                    name: 'campagne',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            when('/campagne/:campaignId/menu', _.extend({
                    templateUrl: 'html/cms/menu.html',
                    controller: 'CmsMenuController',
                    name: 'menu',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            when('/campagne/:campaignId/resultaat', _.extend({
                    templateUrl: 'html/cms/result.html',
                    controller: 'CmsResultController',
                    name: 'resultaat',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            when('/campagne/:campaignId/thema/:themeId/onderwerp/:statementId', _.extend({
                    templateUrl: 'html/cms/question.html',
                    controller: 'CmsQuestionController',
                    name: 'onderwerp',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            when('/campagne/:campaignId/thema/:themeId/onderwerp', _.extend({
                    templateUrl: 'html/cms/question.html',
                    controller: 'CmsQuestionController',
                    name: 'onderwerp',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            when('/campagne/:campaignId/thema/:themeId', _.extend({
                    templateUrl: 'html/cms/topic.html',
                    controller: 'CmsTopicController',
                    name: 'thema',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            when('/campagne/:campaignId/thema', _.extend({
                    templateUrl: 'html/cms/topic.html',
                    controller: 'CmsTopicController',
                    name: 'thema',
                    resolve: {
                        checkAdmin: checkAdmin
                    }
                },
                mainLayout)).
            otherwise({
                redirectTo: '/inloggen'
            });

        $locationProvider
            .html5Mode(false);


    }]).run(['$rootScope', '$route', '$httpBackend', function ($rootScope, $route, $httpBackend) { // app is running

    $rootScope.layoutPartial = function (partialName) {
        if ($route && $route.current) {
            return $route.current[partialName];
        }
    };
}]);

;(function () {
    'use strict';
    bcApp.controller('CmsCampaignController', ['$log', '$scope', '$location', 'RestService',
        function ($log, $scope, $location, RestService) {

            $scope.campaigns = [
                {
                    id: "123456",
                    name: "Kennistest Hardenberg",
                    active: true
                }
            ];

            $scope.newCampaign = function () {
                $scope.campaign = {};
            };

            $scope.editCampaign = function (index) {
                $scope.campaign = $scope.campaigns[index];
            };

            $scope.selectCampaign = function (index) {
                var campaign = $scope.campaigns[index];
                $location.path("campagne/" + campaign.id + "/menu");
            };

            $scope.cancel = function () {
                $scope.campaign = null;
            };

            $scope.save = function () {
                if ($scope.campaign.id && $scope.campaign.id != "") {
                    RestService.updateCampaign($scope.campaign).then(function () {
                        //getCampaigns();
                    });
                } else {
                    RestService.insertCampaigns($scope.campaign).then(function () {
                        //getCampaigns();
                    });
                }
            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('CmsLoginController', ['$log', '$scope', '$location', 'RestService',
        function ($log, $scope, $location, RestService) {
            $scope.user = {
                _id: 0,
                username: null,
                password: null
            };

            var getUser = function () {

            };

            getUser();

            $scope.login = function () {
                RestService.loginPerson($scope.user).then(function (user) {
                    if (user) {
                        $scope.user = user;
                        $location.path("campagne");
                    }
                });
            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('CmsMenuController', ['$log', '$scope', '$location', '$routeParams',
        function ($log, $scope, $location, $routeParams) {
            $scope.campaignId = $routeParams.campaignId;

            $scope.selectMenu = function (menu) {
                $scope.campaignId = $routeParams.campaignId;
                $location.path("campagne/" + $scope.campaignId + "/" + menu);
            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('CmsQuestionController', ['$log', '$scope', '$routeParams', '$location', 'RestService',
        function ($log, $scope, $routeParams, $location, RestService) {
            $scope.statements = [];
            var newStatement = { type: "statement" };
            $scope.statement = newStatement;
            $scope.themeId = "";

            var setStatement = function () {
                var statementId = $routeParams.statementId;
                if (statementId && statementId != "") {
                    RestService.getStatement(statementId).then(function (statement) {
                        $scope.statement = statement;

                        if ($scope.statement.info) {
                            setDescription($scope.statement.info);
                        }
                    });
                } else {
                    $scope.statement = null;
                }
            };

            var getStatementsByTheme = function () {
                $scope.campaignId = $routeParams.campaignId;
                $scope.themeId = $routeParams.themeId;
                if ($scope.themeId && $scope.themeId != "") {
                    RestService.getStatementsByTheme($scope.themeId).then(function (statements) {
                        $scope.statements = statements;

                        setStatement();
                    });

                    //RestService.getTheme($scope.themeId).then(function (theme) {
                    //    $scope.theme = theme;
                    //});
                } else {
                    $scope.statements = [];
                    $scope.statement = null;
                }
            };
            getStatementsByTheme();

            $scope.goBack = function () {
                $location.path("campagne/" + $scope.campaignId + "/thema/" + $scope.themeId);
            };

            $scope.newStatement = function () {
                $scope.statement = newStatement;
                $scope.description = "";
                $location.path("campagne/" + $scope.campaignId + "/thema/" + $scope.themeId + "/onderwerp");
            };

            $scope.editStatement = function ($index) {
                $scope.statement = $scope.statements[$index];
console.log("campagne/" + $scope.campaignId + "thema/" + $scope.themeId + "/onderwerp/" + $scope.statement.id);
                $location.path("campagne/" + $scope.campaignId + "/thema/" + $scope.themeId + "/onderwerp/" + $scope.statement.id);
            };

            var setDescription = function (description) {
                //description = description.split("</br>").join("");
                //$scope.description.information = description.substring(description.indexOf("<p>") + 3, description.indexOf("</p>"));
                //description = description.substring(description.indexOf("</p>") + 4, description.length);

                //$scope.description.link = description.substring(description.indexOf("<span>") + 6, description.indexOf("</span>"));
                //description = description.substring(description.indexOf("</span>") + 7, description.length);

                //$scope.description.decision = description.substring(description.indexOf("<span>") + 6, description.indexOf("</span>"));
//console.log("set description = ", $scope.description);
            };

            $scope.$watch('description.information', function () {
                updateDescription();
            });
            $scope.$watch('description.link', function () {
                updateDescription();
            });
            $scope.$watch('description.decision', function () {
                updateDescription();
            });

            var updateDescription = function () {
                if ($scope.statement) {
//console.log("update description = ", $scope.description);
//                    var description = "";
//                    if ($scope.description.information) {
//                        description = description + "<p>" + $scope.description.information + "</p>";
//                    }
//                    if ($scope.description.link) {
//                        description = description + "<span></br>" + $scope.description.link + "</br></br></span>";
//                    }
//                    if ($scope.description.decision) {
//                        description = description + "<p>Besluitvorming</p>";
//                        description = description + "<span>" + $scope.description.decision + "</span>";
//                    }
//
//                    $scope.statement.description = description;
//console.log("update statement = ", $scope.statement);
                }
            };

            $scope.cancel = function () {
                $location.path("campagne/" + $scope.campaignId + "/thema/" + $scope.themeId + "/onderwerp");
                $scope.statement = null;
            };

            $scope.isSaving = false;
            $scope.save = function () {
                $scope.isSaving = true;

                //RestService.updateTheme($scope.theme).then(function () {
                    saveStatement();
                //});
            };

            var saveStatement = function () {

                if ($scope.statement && $scope.statement.id != "") {
                    RestService.updateStatement($scope.statement).then(function () {
                        $scope.isSaving = false;
                    });
                } else {
                    $scope.statement.topicId = $scope.themeId;
                    delete $scope.statement.id;

                    RestService.insertStatement($scope.themeId, $scope.statement).then(function (statementId) {
                        var newId = statementId.replace(/"/g,"");
                        $scope.statement.id = newId;
                        $scope.isSaving = false;
                        $location.path("thema/" + $scope.themeId + "/onderwerp/" + newId);
                    });
                }
            };

            $scope.deleteStatement = function (index) {
                var statement = $scope.statements[index];
                if (statement.id && statement.id != "") {
                    RestService.deleteStatement(statement).then(function () {
                        getStatementsByTheme();
                    });
                }
            };

            $scope.parseImageUrl= function (imageId) {
                var imageUrl = imageId ? "question-" + imageId + ".jpg" : "";
                return imageUrl;
            };

        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('CmsResultController', ['$log', '$scope', '$location', '$routeParams', 'RestService',
        function ($log, $scope, $location, $routeParams, RestService) {

            var getAllResults = function () {
                RestService.getScores().then(function (scores) {
                    $scope.scores = _.filter(scores, function (item) {
                        return item !== null && item !== "null";
                    });
                });
            };
            getAllResults();


            $scope.selectWinner = function () {
                var winners = _.filter($scope.scores, function (item) {
                    return item.scoreCount === 15;
                });

                if (winners.length == 0) {
                    winners = _.filter($scope.scores, function (item) {
                        return item.scoreCount === 14;
                    });
                }

                if (winners.length == 0) {
                    winners = _.filter($scope.scores, function (item) {
                        return item.scoreCount > 10;
                    });
                }

                var randomWinner = Math.floor(Math.random() * winners.length);
//console.log("select a random winner = ", randomWinner, winners);
                $scope.winner = winners[randomWinner];
            };

            $scope.goBack = function () {
                $scope.campaignId = $routeParams.campaignId;
                $location.path("campagne/" + $scope.campaignId + "/menu");
            };
        }
    ]);
}());
;(function () {
    'use strict';
    bcApp.controller('CmsTopicController', ['$log', '$scope', '$routeParams', '$location', 'RestService', 'DataService',
        function ($log, $scope, $routeParams, $location, RestService, DataService) {
            $scope.themes = [];
            $scope.theme = null;

            var getThemes = function () {
                $scope.campaignId = $routeParams.campaignId;
                $scope.theme = null;

                RestService.getThemes().then(function (themes) {
                    $scope.themes = themes;
                });
            };
            getThemes();

            $scope.goBack = function () {
                $location.path("campagne/" + $scope.campaignId + "/menu");
            };

            $scope.newTheme = function () {
                $scope.theme = {};
            };

            $scope.editTheme = function (index) {
                $scope.theme = $scope.themes[index];
            };

            $scope.selectTheme = function (index) {
                var theme = $scope.themes[index];
                $location.path("campagne/" + $scope.campaignId + "/thema/" + theme.id + "/onderwerp");
            };

            $scope.deleteTheme = function (index) {
                var theme = $scope.themes[index];
                if (theme.id && theme.id != "") {
                    RestService.deleteTheme(theme).then(function () {
                        getThemes();
                    });
                }
            };

            $scope.cancel = function () {
                $scope.theme = null;
            };

            $scope.save = function () {
                if ($scope.theme.id && $scope.theme.id != "") {
                    RestService.updateTheme($scope.theme).then(function () {
                        getThemes();
                    });
                } else {
                    RestService.insertTheme($scope.theme).then(function () {
                        getThemes();
                    });
                }
            };


        }
    ]);
}());
;(function () {
    'use strict';
    bcDirectives.directive('coenCms', function () {
        return {
            restrict: 'AE',
            replace: true,
            controller: ['$scope', '$rootScope', '$route', '$routeParams', '$window',
                function ($scope, $rootScope, $route, $routeParams, $window) {

                    $rootScope.$on('$routeChangeStart', function (evt, absNewUrl, absOldUrl) {
                        if ($route.current) {
//console.log("start = ", $route.current.$$route, $routeParams);
                        }
                        $window.scrollTo(0, 0);
                    });

                    $rootScope.$on('$routeChangeSuccess', function () {
//console.log("success = ", $route, $routeParams);
                        if ($route.current && $route.current.$$route) {
                            var routeParam = $route.current.$$route.name;
//console.log("go select menu from route = ", routeParam);
                            $rootScope.$broadcast('navigate', routeParam);
                        }
                    });

                    $scope.menu = null;
                    $scope.$on('menu:select', function (event, data) {
                        var menu = data.menu;
                        $scope.menu = menu;
//console.log("app selected menu = ", $scope.menu);
                    });

                }

            ],
            templateUrl: 'html/directives/cmsTemplate.html'

        };
    });
}());;(function () {
    'use strict';
    bcDirectives.directive('iconFontLibrary', ['DataService', function (DataService) {
        return {
            restrict: 'AE',
            replace: false,
            templateUrl: 'html/directives/plugins/iconFontLibrary.html'
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('iconFont', ['DataService', function (DataService) {
        return {
            restrict: 'AE',
            replace: false,
            scope: {
                iconClass: "@"
            },
            controller: ['$scope', '$rootScope', '$timeout', function ($scope, $rootScope, $timeout) {
                $scope.$watch('iconClass', function () {
                    $scope.iconClass = $scope.iconClass || "";
                    $scope.iconLink = "#" + $scope.iconClass;
                });
            }],
            template: '<svg class="icon" ng-class="iconClass" ><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{iconLink}}"></use></svg>'
        };
    }]);
}());;(function () {
    'use strict';
    bcDirectives.directive('imageThumb', function () {
        return {
            restrict: 'A',
            replace: false,
            scope: {
                'imageUrl': '=',
                'imageId': '=',
                'altText': '@?',
                'title': '@?'
            },
            link: function (scope, element, attrs, ngModel) {

                scope.setBackground = function () {

                    var timestamp = new Date().getTime();
                    $(element).find('#image').css('background', 'none');
                    $(element).find('#image').css('background', 'url(' + scope.parsedUrl + '?' + timestamp + ') no-repeat bottom center');
                    $(element).find('#image').css('background-color', '#000');
                    $(element).find('#image').css('background-size', 'cover');
                    $(element).find('#image').css('-o-background-size', 'cover');
                    $(element).find('#image').css('-moz-background-size', 'cover');
                    $(element).find('#image').css('-webkit-background-size', 'cover');
                    $(element).find('#image').css('background-position', 'center bottom');
                };

                $(element).find('#image').css('background-color', '#000');
            },
            controller: ['$scope', 'RestService', function ($scope, RestService) {

                $scope.$watch('imageUrl',function(){
                    parseUrl();
                });

                $scope.$on('upload:complete',function(){
                    parseUrl();
                });

                var parseUrl = function () {
                    if ($scope.imageUrl) {
                        $scope.parsedUrl = Config.webUrl + Config.imageUrl + imageSize() + "/" + $scope.imageUrl;
                        $scope.parsedId = "image" + $scope.imageId;
                        $scope.setBackground();
                    }
                };

                var imageSize = function () {
                    var windowWidth = $(window).width();
                    if (windowWidth <= 320) {
                        return 320;
                    } else if (windowWidth <= 480) {
                        return 480;
                    } else if (windowWidth <= 768) {
                        return 768;
                    } else if (windowWidth <= 992) {
                        return 992;
                    } else if (windowWidth <= 1200) {
                        return 1200;
                    } else {
                        return 1200;
                    }
                }

            }],
            template: "<img id='image' alt='{{ altText }}' title='{{ title }}'/>"
        };
    });
}());;/**
 * Created by Edyon on 11-2-2015.
 */
(function () {
    'use strict';
    bcDirectives.directive('uploadFile', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                'questionId': '='
            },
            link: function (scope, element, attrs) {
                scope.progress = 0;

                scope.getPercentage = function () {
                    return (100 / scope.progress).toFixed(2);
                };

                var handleProgress = function (progress) {
//console.log("upload process = ", progress);
                    scope.progress = progress;
                };

                var handleComplete = function (data) {
//console.log("upload complete = ", data);
                    $rootScope.$broadcast("upload:complete");
                };

                var handleError = function () {

                };

                scope.initUpload = function () {
                    console.log("init upload for question = " + scope.questionId, $(element).find('#fileupload'));
                    $(element).find('#fileupload').fileupload({
                        xhrFields: {
                            withCredentials: true
                        },
                        url: Config.apiUrl + "/cms/question/" + scope.questionId + "/image",
                        maxFileSize: 20000000,
                        contentType: 'multipart/form-data',
                        processData: false,
                        autoUpload: false,
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                        process: [
                            {
                                action: 'load',
                                fileTypes: /^image\/(gif|jpeg|png)$/,
                                maxFileSize: 20000000 // 20MB
                            },
                            {
                                action: 'resize',
                                maxWidth: 1440,
                                maxHeight: 900
                            },
                            {
                                action: 'save'
                            }
                        ],
                        add: function (e, form) {
                            console.log("upload add = ", e, form);
                            form.submit();
                        },
                        complete: function (e, data) {
//console.log("upload complete = ", data);
                            if (data == "error") {
                                handleError();
                            } else {
                                handleComplete(data);
                            }
                            return false;
                        },
                        done: function (data) {
//console.log("upload done = ", data);
//		                var filess= data.files[0];
//			            var filenam = filess.name;
//			            data.context.text('');
//			            $(data.context).append('<td class="name" colspan="6">'+filenam+' uploaded Success</td>');

                            //handleComplete(data);
                        },
                        progressall: function (e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            handleProgress(progress);
                        }
                    });
                };
            },
            controller: ['$log', '$scope', '$location', '$route', '$sce', '$window', '$timeout',
                function ($log, $scope, $location, $route, $sce, $window, $timeout) {
                    $scope.$watch("questionId", function () {
                        if ($scope.questionId != "") {
                            $scope.uploadUrl = $scope.parseActionUrl();
                            //$scope.initUpload();
                        }
                    });

                    $scope.parseActionUrl = function () {
                        return $sce.trustAsResourceUrl(Config.apiUrl + '/cms/question/' + $scope.questionId + '/image');
                    };

                    $scope.refreshPage = function () {
                        $timeout(function () {
                            $window.location.reload();
                        }, 1000);
                    };
                }
            ],
            templateUrl: 'html/directives/uploadFileTemplate.html'
        };
    }
    ]);
}());;(function () {
    'use strict';
    bcServices.factory('DataService', ['$log', '$rootScope', '$q', '$location', 'RestService',
        function ($log, $rootScope, $q, $location, RestService) {

            /******************************************************
             * check if user is logged or redirect to home
             *******************************************************/
            var _user = null;
            var checkAdmin = function () {
                var deferred = $q.defer();
//$log.log("data service: checking for user = ", _user);
                if (!_user) {
                    RestService.getLoggedUser().then(function (user) {
                        var success = user && user != "null";
                        if (success) {
                            setAdmin(user);
                        }
                        deferred.resolve(user);

                    }, function () { // handle 401 not authorised
                        setAdmin(null);
                        deferred.resolve(null);
                        $location.path("inloggen");
                    });
                } else {
                    deferred.resolve(_user);
                }
                return deferred.promise;
            };

            var setAdmin = function (user) {
                _user = user;
//$log.log("data service: set user = ", _user);
                $rootScope.$broadcast('user', {user: _user});
            };

            var getAdmin = function () {
                return _user;
            };


            return {
                checkAdmin: checkAdmin,
                setAdmin: setAdmin,
                getAdmin: getAdmin
            };
        }
    ]);
}());;(function () {
    'use strict';
    bcServices.factory('RestService', ['$http', '$q', '$location',
        function ($http, $q, $location) {

            var handleError = function (data, status) {
                if (status == -1) {
                    $location.path('inloggen');
                }
            };

            // login admin
            var loginPerson = function (person) {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/admin/login", person, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject();
                });
                return deferred.promise;
            };

            // logout admin
            var logoutPerson = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/admin/logout", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject();
                });
                return deferred.promise;
            };

            // check user
            var getLoggedUser = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/admin", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get themes
            var getThemes = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/cms/topic/all", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get themes
            var getStatementsByTheme = function (themeId) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/cms/topic/" + themeId + "/question", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get theme
            var getTheme = function (themeId) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/cms/topic/" + themeId, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // insert theme
            var insertTheme = function (theme) {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/cms/topic", theme, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // update theme
            var updateTheme = function (theme) {
                var deferred = $q.defer();
                $http.put(Config.apiUrl + "/cms/topic/"+ theme.id, theme, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // delete theme
            var deleteTheme = function (theme) {
                var deferred = $q.defer();
                $http.delete(Config.apiUrl + "/cms/topic/"+ theme.id, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get statement
            var getStatement = function (statementId) {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/cms/question/" + statementId, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // insert statement in theme
            var insertStatement = function (themeId, statement) {
                var deferred = $q.defer();
                $http.post(Config.apiUrl + "/cms/topic/" + themeId + "/question", statement, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // update statement
            var updateStatement = function (statement) {
                var deferred = $q.defer();
                $http.put(Config.apiUrl + "/cms/question/" + statement.id, statement, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // delete statement
            var deleteStatement = function (statement) {
                var deferred = $q.defer();
                $http.delete(Config.apiUrl + "/cms/question/" + statement.id, {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data, status) {
                    handleError(data, status);
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            // get scores
            var getScores = function () {
                var deferred = $q.defer();
                $http.get(Config.apiUrl + "/cms/scores", {
                    withCredentials: true
                }).success(function (data) {
                    deferred.resolve(data);
                }).error(function (data) {
                    deferred.reject(data);
                });
                return deferred.promise;
            };

            return {
                loginPerson: loginPerson,
                logoutPerson: logoutPerson,
                getLoggedUser: getLoggedUser,
                getThemes: getThemes,
                getStatementsByTheme: getStatementsByTheme,
                getTheme: getTheme,
                insertTheme: insertTheme,
                updateTheme: updateTheme,
                deleteTheme: deleteTheme,
                getStatement: getStatement,
                insertStatement: insertStatement,
                updateStatement: updateStatement,
                deleteStatement: deleteStatement,
                getScores: getScores
            };
        }
    ]);
}());