This repo contains the code of COEN application and is for reviewing and demo purpose only.

# The app
The app is a research tool that can be used by multiple clients with their own styling. The client can add some multiple choice, open or statement questions in the CMS and view these in the web app. With the app any user can answer questions using like/swipe or just touching the right answer. The tool is an easy to use research tool that shows the results in the app or advanced results in the CMS.

# Frameworks
- Gulp and Webpack:
  Used to compile the code into 1 single minified js file and run the app on a server on localhost:8000
- Angular 1.5: 
  An Angular components setup is used to clearly separate functionality into reusable components. Each component can have its own html template, controller and css styling. This code is migrated from Angular 1.3 and uses 1.5 now to be ready to be migrated to Angular 2 as the components setup is Angular 2 ready.  
- SASS/CSS:
  SASS is used to write styling for several client using variables. Webpack compiles the SASS into CSS while building the minified js file.
- Karma/PhantomJS/Jasmine:
  Jasmine is used to write unit tests for each component running with Karam and PhantomJS.

# How to install
- checkout the project
- run 'npm install'
- run 'gulp run' OR 'gulp run <project>'
- go to localhost:8000

# How to test
- checkout the project
- run 'npm install'
- run 'gulp test'
- open test/coverage/index/html to see the results
