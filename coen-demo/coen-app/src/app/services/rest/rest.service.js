class RestService {

    constructor($http, $q) {
        this.$http = $http;
        this.$q = $q;

        this._apiUrl = Config.apiUrl;
    }

    // get facebook login window
    getLoginWindow(platform) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/person/authentication-url?platform=" + platform, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // check user
    getLoggedUser(user) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/person", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // login person
    loginPerson(person) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/person", person, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // save person
    savePerson(person) {
        var deferred = this.$q.defer();
        this.$http.put(this._apiUrl + "/person", person, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // start survey
    startSurvey(campaign) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/survey", campaign, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get incomplete questionnaires
    getSurvey() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get complete questionnaires
    getCompleteSurvey() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey/complete", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get survey
    getSurveyById(questionnaireId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey/" + questionnaireId, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get statements
    getStatements() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/question", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get statement
    getStatement(statementId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/question/" + statementId, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // answer question
    answerQuestion(surveyId, questionId, answer) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/survey/" + surveyId + "/question/" + questionId, answer, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get theme score
    getThemeScore(surveyId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey/" + surveyId + "/theme-scores", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get total score
    getTotalScore(surveyId) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey/" + surveyId + "/theme-scores", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get theme scores
    getThemeScoresCount() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/theme-scores/count", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get people count
    getPeopleCount() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/person/count", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get cool people score
    getCoolCount() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/person/cool-people/count", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get profiles
    getProfiles() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/profiles", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get persons
    getPersons() {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/person/all", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get score
    getScore(min, max) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey/scorecount?min=" + min + "&max=" + max, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    // get score
    getResults(min, max) {
        var deferred = this.$q.defer();
        this.$http.get(this._apiUrl + "/survey/results", {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    sendEmail(email) {
        var deferred = this.$q.defer();
        this.$http.post(this._apiUrl + "/share", email, {
            withCredentials: true
        }).then((data) => {
            deferred.resolve(data.data);
        }).catch((data) => {
            deferred.reject(data);
        });
        return deferred.promise;
    };

    static RestFactory($http, $q) {
        return new RestService($http, $q);
    }
}

RestService.RestFactory.$inject = ['$http', '$q'];

angular.module('coen.services.rest', [])
    .factory('RestService', RestService.RestFactory);

export default RestService;