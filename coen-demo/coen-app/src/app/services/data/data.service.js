class DataService {

    constructor($rootScope, $q, $location, $route, RestService, ConfigService) {
        this.$rootScope = $rootScope;
        this.$q = $q;
        this.$location = $location;
        this.$route = $route;

        this.RestService = RestService;
        this.ConfigService = ConfigService;

        this._user = null;
        this._survey = null;
        this._themeScores = null;
        this._totalScores = null;


        this._answer;
        this._themes = [];
        this._votes = [];
        this._themeVotes = [];
        this._profiles = [];
        this._profile = null;
    }

    /******************************************************
     * check if user is logged or redirect to home
     *******************************************************/
    checkUser() {
        var deferred = this.$q.defer();
// console.log("data service: checking for user = ", this._user);
        if (!this._user) {
            this.RestService.getLoggedUser().then((user) => {
                var success = user && user != "null";
                if (success) {
                    this.setUser(user);
                    deferred.resolve(user);
                } else {
                    this.setUser(null);
                    deferred.reject(null);

                    this.$location.path("home");
                }
            }).catch(error => {
                this.setUser(null);
                deferred.reject(null);
                this.$location.path("home");
            });
        } else {
            deferred.resolve(this._user);
        }
        return deferred.promise;
    };

    setUser(user) {
        this._user = user;
        this.$rootScope.$broadcast('user', {user: this._user});
    };

    getUser() {
        return this._user;
    };

    /******************************************************
     * set the user survey
     *******************************************************/
    checkSurvey() {
        var campaignId = Config.campaignId;
        var deferred = this.$q.defer();

        if (!this._survey) {
            this.RestService.getSurvey().then((survey) => {
                if (survey) {
                    this.setSurvey(survey);
                    deferred.resolve(survey);
                } else {
                    var sortBy = this.ConfigService.getSortQuestions();
                    var campaign = {campaignId: campaignId || null, sortBy: sortBy};
                    this.RestService.startSurvey(campaign).then((survey) => {
                        this.setSurvey(survey);
                        deferred.resolve(survey);
                    });
                }
            }).catch((error) => {
                this.setSurvey(null);
                deferred.reject();
            });
        } else {
            deferred.resolve(this._survey);
        }

        return deferred.promise;
    };

    setSurvey(survey) {
        this._survey = survey;
        this.$rootScope.$broadcast('survey', {survey: this._survey});
    };

    getSurvey() {
        return this._survey;
    };

    /******************************************************
     * set the user scores
     *******************************************************/
    checkThemeScores() {
        var deferred = this.$q.defer();

        if (!this._themeScores) {
            if (this._survey) {
                this.RestService.getThemeScore(this._survey.id).then((themeScores) => {
                    this.setThemeScores(themeScores);
                    deferred.resolve(themeScores);
                }).catch(error => {
                    this.setThemeScores(null);
                    deferred.reject();
                    this.$location.path("home");
                });
            }
            else { // no active survey found
                this.setThemeScores(null);
                deferred.resolve();
                this.$location.path("statement");
            }
        } else {
            deferred.resolve(this._themeScores);
        }
        return deferred.promise;
    };

    setThemeScores(themeScores) {
        this._themeScores = themeScores;
        this.$rootScope.$broadcast('themeScores', {themeScores: this._themeScores});
    };

    getThemeScores() {
        return this._themeScores;
    };

    /******************************************************
     * set the total scores
     *******************************************************/

    checkTotalScores() {
        var deferred = this.$q.defer();

        if (!this._totalScores) {
            if (this._survey) {
                this.RestService.getTotalScore(this._survey.id).then((totalScores) => {
                    this.setTotalScores(totalScores);
                    deferred.resolve(totalScores);
                    this.$location.path("about");
                }).catch(error => {
                    this.setTotalScores(null);
                    deferred.reject();
                    this.$location.path("home");
                });
            }
            else { // no active survey found
                this.setTotalScores(null);
                deferred.resolve();
                this.$location.path("statement");
            }
        } else {
            deferred.resolve(this._totalScores);
        }
        return deferred.promise;
    };

    setTotalScores(totalScores) {
        this._totalScores = totalScores;
        this.$rootScope.$broadcast('totalScores', {totalScores: this._totalScores});
    };

    getTotalScores() {
        return this._totalScores;
    };

    /**
     * set profiles
     */

    setAnswer(answer, currentIndex, currentPane) {
        this._answer = answer;

        this.$rootScope.$broadcast('next-statement', {
            answer: this._answer,
            currentIndex: currentIndex,
            currentPane: currentPane
        });
    };

    getAnswer() {
        return this._answer;
    };

    setThemes(themes) {
        this._themes = themes;
    };

    setVotes(votes) {
        this._votes = answers;
    };

    getVotes() {
        return this._votes;
    };

    setThemeVotes(themeVotes) {
        this._themeVotes = themeVotes;
    };

    getThemeVotes() {
        return this._themeVotes;
    };

    setProfiles(profiles) {
        this._profiles = profiles;
    };

    getProfiles() {
        return this._profiles;
    };

    getProfileByScore(score, profiles) {
        var selectedProfile = _.find(profiles, function (profile) {
            return profile.score === score;
        });
        return selectedProfile ? selectedProfile : profiles[0];
    };

    setProfile(profile) {
        this._profile = profile;
        this.$rootScope.$broadcast('profile', {profile: this._profile});
    };

    getProfile() {
        return this._profile;
    };

    static DataFactory($rootScope, $q, $location, $route, RestService, ConfigService) {
        return new DataService($rootScope, $q, $location, $route, RestService, ConfigService);
    }
}

DataService.DataFactory.$inject = ['$rootScope', '$q', '$location', '$route', 'RestService', 'ConfigService'];

angular.module('coen.services.data', [])
    .factory('DataService', DataService.DataFactory);

export default DataService;