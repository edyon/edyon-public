class HighchartService {

    constructor($rootScope, $window, DataService) {
        this.$rootScope = $rootScope;
        this.$window = $window;


        this._user = DataService.getUser();
        this._seriesId = 0;
        this._axisNames = ["xAxis", "yAxis"];
        this._prevOptions = {};
    }

    /**
     * initialise the chart
     */
    initialiseChart(scope, element, config, theme) {
        config = config || {};
        var mergedOptions = this.getMergedOptions(scope, element, config);
//console.log("init chart with options = ", mergedOptions, " -- theme = ", theme);
        var chart = config.useHighStocks ? new Highcharts.StockChart(mergedOptions) : new Highcharts.Chart(Highcharts.merge(mergedOptions, theme));

        for (var i = 0; i < this._axisNames.length; i++) {
            if (config[this._axisNames[i]]) {
                this.processExtremes(chart, config[this._axisNames[i]], this._axisNames[i]);
            }
        }
        if (config.series && config.series.length > 0) {
            this.processSeries(chart, config.series);
        }
        if (config.loading) {
            chart.showLoading();
        }
        chart.redraw();
        return chart;
    };

    /**
     * convert theme scores to chart data
     */
    convertToScore(results, agree) {
        var chartData = [];

        _.each(results, (result) => {
            if (result.answers) {
                let firstResult = result.answers[0];
                let chartValue = 0;
                _.each(result.answers, (answer) => {
                    chartValue = answer.correct ? 1 : 0;
                    chartData.push([answer.text, chartValue]);
                });
            }
        });
        return chartData;
    }

    getChartConfig(dataAgree, dataDisagree, bFull, barType) {
        var chartConfig = {
            series: [
                {
                    type: 'column',
                    data: dataDisagree,
                    name: '-',
                    tooltip: {enabled: false},
                    borderColor: '#7f1016',
                    dataLabels: {}
                },
                {
                    type: 'column',
                    data: dataAgree,
                    name: '+',
                    tooltip: {enabled: false},
                    marker: {
                        symbol: 'triangle'
                    },
                    borderColor: '#E20613',
                    dataLabels: {
                        x: 30,
                        y: 30
                    }
                }

            ],
            yAxis: {
                title: {
                    text: "Aantal Vragen",
                    align: barType === "column" ? "middle" : "high",
                    margin: 20,
                    style: {
                        fontSize: '20px',
                        fontFamily: 'Lato, sans-serif',
                        'text-transform': 'uppercase',
                        fontWeight: 'bold'
                    },
                    opposite: barType === "column" ? true : false
                },
                min: 0,
                max: 3
            },
            xAxis: {
                type: 'category',
                title: {
                    text: "Thema's",
                    align: barType === "column" ? "high" : "middle",
                    style: {
                        fontSize: '20px',
                        fontFamily: 'Lato, sans-serif',
                        'text-transform': 'uppercase',
                        fontWeight: 'bold',
                        textAlign: 'right'
                    }
                },
                offset: 0,
                labels: {
                    useHTML: true,
                    formatter: () => {
                        return this.parseChartInfo(this.value, bFull, barType);
                    }
                }
            },
            useHighStocks: false,
            options: {
                chart: {
                    spacingTop: 30
                },
                bar: {
                    borderWidth: 0
                },
                navigator: {
                    enabled: false
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    },
                    series: {
                        cursor: 'default',
                        point: {
                            events: {
                                click: () => {
                                    //openChartInfo(this.category, bFull);
                                }
                            }
                        }
                    }
                },
                credits: {
                    enabled: false
                }
            }
        };

        return chartConfig;
    };

    parseChartInfo(themeName, bFull, barType) {
        var infoClass = "chart-info " + barType;
        var divInfo = '<div class="' + infoClass + '">';
        var btn = "btn-info-chart btn-" + barType;
        if (bFull) divInfo += '<div id="' + themeName + '" class="' + btn + '"><span>i</span></div>';
        divInfo += '<div class="chart-theme"><h3>' + themeName + '</h3></div>';
        divInfo += '</div>';
        return divInfo;
    }

    openChartInfo(category, bFull) {
        if (bFull) {
            this.$rootScope.$broadcast("chart:info", {'category': category});
        }
    }

    animationComplete() {
        this.$rootScope.$broadcast("chart:animated");
    }

    chartComplete() {
        this.$rootScope.$broadcast("chart:complete");
    }

    /**
     * get the highcharts theme
     *
     */
    getChartTheme(barType) {

        /***
         @coen-red: #e52520;
         @coen-lightred: #FFDEE1;
         @coen-darkred: #7e1016;
         @coen-link: #f5f5f5;
         @coen-green: #91c21b;

         @coen-darkgrey: #262626;
         @coen-grey: #4b3b3c;
         @coen-lightgrey: #F2F2F2;
         @coen-white: #ffffff;
         @coen-black: #000000;
         ****/

        var theme = {
            colors: ["#7e1016", "#e52520", "#e52520", "#7CAAD6", "#96B439", "#903060", "#F09000", "#55BF3B", "#DF5353", "#7798BF", "#D8DA86"],

            chart: {
                backgroundColor: "rgba(255, 255, 255, 0)",
                type: barType,
                style: {
                    fontFamily: "Lato, sans-serif"
                },
                events: {
                    redraw: (event) => {
                        this.animationComplete();
                    },
                    load: (event) => {
                        this.chartComplete();
                    }
                }
            },
            title: {
                style: {
                    color: '#262626'
                },
                text: ''
            },
            subtitle: {
                style: {
                    color: '#DDD'
                }
            },
            tooltip: {enabled: false},
            plotOptions: {
                series: {
                    pointWidth: (barType === "column" ? 40 : 40) //width of the column bars irrespective of the chart size
                }
            },
            xAxis: {
                gridLineWidth: 0,
                lineColor: '#999',
                tickColor: '#191919',
                tickWidth: 0,
                tickPosition: 'inside',
                labels: {
                    style: {
                        color: '#999',
                        fontWeight: 'bold'
                    }
                },
                title: {
                    style: {
                        color: '#AAA',
                        fontSize: '16px'
                    }
                },
                events: {
                    afterSetExtremes: () => {
                        //initAnnotations();
                    }
                }
            },
            yAxis: {
                alternateGridColor: null,
                tickInterval: 1,
                minorTickInterval: 1,
                gridLineColor: 'rgba(0, 0, 0, 0)',
                minorGridLineColor: 'rgba(0, 0, 0, 0)',
                lineColor: '#999',
                lineWidth: 1,
                tickColor: '#191919',
                tickWidth: 1,
                tickPosition: 'inside',
                labels: {
                    style: {
                        color: '#999',
                        fontWeight: 'bold',
                        fontSize: '16px'
                    }
                },
                title: {
                    style: {
                        color: '#AAA',
                        font: '16px'
                    }
                }
            },
            legend: {
                enabled: false
            }
        };

        return theme;
    };

    prependMethod(obj, method, func) {
        var original = obj[method];
        obj[method] = () => {
            var args = Array.prototype.slice.call(arguments);
            func.apply(this, args);
            if (original) {
                return original.apply(this, args);
            } else {
                return;
            }

        };
    }

    deepExtend(destination, source) {
        for (var property in source) {
            if (source[property] && source[property].constructor &&
                source[property].constructor === Object) {
                destination[property] = destination[property] || {};
                this.deepExtend(destination[property], source[property]);
            } else {
                destination[property] = source[property];
            }
        }
        return destination;
    }

    ensureIds(series) {
        angular.forEach(series, (s) => {
            if (!angular.isDefined(s.id)) {
                s.id = "series-" + this._seriesId++;
            }
        });
    };

    getAxisNames() {
        return this._axisNames;
    };

    getMergedOptions(scope, element, config) {
        var mergedOptions = {};

        var defaultOptions = {
            chart: {
                events: {},
                options: {}
            },
            title: {},
            subtitle: {},
            series: [],
            credits: {},
            plotOptions: {},
            navigator: {enabled: false}
        };

        if (config.options) {
            mergedOptions = this.deepExtend(defaultOptions, config.options);
        } else {
            mergedOptions = defaultOptions;
        }

        mergedOptions.chart.renderTo = element[0];

        this._axisNames.forEach((axisName) => {
            if (config[axisName]) {
                this.prependMethod(mergedOptions.chart.events, 'selection', (e) => {
                    var thisChart = this;
                    if (e[axisName]) {
                        scope.$apply(() => {
                            scope.config[axisName].currentMin = e[axisName][0].min;
                            scope.config[axisName].currentMax = e[axisName][0].max;
                        });
                    } else {
                        //handle reset button - zoom out to all
                        scope.$apply(() => {
                            scope.config[axisName].currentMin = thisChart[axisName][0].dataMin;
                            scope.config[axisName].currentMax = thisChart[axisName][0].dataMax;
                        });
                    }
                });

                this.prependMethod(mergedOptions.chart.events, 'addSeries', (e) => {
                    scope.config[axisName].currentMin = this[axisName][0].min || scope.config[axisName].currentMin;
                    scope.config[axisName].currentMax = this[axisName][0].max || scope.config[axisName].currentMax;
                });

                mergedOptions[axisName] = angular.copy(config[axisName]);
            }
        });

        if (config.title) {
            mergedOptions.title = config.title;
        }

        if (config.subtitle) {
            mergedOptions.subtitle = config.subtitle;
        }

        if (config.credits) {
            mergedOptions.credits = config.credits;
        }
        return mergedOptions;
    };

    updateZoom(axis, modelAxis) {
        var extremes = axis.getExtremes();
        if (modelAxis.currentMin !== extremes.dataMin || modelAxis.currentMax !== extremes.dataMax) {
            axis.setExtremes(modelAxis.currentMin, modelAxis.currentMax, false);
        }
    };

    processExtremes(chart, axis, axisName) {
        if (axis.currentMin || axis.currentMax) {
            chart[axisName][0].setExtremes(axis.currentMin, axis.currentMax, true);
        }
    };

    chartOptionsWithoutEasyOptions(options) {
        return angular.extend({}, options, {data: null, visible: null});
    };


    processSeries(chart, series) {
        var ids = [];
        if (series) {
            this.ensureIds(series);

            //Find series to add or update
            angular.forEach(series, (s) => {
                ids.push(s.id);

                var chartSeries = chart.get(s.id);
                if (chartSeries) {
                    if (!angular.equals(this._prevOptions[s.id], this.chartOptionsWithoutEasyOptions(s))) {
                        chartSeries.update(angular.copy(s), false);
                    } else {
                        if (s.visible !== undefined && chartSeries.visible !== s.visible) {
                            chartSeries.setVisible(s.visible, false);
                        }
                        if (chartSeries.options.data !== s.data) {
                            chartSeries.setData(s.data, false);
                        }
                    }
                } else {
                    chart.addSeries(angular.copy(s), false);
                }

                this._prevOptions[s.id] = this.chartOptionsWithoutEasyOptions(s);
            });
        }

        //Now remove any missing series
        for (var i = chart.series.length - 1; i >= 0; i--) {
            var s = chart.series[i];
            if (indexOf(ids, s.options.id) < 0) {
                s.remove(false);
            }
        }
    };

    beforeInit() {
        var orgHighchartsRangeSelectorPrototypeRender = Highcharts.RangeSelector.prototype.render;
        Highcharts.RangeSelector.prototype.render = (min, max) => {
            orgHighchartsRangeSelectorPrototypeRender.apply(this, [min, max]);
            var leftPosition = this.chart.plotLeft,
                topPosition = this.chart.plotTop + 5,
                space = 2;
            this.zoomText.attr({
                x: leftPosition,
                y: topPosition - 50
            });
            leftPosition += this.zoomText.getBBox().width;
            for (var i = 0; i < this.buttons.length; i++) {
                this.buttons[i].attr({
                    x: leftPosition,
                    y: topPosition - 65
                });
                leftPosition += this.buttons[i].width + space;
            }
        };
    };

    static HighchartFactory($rootScope, $window, DataService) {
        return new HighchartService($rootScope, $window, DataService);
    }
}

HighchartService.HighchartFactory.$inject = ['$rootScope', '$window', 'DataService'];

angular.module('coen.services.highchart', [])
    .factory('HighchartService', HighchartService.HighchartFactory);

export default HighchartService;