class AnswerService {

    constructor($rootScope, $timeout, RestService, ConfigService) {
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;

        this.RestService = RestService;
        this.ConfigService = ConfigService;

        this._survey = null;
        this._currentQuestion = null;
    }

    setSurvey(survey) {
        this._survey = survey;
    };

    setCurrentQuestion(currentQuestion) {
        this._currentQuestion = currentQuestion;
    };

    saveOpenAnswer(answer) {
        if (this._currentQuestion.type == "open") {
            this.saveAnswer(answer, false, false);
        } else {
            this.$rootScope.$broadcast('question:complete');
        }
    };

    saveAnswer(answer, delay, swiped) {
        let timeout = delay ? 300 : 0;
        let isAnswered = this._currentQuestion.correct;
        if (isAnswered == null) { // go answer the question
// console.log("AnswerService: save answer type = ", this._currentQuestion.type, " answer = ", answer, delay, swiped);
            if (this._currentQuestion.type == "multiple" || this._currentQuestion.type == "multiselect") {
                if (swiped) {
                    this.$rootScope.$broadcast('question:reset');
                } else {
                    this.saveOnTimeout(answer, timeout);
                }

            } else if (this._currentQuestion.type == "statement" || this._currentQuestion.type == "question") {
                if (swiped) {
                    if (this._currentQuestion.info && this._currentQuestion.info != "") {
                        this.$rootScope.$broadcast('question:reset');
                    }
                }
                this.saveOnTimeout(answer, timeout);

            } else if (this._currentQuestion.type == "open") {
                if (swiped) {
                    this.$rootScope.$broadcast('question:reset');
                } else {
                    this.goAnswerQuestion(answer);
                }
            }
        } else {
// console.log("AnswerService: question is answered");
            this.$rootScope.$broadcast('question:complete');
        }
    };

    saveOnTimeout(answer, timeout) {
        this.$timeout(() => {
            this.goAnswerQuestion(answer);
        }, timeout);
    };

    /**
     * save the vote and get the correct answer
     */
    goAnswerQuestion(answer) {
        var answer = {answer: answer};

        this.RestService.answerQuestion(this._survey.id, this._currentQuestion.questionId, answer).then((data) => {
            this._currentQuestion.correct = (data.result === true);
// console.log(">>>>>>> closed question is answered is correct = ", this._currentQuestion.correct);
            this.handleSaveQuestionComplete();
        });
    };

    handleSaveQuestionComplete() {
        var autoShowInfo = this.ConfigService.checkShowCorrectInfo();
        if (autoShowInfo) {
            if (this._currentQuestion.info && this._currentQuestion.info != "") {
                this.$rootScope.$broadcast('question:answered');
                return;
            }
        }

        this.$rootScope.$broadcast('question:complete');
    };

    static AnswerFactory($rootScope, $timeout, RestService, ConfigService) {
        return new AnswerService($rootScope, $timeout, RestService, ConfigService);
    }
}

AnswerService.AnswerFactory.$inject = ['$rootScope', '$timeout', 'RestService', 'ConfigService'];

angular.module('coen.services.answer', [])
    .factory('AnswerService', AnswerService.AnswerFactory);

export default AnswerService;
