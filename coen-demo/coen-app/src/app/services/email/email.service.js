class EmailService {

    constructor($window) {
        this.$window = $window;
    }

    getEmailLight(socialPost) {
        var email = "mailto:?to=";
        email += "&subject=" + encodeURIComponent(socialPost.message.message);
        email += "&body=" + encodeURIComponent(socialPost.message.description + " " + Config.shareUrl);
        //email += "&target=_self";
        return email;
    };

    sendEmailLight(email) {
        this.$window.location.href = email;
    };

    getAnswerPost(user, answer) {
        var post = {};
        post.platform = "email";
        post.user = user;
        post.email = "";

        var message = {};
        if (answer) {
            message.message = answer.text;
            message.name = "";
            message.picture = Config.webUrl + Config.imageUrl + "/backgrounds/1200/statement-" + answer.questionId + ".jpg";
            message.description = "Doe ook mee aan de test voor de gemeente Hardenberg";
        }
        post.message = message;
        post.title = "Kennistest. Alcohol, drugs & roken";
        return post;
    };

    getProfilePost(user, profile) {
        var post = {};
        post.platform = "email";
        post.user = user;
        post.email = "";

        var message = {};

        if (profile) {
            message.message = profile.shareText;
            message.name = "";
            message.picture = Config.webUrl + Config.profileUrl + profile.label + ".jpg";
            message.description = profile.teaserText;
        }

        post.message = message;
        post.title = "Kennistest. Alcohol, drugs & roken";
        return post;
    };

    static EmailFactory($window) {
        return new EmailService($window);
    }
}

EmailService.EmailFactory.$inject = ['$window'];

angular.module('coen.services.email', [])
    .factory('EmailService', EmailService.EmailFactory);

export default EmailService;