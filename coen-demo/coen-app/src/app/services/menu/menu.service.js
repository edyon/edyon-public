class MenuService {

    constructor($filter, ConfigService) {
        this.$filter = $filter;

        this.ConfigService = ConfigService;

        this._menus = [
            {
                index: 0, bulletIndex: 0, label: "menu.home.label", title: "home", url: "home",
                showNext: true, showPrevious: false, showHome: true, showSocial: false, showHeader: false
            },
            {
                index: 1, bulletIndex: 1, label: "menu.about.label", title: "about", url: "about",
                showNext: true, showPrevious: false, showVotes: false, showSocial: false, showHeader: true
            },
            {
                index: 2, bulletIndex: 2, label: "menu.info.label", title: "info", url: "info",
                showNext: true, showPrevious: false, showVotes: false, showSocial: false, showHeader: true
            },
            {
                index: 3, bulletIndex: 3, label: "menu.question.label", title: "statement", url: "statement",
                showNext: true, showPrevious: false, showVotes: true, showSocial: true, showHeader: true
            },
            {
                index: 4, bulletIndex: 4, label: "menu.result.label", title: "result", url: "result",
                showNext: true, showPrevious: false, showVotes: false, showSocial: false, showHeader: true
            },
            {
                index: 5, bulletIndex: 5, label: "menu.overview.label", title: "overview", url: "overview",
                showNext: true, showPrevious: false, showVotes: false, showSocial: true, showHeader: true
            },
            {
                index: 6, bulletIndex: 6, label: "menu.social.label", title: "social", url: "social",
                showNext: false, showPrevious: false, showVotes: false, showSocial: false, showHeader: false
            },
            {
                index: 7, bulletIndex: 7, label: "menu.profile.label", title: "profile", url: "profile",
                showNext: true, showPrevious: false, showVotes: false, showSocial: false, showHeader: true
            },
            {
                index: 8, bulletIndex: 8, label: "menu.end.label", title: "end", url: "end",
                showNext: false, showPrevious: false, showVotes: false, showSocial: false, showHeader: false
            }
        ];

        this._profiles = [
            {label: "Beginner", icon: "icon-starter", description: "0 van de 15 goed", score: 1},
            {label: "Middelmaatje", icon: "icon-average", description: "0-5 van de 15 goed", score: 2},
            {label: "Baas", icon: "icon-boss", description: "5-10 van de 15 goed", score: 3},
            {label: "Meesterbrein", icon: "icon-master", description: "10-15 van de 15 goed", score: 4},
            {label: "Eindbaas", icon: "icon-endboss", description: "15 van de 15 goed", score: 5}
        ];

        this._burgerItems = [
            {title: "menu.burger.item1.title", description: "menu.burger.item1.description"},
            {title: 'menu.burger.item2.title', description: "menu.burger.item2.description"}
        ];
    }

    getBurgerItems() {
        return this._burgerItems;
    };

    getQuestionsByMenu(menu) {
        var questions = [];
        switch (menu) {
            case "about":
                var numberOfAboutItems = this.ConfigService.getNumberOfAboutItems();
                for (var i = 1; i < numberOfAboutItems + 1; i++) {
                    var question = {
                        question: "about.question.question" + i,
                        answer: "about.question.question" + i + ".answer"
                    };
                    questions.push(question);
                }
                break;
            case "info":
                var userItemIndex = this.ConfigService.getUserItemIndex();
                var question = {
                    question: "about.question.question" + userItemIndex,
                    answer: "about.question.question" + userItemIndex + ".answer",
                    icon: 'icon-person'
                };
                questions.push(question);
                break;
            case "overview":
                var userItemIndex = this.ConfigService.getUserItemIndex();
                var question = {
                    question: "about.question.question" + userItemIndex,
                    answer: "about.question.question" + userItemIndex + ".answer"
                };
                questions.push(question);
                break;
        }

        return questions;
    };

    getMenu() {
        let allowedMenus = [];
        let configMenu = Config.menu;
        _.each(this._menus, (menuItem) => {
            let allowedMenu = _.find(configMenu, (title) => {
                return title == menuItem.title;
            });

            if (allowedMenu) {
                allowedMenus.push(menuItem);
            }
        });
        return allowedMenus;
    };

    getAfterQuestionnaireMenu() {
        var menus = this.getMenu();
        var menu = _.find(menus, (item) => {
            return item.title === "statement";
        });

        let index = _.indexOf(menus, menu);
        let nextMenu = menus[index + 1];
        return nextMenu ? nextMenu : menus[0];
    };

    createStatementSteps(globalIndex, menuSteps, answers) {
        var surveyLength = answers.length > 1 ? answers.length : 15;
        for (var i = 0; i < surveyLength; i++) {
            var answer = answers[i];
// console.log("---- ", answers, i, answer);
            if (answer) {
                let showVotes = answer.type == "statement" || answer.type == "question";
                let showNext = answer.type == "multiple" || answer.type == "multiselect" || answer.type == "open";
// console.log("answer = ", answer, showVotes, showNext, globalIndex);
                menuSteps.push(
                    {
                        index: globalIndex++,
                        label: 'menu.onderwerp.label',
                        title: 'statement',
                        url: 'statement/' + answer.questionId,
                        active: false,
                        showVotes: showVotes,
                        showNext: showNext,
                        showHeader: true,
                        type: answer.type
                    });
            } else {
                menuSteps.push(
                    {
                        index: globalIndex++,
                        label: 'menu.onderwerp.label',
                        title: 'statement',
                        url: 'statement',
                        active: false,
                        showHeader: true
                    });
            }
        }
        return globalIndex;
    };

    getMenuWithSteps(answers) {
        var menuSteps = [];
        var index = 0;
        var menus = this.getMenu();
        _.each(menus, (menuItem) => {
            if (menuItem.title === 'statement') {
                index = this.createStatementSteps(index, menuSteps, answers);
            }
            else {
                menuItem.index = index;
                menuItem.active = (index === 0);
                menuSteps.push(menuItem);
                index++;
            }
        });
        return menuSteps;
    };


    getProfiles() {
        return this._profiles;
    };

    getProfileByScore(score) {
        let profile = _.find(this._profiles, (profile) => {
            return profile.score === score;
        });
        return profile ? profile : this._profiles[0];
    };

    getProfilesWithScore(profiles, userCount, scores) {
        _.each(profiles, (profile) => {
            profile.correctCount = scores[profile.score - 1];
            profile.userCount = userCount;
            profile.userPercentage = (profile.correctCount / userCount) * 100;
        });
        return profiles;
    };

    getProfileScore(numCorrectAnswers, numQuestions) {
        let modQuestions = numQuestions % 3;
        let numProfiles = Math.floor(numQuestions / 3);

        let score = Math.ceil(numCorrectAnswers / numProfiles);
        score = score + 1;
        let modProfile = numCorrectAnswers % numProfiles;
        let addScore = modProfile == 0 && numCorrectAnswers > 0;
        score = score + (addScore ? 1 : 0);
        return score;
    };

    setShareText(profile, numQuestions, numCorrectAnswers) {
        let shareText = this.$filter('translate')('social.share.result.text');
        if (numCorrectAnswers && numQuestions) {
            shareText = shareText.replace('%NUM_CORRECT%', numCorrectAnswers);
            shareText = shareText.replace('%NUM_QUESTIONS%', numQuestions);
        }
        profile.shareText = shareText;
        profile.teaserText = this.$filter('translate')('social.share.teaser.text');
        profile.titleText = this.$filter('translate')('social.share.title.text');
    };

    static MenuFactory($filter, ConfigService) {
        return new MenuService($filter, ConfigService);
    }
}

MenuService.MenuFactory.$inject = ['$filter', 'ConfigService'];

angular.module('coen.services.menu', [])
    .factory('MenuService', MenuService.MenuFactory);

export default MenuService;
