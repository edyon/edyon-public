import angular from 'angular';

import angularAnimate from 'angular-animate';
import angularAria from 'angular-aria';
import angularCookies from 'angular-cookies';
import angularResource from 'angular-resource';
import angularRoute from 'angular-route';
import angularSanitize from 'angular-sanitize';
import angularTouch from 'angular-touch';
import angularTranslate from 'angular-translate';
import angularTranslateLoaderPartial from 'angular-translate-loader-partial';
import angularBootstrap from 'angular-ui-bootstrap/ui-bootstrap';
import jTinder from 'jquery';
import underscore from 'underscore';

import components from './components/components.module';
import features from './features/features.module';
import services from './services/services.module';
import plugins from './plugins/plugins.module';
import template from './module.html';
import style from './module.scss';

const coenModule = angular.module('app', [
    // 'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'pascalprecht.translate',
    'ui.bootstrap',
    components.name,
    services.name,
    plugins.name,
    features.name

]).component('coenApp', {
    template: template
});

coenModule.constant('appConstants', {
    services: {
        endpoint: 'http://api.coen.scuro.nl/'
    },
    defaultTimeout: 7000
});

coenModule.config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', '$translateProvider', '$translatePartialLoaderProvider',
    ($routeProvider, $locationProvider, $sceDelegateProvider, $translateProvider, $translatePartialLoaderProvider) => {

        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'http://api.coen.scuro.nl/*'
        ]);

        $routeProvider
            .when('/home', {
                template: '<home-feature></home-feature>',
                name: 'home'
            })
            .when('/about', {
                template: '<about-feature></about-feature>',
                name: 'about'
            })
            .when('/info', {
                template: '<info-feature></info-feature>',
                name: 'info'
            })
            .when('/statement', {
                template: '<statement-feature></statement-feature>',
                name: 'statement'
            })
            .when('/statement/:statementId', {
                template: '<statement-feature></statement-feature>',
                name: 'statement'
            })
            .when('/result', {
                template: '<result-feature></result-feature>',
                name: 'result'
            })
            .when('/overview', {
                template: '<overview-feature></overview-feature>',
                name: 'overview'
            })
            .when('/profile', {
                template: '<profile-feature></profile-feature>',
                name: 'profile'
            })
            .when('/social', {
                template: '<social-feature></social-feature>',
                name: 'social'
            })
            .when('/end', {
                template: '<end-feature></end-feature>',
                name: 'end'
            })
            .otherwise({
                redirectTo: '/home'
            });

        $locationProvider.html5Mode(false);

        $translateProvider.useSanitizeValueStrategy('escaped');
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: 'locale/{lang}/{part}.json'
        });

        let clientUrl = Config.client;
        $translateProvider.preferredLanguage("nl");
        $translatePartialLoaderProvider.addPart(clientUrl);
    }
]);

export default coenModule;

