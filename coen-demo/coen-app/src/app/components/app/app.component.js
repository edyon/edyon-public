import angular from 'angular';

import template from './app.template.html';
import controller from './app.controller';

var appModule = angular.module('coen.components.app', []);

appModule.component('coenApplication', {
    template: template,
    controller: controller
});

export default appModule;
