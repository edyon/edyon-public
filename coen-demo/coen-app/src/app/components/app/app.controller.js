export default class appController {

    constructor($scope, $rootScope, $route, $window) {
        this.$rootScope = $rootScope;
        this.$route = $route;
        this.$window = $window;

        this.menu = null;
        this.routeClass = "home";

        $rootScope.$on('$routeChangeStart', (evt, absNewUrl, absOldUrl) => {
            this.$window.scrollTo(0, 0);
        });

        $rootScope.$on('$routeChangeSuccess', () => {
            if (this.$route.current && this.$route.current.$$route) {
                let routeParam = this.$route.current.$$route.name;
                this.routeClass = routeParam;
                if (this.$route.current.params && this.$route.current.params.statementId) {
                    routeParam = routeParam + "/" + this.$route.current.params.statementId;
                }

                this.$rootScope.$broadcast('navigate', routeParam);
            }
        });

        $scope.$on('menu:select', (event, data) => {
            this.menu = data.menu;
        });

    }
}