import angular from 'angular';

import template from './result-bar.template.html';
import style from './result-bar.scss';

class ResultBarController {

    constructor($scope) {
        $scope.$watch('$ctrl.results', () => {
// console.log("resultbar: this.results ", this.results);
        });
    }
}

var resultBarModule = angular.module('coen.components.result.bar', []);

resultBarModule.component('resultBars', {
    template: template,
    controller: ResultBarController,
    bindings: {
        results: "="
    }
});

export default resultBarModule;
