export default class ThemeController {

    constructor($scope, RestService) {
        this.$scope = $scope;
        this.RestService = RestService;

        this.checked = false;
        this.getTheme();

        $scope.$watch('checked', () => {
            if (!this.themeRating.ratingValue === 3) {
                this.themeRating.ratingValue = 2;
            }
        });

        $scope.$watch('favourite', () => {
            if (this.favourite == this.themeRating.id) {
                this.themeRating.ratingValue = 3;
            }
        });
    }

    getTheme() {
        this.RestService.getTheme(this.themeRating.themeId).then((theme) => {
            this.theme = theme;
            this.checked = this.themeRating.ratingValue >= 2;
        });
    }
}