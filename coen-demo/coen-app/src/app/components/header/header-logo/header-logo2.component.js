import angular from 'angular';

import template from './header-logo2.template.html';

var headerLogoModule = angular.module('coen.components.header.logo2', []);

class HeaderLogoController {

    constructor($scope, $location) {
        this.$location = $location;

        $scope.$on('menu:select', (event, args) => {
            this.menu = args.menu;
            this.showLogo = (this.menu.title == "home");
        });
    }

}

headerLogoModule.component('headerLogo2', {
    template: template,
    controller: HeaderLogoController
});

export default headerLogoModule;