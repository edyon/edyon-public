export default class bulletbarController {

    constructor($scope, $rootScope, $element, $route, $window, $timeout, DataService, MenuService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$element = $element;
        this.$route = $route;
        this.$window = $window;
        this.$timeout = $timeout;

        this.DataService = DataService;
        this.MenuService = MenuService;

        $($window).resize(() => {
            if (this.steps) {
                this.setBulletBarWidth(this.steps.length);
                this.setBulletBarLeft(this.bulletIndex, this.steps.length);
            }
        });

        if (!this.survey) this.setSteps(this.survey);


        this.bulletIndex = 0;
        this.survey = DataService.getSurvey();

        $scope.$on('survey', (event, args) => {
            this.setSteps(args.survey);
        });

        // handle navigating between routes
        $scope.$on('navigate', (event, args) => {
//console.log('------------------ navigate '+ args);
            this.setNextStepStyle(args);
        });

        $scope.$on('menu:select', (event, args) => {
            this.menu = args.menu;
        });

        // handle navigate from statement
        $scope.$on('next-statement', (event, args) => {
            let answer = args.answer;
            this.setNextStepStyle('statement/' + answer.questionId);
        });
    }

    setSteps (survey) {
        this.survey = survey;
        var answers = this.survey ? this.survey.answers : [];
        this.steps = this.MenuService.getMenuWithSteps(answers);
// console.log("bullet bar steps = ", this.steps);
        this.startFromRoute();
    };

    // In case the app is reloaded while the current page is /statement,
    // find the first correct == undefined and use that index to mark the current step
    startFromRoute () {
        var routeParam = this.$route.current ? this.$route.current.$$route.name : "";
        if (routeParam == "statement") {
            var q = this.DataService.getSurvey();
            if (q && q.answers) {
                for (var i = 0; i < q.answers.length; i++) {
                    if (q.answers[i].correct == undefined) {
                        this.currentIndex = i;
                        this.setNextStepStyle('statement/' + q.answers[i].questionId);
                        break;
                    }
                }
            }
        }
        // otherwise use the current route to mark the current step
        else {
            this.setNextStepStyle(routeParam);
        }
    }

    // This function actually activates a bullet for the provided title
    setNextStepStyle (url) {
        if (!url || !this.steps)
            return;

        _.each(this.steps, (step) => {
            step.active = (step.url === url);
            if (step.active) {
                this.bulletIndex = step.index + 1;
            }
        });

// console.log('---- BULLET display step: ', url, " bullet index = ", this.bulletIndex);
        this.renderBulletBar();
    }

    renderBulletBar () {
        this.$timeout(() => {
            this.setBulletBarWidth(this.steps.length);
            this.setBulletBarLeft(this.bulletIndex, this.steps.length);
        }, 1000);
    }

    setBulletBarWidth (numBullets) {
        var totalWidth = $(document).width();
        var maxWidth = totalWidth * 1.83;
        if (totalWidth < 768) {
            totalWidth = maxWidth;
        }
        this.totalWidth = totalWidth;
        $(".bullet-bread-bar").css('width', totalWidth);

        totalWidth = totalWidth - 11;

        var stepWidth = totalWidth * (1 / numBullets);
        this.stepWidth = stepWidth;
        $(".bullet-step").css('width', stepWidth);
        $(".bullet-bar-line").css('width', stepWidth);
//console.log("bullet bar *** set width = ", stepWidth);
    }

    setBulletBarLeft (bulletIndex, numBullets) {
        var totalWidth = $(document).width();
        var maxWidth = totalWidth * 1.83;
        this.totalWidth = maxWidth;
        if (this.totalWidth < 768) {
            this.barLeft = (bulletIndex > (numBullets / 2)) ? (totalWidth - maxWidth) : 0;
            $(".bullet-bread-bar").css('margin-left', this.barLeft);

            this.moreLeft = (bulletIndex > (numBullets / 2)) ? 10 : (totalWidth - 34);
            $(".icon-broken-line").css('left', this.moreLeft);
//console.log("set left = ", this.barLeft, this.moreLeft, bulletIndex, numBullets);
        } else {
            $(".bullet-bread-bar").css('margin-left', 0);
        }
    }


}
