import angular from 'angular';

import template from './burger.template.html';
import controller from './burger.controller';
import style from './_burger.scss';

var burgerModule = angular.module('coen.components.burger', []);

burgerModule.component('burgerMenu', {
    template: template,
    controller: controller
});

export default burgerModule;
