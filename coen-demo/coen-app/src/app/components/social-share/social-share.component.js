import angular from 'angular';

import template from './social-share.template.html';
import controller from './social-share.controller';
import style from './_social-share.scss';

var socialModule = angular.module('coen.components.social', []);

socialModule.component('socialShare', {
    template: template,
    controller: controller
});

export default socialModule;
