export default class SocialShareController {

    constructor($scope, $rootScope, $location, $route, DataService, FacebookService, TwitterService, EmailService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;

        this.DataService = DataService;
        this.FacebookService = FacebookService;
        this.TwitterService = TwitterService;
        this.EmailService = EmailService;

        this.socialTitles = [
            {url: "statement", title: "statement"},
            {url: "overview", title: "overview"},
            {url: "result", title: "result"},
            {url: "end", title: "end"}
        ];

        DataService.checkUser().then(user => {
            this.user = user;

            this.currentAnswer = DataService.getAnswer();
            this.currentProfile = DataService.getProfile();

            let routeParam = $route.current.$$route.name;
            this.currentMenu = routeParam;
        });

        // handle navigating between routes
        $scope.$on('menu:select', (event, args) => {
            this.currentMenu = args.menu.url;
        });

        $scope.$on('next-statement', (event, args) => {
            this.currentAnswer = args.answer;
        });

        $scope.$on('profile', (event, args) => {
            this.currentProfile = args.profile;
        });
    }

    /**
     * facebook
     */
    shareFacebook() {
        //loginFacebook();

        var socialPost = {};
        if (this.currentMenu === "statement") {
            socialPost = this.FacebookService.getAnswerPost(this.user, this.currentAnswer);
        } else if (this.currentMenu === "result" || this.currentMenu === "social") {
            socialPost = this.FacebookService.getProfilePost(this.user, this.currentProfile);
        }

        var message = this.FacebookService.getPostLight(socialPost);
        this.FacebookService.publishPostLight(message);
    };

    loginFacebook() {
        this.FacebookService.checkLoginStatus().then((response) => {
            if (response) {
                this.publishToFacebook(response);
            } else {
                this.FacebookService.goLogin().then(this.publishToFacebook);
            }
        });
    };

    publishToFacebook(user) {
        var socialPost = this.FacebookService.getPost(user, this.currentMenu, this.currentAnswer);
        this.$rootScope.$broadcast('toggle:social-post', {post: socialPost});
    };

    /**
     * twitter
     */
    shareTwitter() {
        //loginTwitter();

        var socialPost = {};
        if (this.currentMenu === "statement") {
            socialPost = this.TwitterService.getAnswerPost(this.user, this.currentAnswer);
        } else if (this.currentMenu === "result" || this.currentMenu === "social") {
            socialPost = this.TwitterService.getProfilePost(this.user, this.currentProfile);
        }

        var tweet = this.TwitterService.getPostLight(socialPost);
        this.TwitterService.publishPostLight(tweet);
    };

    loginTwitter() {
        this.TwitterService.connectTwitter().then((response) => {
            if (this.TwitterService.isReady()) {
                this.publishToTwitter(response);
            }
        });
    };

    publishToTwitter(user) {
        var socialPost = this.TwitterService.getPost(user, this.currentMenu, this.currentAnswer);
        this.$rootScope.$broadcast('toggle:social-post', {post: socialPost});
    };

    /**
     * email
     */
    shareEmail() {
        //shareEmail();
        this.publishEmailLight();
    };

    publishEmail() {
        var socialPost = this.EmailService.getPost(null, this.currentMenu, this.currentAnswer);
        this.$rootScope.$broadcast('toggle:social-post', {post: socialPost});
    };

    publishEmailLight() {
        var socialPost = {};
        if (this.currentMenu === "statement") {
            socialPost = this.EmailService.getAnswerPost(this.user, this.currentAnswer);
        } else if (this.currentMenu === "result" || this.currentMenu === "social") {
            socialPost = this.EmailService.getProfilePost(this.user, this.currentProfile);
        }

        var email = this.EmailService.getEmailLight(socialPost);
        this.EmailService.sendEmailLight(email);
    };


    getTitleForMenu() {
        var shareTitle = _.find(this.socialTitles, (socialItem) => {
            return socialItem.title === this.currentMenu;
        });
        return shareTitle ? shareTitle.title : "";
    };
}