import angular from 'angular';

import app from './app/app.component';
import footer from './footer/footer.component';
import header from './header/header.component';
import resultBar from './result-bar/result-bar.component';
import score from './score/score.component';
import socialShare from './social-share/social-share.component';
import socialPost from './social-post/social-post.component';
import statement from './statement-view/statement-view.component';
import text from './text/text.component';
import theme from './theme/theme.component';
import thumb from './thumb/thumb.component';
import toggleInfo from './toggle-info/toggle-info.component';
import toggleQuestion from './toggle-question/toggle-question.component';
import voters from './voters/voters.component';

const ComponentsModule = angular.module('coen.components', [
    app.name,
    footer.name,
    header.name,
    resultBar.name,
    score.name,
    socialShare.name,
    socialPost.name,
    statement.name,
    text.name,
    theme.name,
    thumb.name,
    toggleInfo.name,
    toggleQuestion.name,
    voters.name
]);

export default ComponentsModule;
