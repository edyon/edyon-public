export default class TextFormatController {

    constructor($scope, $sce, $filter) {
        this.$scope = $scope;
        this.$sce = $sce;
        this.$filter = $filter;

        $scope.$watch('$ctrl.text', (newValue) => {
            this.formatText(newValue);
        });
    }

    formatText (text) {
        if (text) {
            // Display "< & >" in text.
            // text = text.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/&/g, '&amp;');
            // this.formatText = text.replace(/\n/g, "<br/>");
            //
            // var urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/ig;
            //
            // if (text.search(urlRegex) > -1) {
            //     text = text.replace(urlRegex, (url) => {
            //         var target = "_blank";
            //         if (url.search('www') > -1) {
            //             var refTitle = "";
            //             var refLabel = url;
            //             if (url.search('http://') > -1) {
            //                 return '<a title="' + refTitle + '" href="' + url + '" target="' + target + '">' + refLabel + '</a>';
            //             } else {
            //                 return '<a title="' + refTitle + '" href="http://' + url + '" target="' + target + '">' + refLabel + '</a>';
            //             }
            //         }
            //     });
            // }

            this.parsedText = this.$sce.trustAsHtml(text);
        }
// console.log("parsedText = ", this.parsedText);
    }
}