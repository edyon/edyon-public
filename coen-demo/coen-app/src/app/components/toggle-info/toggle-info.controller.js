export default class InfoController {

    constructor($scope, $rootScope, ConfigService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.ConfigService = ConfigService;

        this.showInfo = false;
        this.showCorrectInfo = ConfigService.checkShowCorrectInfo();

        $scope.$on('toggle:popup', (event, args) => {
            this.togglePopup(args);
        });

        $scope.$watch('$ctrl.info', () => {
            this.checkShowInfo();
        });
    }

    checkShowInfo () {
        if (this.info && this.info.description) {
            this.showInfo = true;
        } else {
            this.showInfo = false;
        }
    }

    togglePopup (args) {
        if (args) {
            if (args.type == this.type) {
                this.info = args.info;
                this.showInfo = true;
                return;
            }
        }

        this.info = {};
        this.showInfo = false;
    }

    close() {
        this.showInfo = false;
        this.$rootScope.$broadcast("close-info-popup");
    }
}