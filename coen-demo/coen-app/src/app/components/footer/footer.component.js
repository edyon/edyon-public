import angular from 'angular';

import template from './footer.template.html';
import style from './_footer.scss';

import navigateComponent from './navigate/navigate.component';

var footerModule = angular.module('coen.components.footer', [
    navigateComponent.name
]);

footerModule.component('footerComponent', {
    template: template
});

export default footerModule;
