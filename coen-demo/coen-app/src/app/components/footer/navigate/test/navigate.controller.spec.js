'use strict';

import NavigateController from '../navigate.controller';

describe('Navigate controller', () => {
    var $q,
        $scope,
        $rootScope,
        $compile,
        $location,
        $routeParams,
        $route,
        controller,
        queryUserDeferred,
        querySurveyDeferred,
        $httpBackend;

    window.Config = {
        client: "coen",
        apiUrl: "http://api.coen.scuro.nl"
    };


    var mockTranslations = readJSON('locale/nl/coen.json');

    var mockUser = readJSON('mocks/coen/user.json');
    var mockSurvey = readJSON('mocks/coen/survey.json');

    var mockMenuService, mockAnswerService;
    var mockDataService = {
        checkUser: () => {
            queryUserDeferred = $q.defer();
            return queryUserDeferred.promise;
        },
        checkSurvey: () => {
            querySurveyDeferred = $q.defer();
            return querySurveyDeferred.promise;
        },
        getSurvey: () => {
            return mockSurvey;
        }
    };

    window.Config = {
        menu: ["home", "about", "info", "statement", "profile", "overview", "social"]
    }

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject((_$q_, _$compile_, _$rootScope_, _$httpBackend_, $controller, _$location_, _$routeParams_, _$route_, $injector) => {
        $q = _$q_;
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
        $location = _$location_;
        $routeParams = _$routeParams_;
        $route = _$route_;

        $httpBackend.when('GET', 'locale/nl/coen.json').respond(mockTranslations);
        $httpBackend.when('GET', '/dist/locale/nl/coen.json').respond(mockTranslations);

        spyOn(mockDataService, 'checkUser').and.callThrough();
        spyOn(mockDataService, 'checkSurvey').and.callThrough();

        mockMenuService = $injector.get('MenuService');
        mockAnswerService = $injector.get('AnswerService');

        controller = $controller(NavigateController, {
            $scope: $scope,
            $rootScope: $rootScope,
            $route: $route,
            $routeParams: $routeParams,
            $location: $location,
            DataService: mockDataService,
            MenuService: mockMenuService,
            AnswerService: mockAnswerService
        });
    }));

    describe('should set the steps when survey is ready', () => {

        describe('when checkSurvey is called', () => {
            beforeEach(() => {

            });

            it('should set the survey', () => {
                expect(controller.survey).toEqual(mockSurvey);
            });
        });

        describe('when setSteps is called', () => {
            beforeEach(() => {
                spyOn(controller, 'startFromRoute').and.callThrough();

                controller.setSteps(mockSurvey);
            });

            it('should set the survey', () => {
                expect(controller.survey).toEqual(mockSurvey);
                expect(controller.steps.length).toBe(14);
                expect(controller.startFromRoute).toHaveBeenCalled();
            });
        });

        describe('when on the home page', () => {
            beforeEach(() => {
                controller.setSteps(mockSurvey);
                controller.setRoute('home');
            });

            it('should set the index to 0', () => {
                expect(controller.menu.title).toBe('home');
                expect(controller.index).toBe(0);
            });
        });

        describe('when on the about page', () => {
            beforeEach(() => {
                controller.setSteps(mockSurvey);
                controller.setRoute('about');
            });

            it('should set the index to 1', () => {
                expect(controller.menu.title).toBe('about');
                expect(controller.index).toBe(1);
            });
        });

    });

    describe('should navigate on the about page', () => {
        beforeEach(() => {
            controller.setSteps(mockSurvey);
            controller.setRoute('about');
        });

        describe('when selecting previous menu', () => {
            beforeEach(() => {

            });

            it('should set the index to 0', () => {
                controller.selectPreviousMenu();
                expect(controller.index).toBe(0);
            });
        });

        describe('when selecting next menu', () => {
            beforeEach(() => {
                controller.isValid = true;
            });

            it('should set the index to 2', () => {
                controller.selectNextMenu();
                expect(controller.index).toBe(2);
            });
        });
    });

    describe('should navigate on the last page', () => {
        beforeEach(() => {
            controller.setSteps(mockSurvey);
            controller.setRoute('end');
        });

        describe('when selecting previous menu', () => {
            beforeEach(() => {

            });

            it('should set the index to 11', () => {
                controller.selectPreviousMenu();
                expect(controller.index).toBe(12);
            });
        });

        describe('when selecting next menu', () => {
            beforeEach(() => {
                controller.isValid = true;
            });

            it('should set the index to 0', () => {
                controller.selectNextMenu();
                expect(controller.index).toBe(0);
            });
        });
    });

    describe('should go to the previous statement', () => {
        beforeEach(() => {
            spyOn(controller, 'setMenu').and.callThrough();
            spyOn(controller, 'navigatePath').and.callThrough();
        });

        it('should go to previous', () => {
            controller.steps = [{title: "home", url: "test"}, {title: "about", url: "test"}];
            controller.index = 1;
            controller.selectPreviousMenu();
            expect(controller.index).toBe(0);
            expect(controller.setMenu).toHaveBeenCalledWith(0);
        });

        it('should not go to previous whne first item', () => {
            controller.steps = [{title: "home", url: "test"}, {title: "about", url: "test"}];
            controller.index = 0;
            controller.selectPreviousMenu();
            expect(controller.index).toBe(0);
            expect(controller.setMenu).toHaveBeenCalledWith(0);
        });
    });
});
