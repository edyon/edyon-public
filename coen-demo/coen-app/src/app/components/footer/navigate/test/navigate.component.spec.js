'use strict';

describe('Navigate component', function () {
	var element,
		$scope,
		$rootScope,
		$compile,
        $httpBackend;

	window.Config = {
		client: "coen",
		apiUrl: "http://api.coen.scuro.nl"
	};

    var mockTranslations = readJSON('locale/nl/coen.json');

    beforeEach(angular.mock.module('app'));
 
	beforeEach(angular.mock.inject(function (_$compile_, _$rootScope_, _$httpBackend_) {
		$scope = _$rootScope_.$new();
		$rootScope = _$rootScope_;
		$compile = _$compile_;
        $httpBackend = _$httpBackend_;

        $httpBackend.when('GET', 'locale/nl/coen.json').respond(mockTranslations);

        var el = angular.element(`<footer-navigate menu="home"></footer-navigate>`);
		element = $compile(el)($scope); // Compile the directive
		$scope.$digest(); // Update the HTML
	}));

	afterEach(function () {
		$scope.$destroy();
	});

	it('should have the directive when empty', function () {
		expect(element.length).not.toBe(0);
	});

});