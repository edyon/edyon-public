export default class navigateController {

    constructor($scope, $rootScope, $location, $route, $timeout, DataService, MenuService, AnswerService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$route = $route;
        this.$timeout = $timeout;

        this.DataService = DataService;
        this.MenuService = MenuService;
        this.AnswerService = AnswerService;

        this.survey = DataService.getSurvey();

        if (!this.survey) {
            this.setSteps(this.survey);
        }

        $scope.$watch('$ctrl.menu', () => {
            // update menu
        });

        $scope.$on('survey', (event, args) => {
            this.setSteps(args.survey);
        });

        $scope.$on('navigate', (event, args) => {
// console.log("on navigate ", args);
            this.setRoute(args);
        });

        /**
         * handle saving person
         */
        $rootScope.$on('person:validate', (event, data) => {
            this.isValid = data.valid;
        });

        $rootScope.$on('person:complete', (event, data) => {
            this.navigateToNextMenu();
        });

        /**
         * handle saving question
         */
        $rootScope.$on('question:answered', (event, data) => {
            if (this.menu) {
                this.menu.showNext = true;
                this.menu.showVotes = false;
            }
            this.isValid = true;
        });

        $rootScope.$on('question:complete', (event, data) => {
            this.navigateToNextMenu();
        });
    }

    setSteps(survey) {
        this.survey = survey;
        var answers = this.survey ? this.survey.answers : [];
        this.steps = this.MenuService.getMenuWithSteps(answers);
        this.startFromRoute();
    }

    startFromRoute() {
        if (this.$route.current) {
            var routeParam = this.$route.current.$$route.name;
            if (this.$route.current.params && this.$route.current.params.statementId) {
                routeParam = routeParam + "/" + this.$route.current.params.statementId;
            }
            this.setRoute(routeParam);
        }
    }

    setRoute(routeParam) {
        var menu = _.find(this.steps, (item) => {
            return item.url === routeParam;
        });
        this.index = _.indexOf(this.steps, menu);
        this.setMenu(this.index);
    }

    /**
     * set the selected menu
     *
     * @param index
     */
    setMenu(index) {
        this.prevMenu = index - 1 > -1 ? this.steps[index - 1] : null;
        this.menu = this.steps[index];
        this.nextMenu = index + 1 < this.steps.length ? this.steps[index + 1] : this.steps[0];
// console.log("navigate menu: set current menu = ", this.menu, " index = ", index);

        if (this.menu) {
            if (this.menu.title === 'info' || this.menu.title === 'statement') {
                this.isValid = false;
            } else {
                this.isValid = true;
            }
            this.$rootScope.$broadcast('menu:select', {menu: this.menu});
        }
    }

    navigatePath(url) {
        this.$timeout(() => {
            this.$location.path(url);
        }, 100);
    }

    selectPreviousMenu() {
        this.navigateToPreviousMenu();
    }

    navigateToPreviousMenu() {
        if (this.index >= 0) {
            this.index--;
        }

        this.setMenu(this.index);
        this.navigatePath(this.menu.url);
    };

    /**
     * handle selecting the next button
     */
    selectNextMenu() {
        if (this.isValid) {
            if (this.menu.title == "info") {
                this.$rootScope.$broadcast('save-person');
                return;
            } else {
                if (this.menu.title == "statement") {
                    this.$rootScope.$broadcast('question:submit');
                    return;
                }
            }

            this.navigateToNextMenu();
        }
    }

    navigateToNextMenu() {
        if (this.index < (this.steps.length - 1)) {
            this.index++;
        } else {
            this.index = 0;
        }
        this.setMenu(this.index);
        this.navigatePath(this.menu.url);
    }

    rateStatement(agree) {
        var routeParam = this.$route.current.$$route.name;
        if (routeParam == "statement") {
            if (agree === true) {
                this.AnswerService.saveAnswer(0, false, false);
            } else {
                this.AnswerService.saveAnswer(1, false, false);
            }
        }
    };

}