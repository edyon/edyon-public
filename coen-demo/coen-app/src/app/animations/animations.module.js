import angular from 'angular';

import slideAnimations from './slide-animations/slide.animation';

const AnimationsModule = angular.module('coen.animations', [
    slideAnimations.name
]);

export default AnimationsModule;
