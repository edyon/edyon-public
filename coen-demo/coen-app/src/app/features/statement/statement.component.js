import angular from 'angular';

import template from './statement.html';
import controller from './statement.controller';
import style from './_statement.scss';

var statementModule = angular.module('coen.features.statement', []);

statementModule.component('statementFeature', {
    template: template,
    controller: controller
});

export default statementModule;
