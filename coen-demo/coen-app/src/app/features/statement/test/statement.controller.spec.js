'use strict';

import StatementController from '../statement.controller';

describe('Statement controller', () => {
    var $q,
        $scope,
        $rootScope,
        $compile,
        $location,
        $routeParams,
        controller,
        queryUserDeferred,
        querySurveyDeferred,
        $httpBackend;

    var mockDataService = {
        checkUser: () => {
            queryUserDeferred = $q.defer();
            return queryUserDeferred.promise;
        },
        checkSurvey: () => {
            querySurveyDeferred = $q.defer();
            return querySurveyDeferred.promise;
        }
    };

    var mockMenuService = {
        getMenu: () => {
            return [];
        }
    };

    var mockAnswerService = {
        getAnswer: () => {
            return [];
        }
    };


    var mockTranslations = readJSON('locale/nl/coen.json');

    var mockUser = readJSON('mocks/coen/user.json');
    var mockSurvey = readJSON('mocks/coen/survey.json');

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject((_$q_, _$compile_, _$rootScope_, _$httpBackend_, $controller, _$location_, _$routeParams_) => {
        $q = _$q_;
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;
        $location = _$location_;
        $routeParams = _$routeParams_;

        $httpBackend.when('GET', 'locale/nl/coen.json').respond(mockTranslations);
        $httpBackend.when('GET', '/dist/locale/nl/coen.json').respond(mockTranslations);

        spyOn(mockDataService, 'checkUser').and.callThrough();
        spyOn(mockDataService, 'checkSurvey').and.callThrough();

        controller = $controller(StatementController, {
            $scope: $scope,
            $rootScope: $rootScope,
            $routeParams: $routeParams,
            $location: $location,
            DataService: mockDataService,
            MenuService: mockMenuService,
            AnswerService: mockAnswerService
        });
    }));

    describe('should get the user and survey', () => {

        xdescribe('when checkUser is called', () => {
            beforeEach(() => {
                queryUserDeferred.resolve(mockUser);
                $rootScope.$apply();
            });

            it('should defining the checkUser method', () => {
                expect(mockDataService.checkUser).toBeDefined();
            });

            it('should invoke the get method', () => {
                expect(mockDataService.checkUser).toHaveBeenCalled();
            });

            it('should set the user', () => {
                expect(controller.user).toEqual(mockUser);
            });
        });

        xdescribe('when checkSurvey is called', () => {
            beforeEach(() => {
                spyOn(controller, 'setAnswerFromRoute').and.callThrough();

                queryUserDeferred.resolve(mockUser);
                $rootScope.$apply();

                querySurveyDeferred.resolve(mockSurvey);
                $rootScope.$apply();
            });

            it('should defining the checkSurvey method', () => {
                expect(mockDataService.checkSurvey).toBeDefined();
            });

            it('should invoke the get method', () => {
                expect(mockDataService.checkSurvey).toHaveBeenCalled();
            });

            it('should set the survey', () => {
                expect(controller.survey).toEqual(mockSurvey);
                expect(controller.setAnswerFromRoute).toHaveBeenCalled();
            });
        });

    });

});
