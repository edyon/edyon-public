'use strict';

describe('Statement component', function () {
    var scope, element, compile, $httpBackend;

    var mockUser = readJSON('mocks/coen/user.json');
    var mockSurvey = readJSON('mocks/coen/survey.json');
    var mockTranslations = readJSON('locale/nl/coen.json');

    window.Config = {
        client: "coen",
        apiUrl: "http://api.coen.scuro.nl"
    };

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject(function ($compile, $rootScope, _$httpBackend_) {
        compile = $compile;
        scope = $rootScope.$new();
        $httpBackend = _$httpBackend_;

        $httpBackend.when('GET', 'locale/nl/coen.json').respond(mockTranslations);
        $httpBackend.when('GET', 'http://api.coen.scuro.nl/person').respond(mockUser);

        // Create an instance of the directive
        element = angular.element('<statement-feature></statement-feature>');
        compile(element)(scope); // Compile the directive
        scope.$digest(); // Update the HTML
    }));

    afterEach(function () {
        scope.$destroy();
    });

    it('should compile to page', function () {
        expect(element.length).not.toBe(0);
    });

});