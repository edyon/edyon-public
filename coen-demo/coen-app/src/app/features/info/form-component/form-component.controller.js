export default class FormController {

    constructor($scope, $rootScope, $timeout, RestService, HighchartService, MenuService, DataService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$timeout = $timeout;
    }

    /**
     * watch if the form is valid
     */
    updateValidation () {
        this.isValid = this.$scope.userForm ? this.$scope.userForm.$valid : false;
        this.$rootScope.$broadcast('person:validate', {valid: this.isValid});
    };

    selectGender (gender) {
        this.user.gender = gender;

        this.$timeout(() => {
            this.isValid = this.$scope.userForm.$valid;
            this.updateValidation();
        });
    };
}
