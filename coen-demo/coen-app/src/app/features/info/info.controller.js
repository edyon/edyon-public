export default class InfoController {

    constructor($scope, $rootScope, $location, RestService, MenuService, ConfigService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$location = $location;

        this.RestService = RestService;

        this.newUser = {};
        this.user = this.newUser;
        this.resetUser = !ConfigService.checkShowCountyForm();
        this.questions = MenuService.getQuestionsByMenu("info");
        this.question1 = this.questions[0];

        this.client = Config.client;
        this.getUser();

        /**
         * save person when pressing navigation next button
         */
        $scope.$on("save-person", () => {
            this.saveUser();
        });
    }

    getUser() {
        this.RestService.getLoggedUser().then((user) => {
            var success = user && user != "null";
            if (success) {
                this.user = this.resetUser ? this.newUser : user; // don't reset the user for Hardenberg
            } else {
                this.user = this.newUser;
            }
        }).catch((error) => {
            this.user = this.newUser;
        });
    };

    saveUser() {
        if (!this.user.id) {
            this.RestService.loginPerson(this.user).then((user) => { // create a new user
                this.user = user;
                this.$rootScope.$broadcast('person:complete');
            });
        } else {
            this.$rootScope.$broadcast('person:complete');
        }
    }


}
