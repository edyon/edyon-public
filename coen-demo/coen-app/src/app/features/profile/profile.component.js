import angular from 'angular';

import template from './profile.html';
import controller from './profile.controller';

var profileModule = angular.module('coen.features.profile', []);

profileModule.component('profileFeature', {
    template: template,
    controller: controller
});

export default profileModule;
