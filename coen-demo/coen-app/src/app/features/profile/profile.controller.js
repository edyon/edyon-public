export default class ProfileController {

    constructor($scope, $filter, $sce, RestService, DataService, MenuService) {
        this.$filter = $filter;
        this.$sce = $sce;

        this.RestService = RestService;
        this.DataService = DataService;
        this.MenuService = MenuService;

        this.results = [];
        this.allResults = [];
        this.likedVotes = [];

        this.menus = MenuService.getMenu();
        this.menu = this.menus[this.menus.length - 1];

        this.getResults();
    }

    getResults() {
        this.RestService.getCompleteSurvey().then(survey => {
            this.survey = survey;

            if (this.survey) {
                this.setUserProfile();
            }
        }).catch(error => {
            // handle error
        });
    };

    setUserProfile() {
        let correctAnswers = _.filter(this.survey.answers, (answer) => {
            return answer.correct === true;
        });

        this.numQuestions = this.survey.answers.length;
        this.numCorrectAnswers = correctAnswers.length;
        this.profileScore = this.MenuService.getProfileScore(this.numCorrectAnswers, this.numQuestions);

        this.getAllResults();
    };

    getAllResults() {
        this.scores = [];

        this.RestService.getScore(0, 0).then((score) => {
            this.scores.push(score);
            this.userCount = Number(score);

            this.RestService.getScore(0, 4).then((score) => {
                this.scores.push(score);
                this.userCount += Number(score);

                this.RestService.getScore(5, 9).then((score) => {
                    this.scores.push(score);
                    this.userCount += Number(score);

                    this.RestService.getScore(10, 14).then((score) => {
                        this.scores.push(score);
                        this.userCount += Number(score);

                        this.RestService.getScore(15, 15).then((score) => {
                            this.scores.push(score);
                            this.userCount += Number(score);

                            this.setAllProfiles();
                        });
                    });
                });
            });
        });
    };

    setAllProfiles() {
        this.profile = this.MenuService.getProfileByScore(this.profileScore, this.profiles);
        this.MenuService.setShareText(this.profile, this.numQuestions, this.numCorrectAnswers);

        this.DataService.setProfile(this.profile);

        let profiles = this.MenuService.getProfiles();
        this.profiles = this.MenuService.getProfilesWithScore(profiles, this.userCount, this.scores);
    };

    trustAsHtml(html) {
        var translated = this.$filter('translate')(html);
        return this.$sce.trustAsHtml(translated);
    };
}
