export default class LoginController {

    constructor($scope, $window, checkUser, RestService) {
        this.$window = $window;

        this.user = checkUser;

        this.RestService = RestService;
    }

    loginFacebook () {
        this.RestService.getLoginWindow('facebook').then(function (data) {
            if (data && data.url) {
                var url = data.url;
                this.$window.open(url, "_self");
            }
        });
    }

    loginTwitter () {

    }

    loginInstagram () {

    }
}
