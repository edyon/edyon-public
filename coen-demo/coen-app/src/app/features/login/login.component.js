import angular from 'angular';

import template from './login.html';
import controller from './login.controller';
import style from './login.scss';

var loginModule = angular.module('coen.features.login', []);

loginModule.component('loginFeature', {
    template: template,
    controller: controller
});

export default loginModule;
