import angular from 'angular';

import template from './reminder.html';
import controller from './reminder.controller';

var reminderModule = angular.module('coen.features.reminder', []);

reminderModule.component('reminderFeature', {
    template: template,
    controller: controller
});

export default reminderModule;
