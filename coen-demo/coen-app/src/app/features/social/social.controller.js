export default class SocialController {

    constructor($scope, $filter, $sce, RestService, DataService, MenuService) {
        this.$scope = $scope;
        this.$filter = $filter;
        this.$sce = $sce;

        this.RestService = RestService;
        this.DataService = DataService;
        this.MenuService = MenuService;

        this.results = [];

        this.menus = MenuService.getMenu();
        this.menu = this.menus[this.menus.length - 1];

        this.getResults();
    }

    getResults() {
        this.RestService.getCompleteSurvey().then((survey) => {
            this.survey = survey;
            this.setUserProfile();
        });
    }

    setUserProfile() {
        if (this.survey.answers) {
            let correctAnswers = _.filter(this.survey.answers, (answer) => {
                return answer.correct === true;
            });

            this.numQuestions = this.survey.answers.length;
            this.numCorrectAnswers = correctAnswers.length;
            this.profileScore = this.MenuService.getProfileScore(this.numCorrectAnswers, this.numQuestions);
        }

        this.getAllResults();
    }

    getAllResults() {
        this.scores = [];

        this.RestService.getScore(0, 10).then((score) => {
            this.scores.push(score);
            this.userCount = Number(score);

            this.setAllProfileText();
        });
    }

    setAllProfileText() {
        this.profile = this.MenuService.getProfileByScore(this.profileScore, this.profiles);

        this.MenuService.setShareText(this.profile, this.numQuestions, this.numCorrectAnswers);
        this.DataService.setProfile(this.profile);
    }

    trustAsHtml(html) {
        var translated = this.$filter('translate')(html);
        return this.$sce.trustAsHtml(translated);
    }
}
