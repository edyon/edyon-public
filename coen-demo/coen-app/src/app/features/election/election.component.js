import angular from 'angular';

import template from './election.html';
import controller from './election.controller';
import style from './election.scss';

var electionModule = angular.module('coen.features.election', []);

electionModule.component('electionFeature', {
    template: template,
    controller: controller
});

export default electionModule;
