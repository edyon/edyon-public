import angular from 'angular';

import about from './about/about.component';
import charts from './charts/charts.component';
import election from './election/election.component';
import end from './end/end.component';
import home from './home/home.component';
import info from './info/info.component';
import login from './login/login.component';
import opinion from './opinion/opinion.component';
import overview from './overview/overview.component';
import profile from './profile/profile.component';
import reminder from './reminder/reminder.component';
import result from './result/result.component';
import social from './social/social.component';
import statement from './statement/statement.component';
import total from './total/total.component';
// import popup from './popup/popup.component';

const FeaturesModule = angular.module('coen.features', [
    about.name,
    charts.name,
    election.name,
    end.name,
    home.name,
    info.name,
    login.name,
    opinion.name,
    overview.name,
    profile.name,
    reminder.name,
    result.name,
    social.name,
    statement.name,
    total.name,
]);

export default FeaturesModule;
