import angular from 'angular';

import template from './end.html';
import controller from './end.controller';
import style from './_end.scss';

var endModule = angular.module('coen.features.end', []);

endModule.component('endFeature', {
    template: template,
    controller: controller
});

export default endModule;
