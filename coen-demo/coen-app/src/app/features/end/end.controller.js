export default class EndController {

    constructor($filter, $sce, $window, DataService) {
        this.$filter = $filter;
        this.$sce = $sce;
        this.$window = $window;

        DataService.checkUser().then(user => {
            this.user = user;
        });
    }

    closeApp() {
        this.$window.close();
    }

    trustAsHtml(html) {
        var translated = this.$filter('translate')(html);
        return this.$sce.trustAsHtml(translated);
    };
}
