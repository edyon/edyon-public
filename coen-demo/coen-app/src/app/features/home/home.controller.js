export default class HomeController {

    constructor($scope, DataService) {
        this.user = null;

        DataService.setSurvey(null);
    }
}