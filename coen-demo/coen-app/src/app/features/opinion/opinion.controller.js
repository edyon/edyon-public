export default class OpinionController {

    constructor($scope, $rootScope, $location, checkUser, RestService, MenuService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;

        this.RestService = RestService;

        this.user = checkUser;

        this.questions = MenuService.getQuestionsByMenu("opinion");
        this.question1 = this.questions[0];

        this.updateValidation(false);

        $scope.$watch('$ctrl.opinionForm.$valid', (newVal) => {
//console.log("update user = ", $scope.user, newVal);
            this.updateValidation(newVal);
        });

        $scope.$on("save-opinion", () => {
            if (this.user.remarks != "") {
                RestService.savePerson(this.user).then((user) => {
//console.log("saved user", user);
                });
            }
        });
    }

    updateValidation (valid) {
        this.$rootScope.$broadcast('person:validate', {valid: valid}); // send event to parent scopes
    }
}
