import angular from 'angular';

import template from './charts.html';
import controller from './charts.controller';

var chartsModule = angular.module('coen.features.charts', []);

chartsModule.component('chartsFeature', {
    template: template,
    controller: controller
});

export default chartsModule;
