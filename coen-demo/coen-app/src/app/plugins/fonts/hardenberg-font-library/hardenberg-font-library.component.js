import angular from 'angular';

import template from './hardenberg-font-library.html';

var hardenbergFontModule = angular.module('coen.components.fonts.hardenberg', []);

hardenbergFontModule.component('hardenbergFontLibrary', {
    template: template
});

export default hardenbergFontModule;
