import angular from 'angular';

import template from './roc-font-library.html';

var rocFontModule = angular.module('coen.components.fonts.roc', []);

rocFontModule.component('rocFontLibrary', {
    template: template
});

export default rocFontModule;
