import angular from 'angular';

import controller from './highchart.controller';

var highchartModule = angular.module('coen.components.highchart', []);

highchartModule.component('highchart', {
    template: '<div></div>',
    controller: controller,
    bindings: {
        config: '=',
        barType: '='
    },
});

export default highchartModule;
