import angular from 'angular';

import controller from './scroll.controller';
import style from './scroll.scss';

var scrollModule = angular.module('coen.components.plugins.scroll', []);

scrollModule.component('customScroll', {
    controller: controller
});

export default scrollModule;