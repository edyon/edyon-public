class ScrollController {

    constructor($scope, $element) {
        this.$scope = $scope;
        this.$element = $element;

        this.scrollPosition = 0;
        this.initScroll();
    }

    initScroll() {
        $(this.$element).mCustomScrollbar({
//			scrollInertia:0, // put on to stop animation
            verticalScroll: true,
            mouseWheel: true,
            scrollButtons: {
                enable: true,
                scrollSpeed: 80
            },
            autoHideScrollbar: false,
            scrollInertia: 0,
            //				contentTouchScroll: true,
            theme: "fs",
            advanced: {
                updateOnContentResize: true,
                autoExpandHorizontalScroll: true
            },
            callbacks: {
                onScrollStart: (scroll) => {
                    // handle scroll start
                },
                onScroll: (scroll) => {

                },
                whileScrolling: (scroll) => {
                    // this.updateScrollPosition(-scroll.left);
                }
            }
        });
    }

    updateScrollPosition(pos) {
        this.scrollPosition = pos;
    }

    updateScroll() {
        $(this.$element).mCustomScrollbar("update");
    };

    scrollToDiv(divId) {
        $(this.$element).mCustomScrollbar("scrollTo", divId);
    };

    scrollToPosition(pos) {
        $(this.$element).mCustomScrollbar("scrollTo", pos);
    };
}