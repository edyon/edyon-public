import angular from 'angular';

var repeatReadyModule = angular.module('coen.components.plugins.repeat.ready', []);

class RepeatReadyController {

    constructor($scope, $element) {
        if ($scope.$last === true) {
            $element.ready(() => {
                $scope.$emit('ngRepeatReady'); // send event to parent scopes
            });
        }
    }
}

repeatReadyModule.component('repeatReady', {
    controller: RepeatReadyController
});

export default repeatReadyModule;