import angular from 'angular';

import fonts from './fonts/fonts.component';
import highchart from './highchart/highchart.component';
import iconFont from './icon-font/icon-font.component';
import iframe from './iframe/iframe.component';
import jTinder from './jtinder/jtinder.component';
import repeatReady from './repeat-ready/repeat-ready.component';
import scroll from './scroll/scroll.component';

const PluginsModule = angular.module('coen.components.plugins', [
    fonts.name,
    highchart.name,
    iconFont.name,
    iframe.name,
    jTinder.name,
    repeatReady.name,
    scroll.name
]);

export default PluginsModule;
