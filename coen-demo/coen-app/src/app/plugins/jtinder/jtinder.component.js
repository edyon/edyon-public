import angular from 'angular';

import controller from './jtinder.controller';
import tinderPlugin from './coen.jquery.jTinder';
import style from './jtinder.scss';

var jTinderModule = angular.module('coen.components.plugins.jtinder', []);

jTinderModule.component('tinderAnimation', {
    controller: controller,
    bindings: {
        animationSpeed: "@?",
        animationRevertSpeed: "@",
        threshold: "@",
        menu: "@"
    }
});

export default jTinderModule;