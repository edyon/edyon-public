import angular from 'angular';

import template from './score.html';
import controller from './score.controller';

var scoreModule = angular.module('cms.features.score', []);

scoreModule.component('scoreFeature', {
    template: template,
    controller: controller
});

export default scoreModule;
