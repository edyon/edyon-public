import angular from 'angular';

import template from './person.html';
import controller from './person.controller';

var personModule = angular.module('cms.features.person', []);

personModule.component('personFeature', {
    template: template,
    controller: controller
});

export default personModule;
