export default class PersonController {

    constructor($scope, $location, $routeParams, RestService, ConfigService) {
        this.$location = $location;

        this.RestService = RestService;
        this.ConfigService = ConfigService;

        this.showAge = ConfigService.checkShowAgeResults();
        this.showCode = ConfigService.checkShowCodeResults();

        this.campaignId = $routeParams.campaignId;
        this.personId = $routeParams.personId;

        this.getAllResults();

        $scope.$on('navigate:back', (event, args) => {
            this.navigateBack(args);
        });
    }

    navigateBack(args) {
        var route = args.route;
        if (route == 'person') {
            if (this.person) {
                this.result = null;
                this.person = null;
                this.personId = 0;
                this.$location.path("campaign/" + this.campaignId + "/person");
            } else {
                this.$location.path("campaign/" + this.campaignId + "/menu/campaign");
            }
        }
    }

    getAllResults() {
        if (!this.persons) {
            this.RestService.getPersons().then((persons) => {
                this.persons = _.filter(persons, (item) => {
                    return item !== null && item !== "null";
                });

                this.csvPersons = [];
                _.each(this.persons, (person) => {
                    this.csvPersons.push(this.ConfigService.parsePersonData(person));
                });

                this.csvHeader = this.ConfigService.getCSVHeader();

                this.setPersonFromRoute();
            });
        } else {
            this.setPersonFromRoute();
        }
    }

    setPersonFromRoute() {
        if (this.personId && this.personId != "") {
            this.person = _.find(this.persons, (item) => {
                return item.id == this.personId;
            });

            if (this.person) {
                this.currentIndex = _.findIndex(this.persons, (item) => {
                    return this.personId == item.id;
                });

                this.setPerson(this.person);
            }
        }
    }

    sort(sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * handle selecting a person
     */
    setPerson(person) {
        this.person = person;
        this.personId = person.id;

        this.$location.path("campaign/" + this.campaignId + "/person/" + this.personId);
        this.getPersonResult();
    }

    getPersonResult() {
        this.RestService.getPersonResults(this.person.id).then((result) => {
            this.result = result;

            _.each(this.result.answers, (answer) => {
                if (this.ConfigService.correctOldQuestions()) {
                    if (answer.type == "statement") answer.type = "question";
                }
                if (!answer.choices || answer.choices.length == 0) {
                    answer.choices = this.getDefaultChoices(answer);
                }
            });
        });
    }

    getDefaultChoices(question) {
        var choices = question.choices;
        if (!choices || choices.length == 0) {
            if (question.type == "statement") {
                choices = ['Eens', 'Oneens'];
            } else if (question.type == "question") {
                choices = ['Goed', 'Fout'];
            }
        }
        return choices;
    }

    goNext() {
        this.currentIndex = _.findIndex(this.persons, (item) => {
            return this.personId == item.id;
        });

        if (this.currentIndex < this.persons.length - 1) {
            this.currentIndex++;
            this.person = this.persons[this.currentIndex];
            this.setPerson(this.person);
        }
    }

    goPrevious() {
        this.currentIndex = _.findIndex(this.persons, (item) => {
            return this.personId == item.id;
        });

        if (this.currentIndex > 0) {
            this.currentIndex--;
            this.person = this.persons[this.currentIndex];
            this.setPerson(this.person);
        }
    }
}
