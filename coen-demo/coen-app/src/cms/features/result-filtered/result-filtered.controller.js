export default class ResultFilteredController {

    constructor($scope, $location, $routeParams, RestService, ConfigService) {
        this.$location = $location;

        this.RestService = RestService;

        this.showAge = ConfigService.checkShowAgeResults();
        this.showCode = ConfigService.checkShowCodeResults();
        this.campaignId = $routeParams.campaignId;

        this.getAllResults();

        $scope.$on('navigate:back', (event, args) => {
            var route = args.route;
            if (route == 'age' || route == 'code') {
                this.$location.path("campaign/" + this.campaignId + "/menu/result");
            }
        });

        $scope.$watch('$ctrl.selected', () => {
            this.setSelectedType();
        });
    }

    setSelectedType() {
        if (this.selected) {
            this.typeGroup = this.results[this.selected];
        }
    }

    getAllResults() {

        this.RestService.getCampaignResults(this.campaignId).then((scores) => {
            this.surveys = _.filter(scores, (item) => {
                return item !== null && item !== "null";
            });

            // handle old results without campaignId
            if (this.campaignId == Config.campaignId) {
                this.RestService.getResults().then((oldScores) => {
                    _.each(oldScores, (item) => {
                        if (item && item.campaignId == null) {
                            item.campaignId = this.campaignId;
                            this.surveys.push(item);
                        }
                    });

                    this.getAllPersons();
                });

            } else {
                this.getAllPersons();
            }
        });
    };

    getAllPersons() {
        this.RestService.getPersons().then((scores) => {
            this.persons = _.filter(scores, (item) => {
                return item !== null && item !== "null";
            });

            this.setResults();
        });
    };


    /**
     * set the results
     */
    setResults() {
        this.results = {};

        let scoreObject = this.getScoreObject();
        let firstType;
        _.each(scoreObject, (typeGroupObject, type) => {
            if (!firstType) firstType = type;

            let results = [];
            _.each(typeGroupObject, (questions, index) => {

                if (questions && questions.length > 0) {
                    let firstQuestion = questions[0];
                    let answerObject = this.getAnswerObject(questions);
                    let answers = this.getAnswers(answerObject, questions);

                    let correctQuestions = _.filter(questions, (question) => {
                        if (!question.answer) {
                            return question.correct == true;
                        } else {
                            return question.answer == 0; // statement agree is 0
                        }
                    });

                    let correctCount = correctQuestions ? correctQuestions.length : 0;
                    let userPercentage = (correctCount / questions.length) * 90;

                    let scoreObj = {
                        question: firstQuestion.text, type: firstQuestion.type,
                        questions: questions, userCount: questions.length, answers: answers,
                        correctCount: correctCount, userPercentage: userPercentage
                    };

                    results.push(scoreObj);

                    this.results[type] = {
                        index: index,
                        type: type,
                        results: results,
                        userCount: questions.length
                    };
                }
            });
        });

        if (firstType) {
            this.selected = firstType;
        }
    };

    getScoreObject() {
        let scoreObject = {};
        _.each(this.surveys, (survey) => {
            var person = _.find(this.persons, (item) => {
                return item.id == survey.personId;
            });

            if (this.showAge) this.setAgeScore(survey, person, scoreObject);
            if (this.showCode) this.setCodeScore(survey, person, scoreObject);
        });
        return scoreObject;
    }

    setAgeScore (survey, person, scoreObject) {
        if (person && person.age > 0) {
            let ageGroup = person.age > 50 ? "50" : person.age > 25 ? "25" : "0";

            _.each(survey.answers, (answer) => {
                if (!scoreObject[ageGroup]) scoreObject[ageGroup] = {};
                var questions = scoreObject[ageGroup][answer.questionId] ? scoreObject[ageGroup][answer.questionId] : [];
                questions.push(answer);
                scoreObject[ageGroup][answer.questionId] = questions;
            });
        }
    }

    setCodeScore (survey, person, scoreObject) {
        if (person.code) {
            let codeGroup = person.code.toUpperCase();
            _.each(survey.answers, (answer) => {
                if (!scoreObject[codeGroup]) scoreObject[codeGroup] = {};
                var questions = scoreObject[codeGroup][answer.questionId] ? scoreObject[codeGroup][answer.questionId] : [];
                questions.push(answer);
                scoreObject[codeGroup][answer.questionId] = questions;
            });
        }
    }

    getAnswerObject(questions) {
        let answerObject = {};
        let answerCount = 0;
        let firstQuestion = questions[0];
        let choices = this.getDefaultChoices(firstQuestion);

        _.each(questions, (question) => {
            let givenAnswer = 0;

            if (question.answer) {
                givenAnswer = question.answer;
            } else { // for questions without answer the given answer is 1 when CORRECT and 0 when INCORRECT
                givenAnswer = question.correct == true ? 0 : 1;
            }

            let title = choices[givenAnswer] ? choices[givenAnswer] : "Waar";
            answerCount = answerObject[givenAnswer] ? answerObject[givenAnswer].answerCount : 0;
            answerCount++;
            answerObject[givenAnswer] = {answerCount: answerCount, title: title};
        });

        return answerObject;
    }

    getAnswers(answerObject, questions) {
        let answers = [];
        _.each(answerObject, (answer) => {
            var answerPercentage = (answer.answerCount / questions.length) * 100;
            answer.answerPercentage = answerPercentage > 70 ? answerPercentage - 30 : answerPercentage;
            answers.push(answer);
        });
        return answers;
    }

    getDefaultChoices(firstQuestion) {
        var choices = firstQuestion.choices;
        if (!choices || choices.length == 0) {
            if (firstQuestion.type == "statement") {
                choices = ['Eens', 'Oneens'];
            } else if (firstQuestion.type == "question") {
                choices = ['Goed', 'Fout'];
            }
        }
        return choices;
    };
}
