import angular from 'angular';

import template from './result-filtered.html';
import controller from './result-filtered.controller';

var resultFilteredModule = angular.module('cms.features.result.filtered', []);

resultFilteredModule.component('resultFilteredFeature', {
    template: template,
    controller: controller,
    bindings: {
        filter: '@?'
    }
});

export default resultFilteredModule;
