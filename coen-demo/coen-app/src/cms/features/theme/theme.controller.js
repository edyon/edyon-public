export default class ScoreController {

    constructor($scope, $rootScope, $routeParams, $location, RestService, DataService) {
        this.$location = $location;
        this.$rootScope = $rootScope;

        this.RestService = RestService;

        this.themes = [];
        this.theme = null;

        this.campaignId = $routeParams.campaignId;
        this.getThemes();

        $scope.$on('navigate:add', (event, args) => {
            var route = args.route;
            if (route == 'theme') {
                this.theme = {};
                this.$rootScope.$broadcast('show:save');
            }
        });

        $scope.$on('navigate:save', (event, args) => {
            var route = args.route;
            if (route == 'theme') {
                this.goSave();
            }
        });

        $scope.$on('navigate:back', (event, args) => {
            var route = args.route;
            if (route == 'theme') {
                if (this.theme) {
                    this.theme = null;
                } else {
                    this.$location.path("campaign/" + this.campaignId + "/menu/campaign");
                }
            }
        });
    }

    getThemes() {
        this.RestService.getThemes().then((themes) => {
            this.themes = themes;
        });
    };

    editTheme(index) {
        this.theme = this.themes[index];
    };

    deleteTheme(index) {
        var theme = this.themes[index];
        if (theme.id && theme.id != "") {
            this.RestService.deleteTheme(theme).then(() => {
                this.getThemes();
            });
        }
    };

    goSave() {
        if (this.theme.id && this.theme.id != "") {
            this.RestService.updateTheme(this.theme).then(() => {
                this.getThemes();
            });
        } else {
            this.RestService.insertTheme(this.theme).then(() => {
                this.getThemes();
            });
        }
    };
}
