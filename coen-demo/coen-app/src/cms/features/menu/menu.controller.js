export default class MenuController {

    constructor($scope, $location, $routeParams, ConfigService) {
        this.$location = $location;

        this.campaignId = $routeParams.campaignId;
        this.menuId = $routeParams.menuId;
        this.showSubmenu = (this.menuId == "result");
        this.showAgeMenu = ConfigService.checkShowAgeResults();
        this.showCodeMenu = ConfigService.checkShowCodeResults();
        this.showScoreMenu = ConfigService.checkShowScoreResults();

        $scope.$on('navigate', (event, args) => {
            this.route = args;
            this.menuId = $routeParams.menuId;
            this.showSubmenu = (this.menuId == "result");
        });

        $scope.$on('navigate:back', (event, args) => {
            var route = args.route;
            if (route == 'menu') {
                if (this.menuId == "campaign") {
                    this.$location.path("campaign");
                } else {
                    this.$location.path("campaign/" + this.campaignId + "/menu/campaign");
                }
            }
        });
    }

    selectMenu (menu) {
        this.$location.path("campaign/" + this.campaignId + "/" + menu);
    };
}
