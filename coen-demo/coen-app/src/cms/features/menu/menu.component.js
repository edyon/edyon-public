import angular from 'angular';

import template from './menu.html';
import controller from './menu.controller';
import style from './menu.scss';

var menuModule = angular.module('cms.features.menu', []);

menuModule.component('menuFeature', {
    template: template,
    controller: controller
});

export default menuModule;
