export default class ResultController {

    constructor($scope, $location, $routeParams, RestService, ConfigService) {
        this.$location = $location;

        this.RestService = RestService;
        this.ConfigService = ConfigService;

        this.campaignId = $routeParams.campaignId;
        this.getAllResults();

        $scope.$on('navigate:back', (event, args) => {
            var route = args.route;
            if (route == 'result') {
                this.$location.path("campaign/" + this.campaignId + "/menu/result");
            }
        });
    }

    getAllResults() {
        this.RestService.getPersons().then((persons) => {
            this.persons = persons;

            this.RestService.getCampaignResults(this.campaignId).then((scores) => {
                this.surveys = _.filter(scores, (item) => {
                    return item !== null && item !== "null";
                });

                // handle old results without campaignId
                if (this.campaignId == Config.campaignId) {
                    this.RestService.getResults().then((oldScores) => {
                        _.each(oldScores, (item) => {
                            if (item && item.campaignId == null) {
                                item.campaignId = this.campaignId;
                                this.surveys.push(item);
                            }
                        });

                        this.setResults();
                    });

                } else {
                    this.setResults();
                }
            });
        });
    };

    /**
     * set the results
     */
    setResults() {
        this.results = [];
        this.questionObj = {};

        let scoreObject = this.getScoreObject();
        _.each(scoreObject, (questions) => {
            if (questions && questions.length > 0) {
                let firstQuestion = questions[0];
                let answerObject = this.getAnswerObject(questions);
                let answers = this.getAnswers(answerObject, questions);

                let correctQuestions = _.filter(questions, (question) => {
                    if (!question.answer) {
                        return question.correct == true;
                    } else {
                        return question.answer == 0; // statement agree is 0
                    }
                });

                let correctCount = correctQuestions ? correctQuestions.length : 0;
                let userPercentage = (correctCount / questions.length) * 100;

                let scoreObj = {
                    question: firstQuestion.text, type: firstQuestion.type,
                    questions: questions, userCount: questions.length, answers: answers,
                    correctCount: correctCount, userPercentage: userPercentage - 10
                };

                this.results.push(scoreObj);
            }
        });
// console.log("parsed results: ", this.results);
        this.setCSVResults();
    };

    getScoreObject() {
        var scoreObject = {};
        _.each(this.surveys, (surveys) => {
            _.each(surveys.answers, (answer) => {
                let questions = scoreObject[answer.questionId] ? scoreObject[answer.questionId] : [];
                questions.push(answer);
                scoreObject[answer.questionId] = questions;
                this.questionObj[answer.questionId] = true;
            });
        });
        return scoreObject;
    }

    getAnswerObject(questions) {
        let answerObject = {};
        let answerCount = 0;
        let firstQuestion = questions[0];
        let choices = this.getDefaultChoices(firstQuestion);
        _.each(questions, (question) => {
            let givenAnswer = 0;
            if (question.answer) {
                givenAnswer = question.answer;
            } else { // for statements without answer the given answer is 1 when CORRECT and 0 when INCORRECT
                givenAnswer = question.correct == true ? 0 : 1;
            }
            var title = choices[givenAnswer] ? choices[givenAnswer] : "Waar";

            answerCount = answerObject[givenAnswer] ? answerObject[givenAnswer].answerCount : 0;
            answerCount++;
            answerObject[givenAnswer] = {answerCount: answerCount, title: title};
        });
        return answerObject;
    }

    getAnswers(answerObject, questions) {
        let answers = [];
        _.each(answerObject, (answer) => {
            let answerPercentage = (answer.answerCount / questions.length) * 100;
            answer.answerPercentage = answerPercentage > 80 ? answerPercentage - 20 : answerPercentage;
            answers.push(answer);
        });
        return answers;
    }

    getDefaultChoices(firstQuestion) {
        var choices = firstQuestion.choices;
        if (!choices || choices.length == 0) {
            if (firstQuestion.type == "statement") {
                choices = ['Eens', 'Oneens'];
            } else if (firstQuestion.type == "question") {
                choices = ['Goed', 'Fout'];
            }
        }
        return choices;
    };

    /**
     * set results for csv download
     */
    setCSVResults() {
        this.csvResults = [];
        let answerHeader = [];
        let index = 1;
        _.each(this.questionObj, (value, key) => {
            answerHeader.push("Vraag " + index);
            answerHeader.push("Antwoord " + index);
            index++;
        });

        let csvHeader = this.ConfigService.getCSVHeader();
        this.csvHeader = csvHeader.concat(answerHeader);

        _.each(this.surveys, (survey) => {
            let surveyObj = {};
            surveyObj["Datum"] = new Date(survey.creationDate);
            let person = _.find(this.persons, (item) => {
                return item.id == survey.personId;
            });
            surveyObj = this.ConfigService.parsePersonData(surveyObj, person);

            let index = 1;
            _.each(this.questionObj, (value, key) => {
                let question = _.find(survey.answers, (answer) => {
                    return answer.questionId == key;
                });

                if (question) {
                    surveyObj["Vraag " + index] = question.text;
                    if (question.type == "question" || question.type == "multiple") {
                        surveyObj["Antwoord " + index] = question.choices[question.answer];
                    } else if (question.type == "statement") {
                        var choices = ["Eens", "Oneens"];
                        surveyObj["Antwoord " + index] = choices[question.answer];
                    } else if (question.type == "open") {
                        surveyObj["Antwoord " + index] = question.answer || question.submittedAnswer;
                    }
                } else {
                    surveyObj["Vraag " + index] = "onbekend";
                    surveyObj["Antwoord " + index] = "onbekend";
                }

                index++;
            });
            this.csvResults.push(surveyObj);
        });
    }
}
