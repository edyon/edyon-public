import angular from 'angular';

import angularAnimate from 'angular-animate';
import angularAria from 'angular-aria';
import angularCookies from 'angular-cookies';
import angularResource from 'angular-resource';
import angularRoute from 'angular-route';
import angularSanitize from 'angular-sanitize';
import angularTranslate from 'angular-translate';
import angularTranslateLoaderPartial from 'angular-translate-loader-partial';
import angularBootstrap from 'angular-ui-bootstrap/ui-bootstrap';

import angularCSV from 'ng-csv/build/ng-csv';
import angularUpload from 'angular-file-upload';

import components from './components/components.module';
import features from './features/features.module';
import services from './services/services.module';
import plugins from './plugins/plugins.module';
import template from './module.html';
import style from './module.scss';

const cmsModule = angular.module('app', [
    // 'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngCsv',
    'pascalprecht.translate',
    'ui.bootstrap',
    'angularFileUpload',
    components.name,
    services.name,
    plugins.name,
    features.name

]).component('coenApp', {
    template: template
});

cmsModule.constant('appConstants', {
    services: {
        endpoint: 'http://api.coen.scuro.nl/'
    },
    defaultTimeout: 7000
});

cmsModule.config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', '$translateProvider', '$translatePartialLoaderProvider',
    ($routeProvider, $locationProvider, $sceDelegateProvider, $translateProvider, $translatePartialLoaderProvider) => {

        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            'http://api.coen.scuro.nl/*'
        ]);

        $routeProvider
            .when('/login', {
                template: '<login-feature></login-feature>',
                name: 'login'
            })
            .when('/campaign', {
                template: '<campaign-feature></campaign-feature>',
                name: 'campaign'
            })
            .when('/campaign/:campaignId', {
                template: '<campaign-feature></campaign-feature>',
                name: 'campaign'
            })
            .when('/campaign/:campaignId/menu/:menuId', {
                template: '<menu-feature></menu-feature>',
                name: 'menu'
            })
            .when('/campaign/:campaignId/result', {
                template: '<result-feature></result-feature>',
                name: 'result'
            })
            .when('/campaign/:campaignId/age', {
                template: '<result-filtered-feature filter="age"></result-filtered-feature>',
                name: 'age'
            })
            .when('/campaign/:campaignId/code', {
                template: '<result-filtered-feature filter="code"></result-filtered-feature>',
                name: 'code'
            })
            .when('/campaign/:campaignId/score', {
                template: '<score-feature></score-feature>',
                name: 'score'
            })
            .when('/campaign/:campaignId/person', {
                template: '<person-feature></person-feature>',
                name: 'person'
            })
            .when('/campaign/:campaignId/person/:personId', {
                template: '<person-feature></person-feature>',
                name: 'person'
            })
            .when('/campaign/:campaignId/statement', {
                template: '<question-feature></question-feature>',
                name: 'statement'
            })
            .when('/campaign/:campaignId/statement/:statementId', {
                template: '<question-feature></question-feature>',
                name: 'statement'
            })
            .when('/campaign/:campaignId/theme', {
                template: '<theme-feature></theme-feature>',
                name: 'theme'
            })
            .when('/campaign/:campaignId/theme/:themeId', {
                template: '<theme-feature></theme-feature>',
                name: 'theme'
            })
            .otherwise({
                redirectTo: '/login'
            });

        $locationProvider.html5Mode(false);

        $translateProvider.useSanitizeValueStrategy('escaped');
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: 'locale/{lang}/{part}.json'
        });

        $translateProvider.preferredLanguage("nl");
        $translatePartialLoaderProvider.addPart("cms");
    }
]);

export default cmsModule;

