export default class NavigateController {

    constructor($scope, $rootScope, $route, $routeParams, ConfigService) {
        this.$rootScope = $rootScope;
        this.$routeParams = $routeParams;

        this.campaignId = $routeParams.campaignId;
        this.menuId = $routeParams.menuId;
        this.statementId = $routeParams.statementId;
        this.personId = $routeParams.personId;

        this.showAddCampaign = ConfigService.checkShowNewCampaignButton();

        $scope.$on('navigate', (event, args) => {
            this.route = $route.current.$$route.name;
            this.handleNavigate(args);
        });

        $scope.$on('show:save', () => {
            this.showAddButton = false;
            this.showSaveButton = true;
        });
    }

    handleNavigate(args) {
        this.route = args;

        this.campaignId = this.$routeParams.campaignId;
        this.menuId = this.$routeParams.menuId;
        this.statementId = this.$routeParams.statementId;
        this.themeId = this.$routeParams.themeId;
        this.personId = this.$routeParams.personId;

        this.showBackButton = (this.route != 'login');
        this.showAddButton = (this.route == 'campaign' && !this.campaignId) && this.showAddCampaign
            || (this.route == 'theme' && !this.themeId)
            || (this.route == 'statement' && !this.statementId);
        this.showSaveButton = (this.route == 'campaign' && this.campaignId)
            || (this.route == 'theme' && this.themeId)
            || (this.route == 'statement' && this.statementId);
    }

    /**
     * handle clicking the buttons
     */
    add() {
        this.$rootScope.$broadcast('navigate:add', {route: this.route});
    };

    save() {
        this.$rootScope.$broadcast('navigate:save', {route: this.route});
    };

    back() {
        this.$rootScope.$broadcast('navigate:back', {route: this.route});
    }
}