export default class ThumbController {

    constructor($scope, $element, $window) {
        this.$scope = $scope;
        this.$element = $element;
        this.$window = $window;

        $scope.$watch('$ctrl.imageUrl',() => {
            this.parseUrl();
        });

        $scope.$on('upload:complete', () => {
            this.parseUrl();
        });
    }

    parseUrl () {
        if (this.imageUrl) {
            this.parsedUrl = Config.webUrl + Config.imageUrl + this.imageSize() + "/" + this.imageUrl;
            this.parsedId = "image" + this.imageId;
            this.setBackground();
        }
    }

    setBackground () {
        $(this.$element).find('#image').css('background', 'none');
        $(this.$element).find('#image').css('background', 'url(' + this.parsedUrl + ') no-repeat bottom center');
        $(this.$element).find('#image').css('background-color', '#fff');
        $(this.$element).find('#image').css('background-size', 'cover');
        $(this.$element).find('#image').css('-o-background-size', 'cover');
        $(this.$element).find('#image').css('-moz-background-size', 'cover');
        $(this.$element).find('#image').css('-webkit-background-size', 'cover');
        $(this.$element).find('#image').css('background-position', 'center bottom');
    };

    imageSize () {
        var windowWidth = $(this.$window).width();
        if (windowWidth <= 320) {
            return 480;
        } else if (windowWidth <= 480) {
            return 768;
        } else if (windowWidth <= 768) {
            return 992;
        } else if (windowWidth <= 992) {
            return 1200;
        } else {
            return 1200;
        }
    }
}