export default class appController {

    constructor($scope, $rootScope, $route, $routeParams, $window) {
        this.menu = null;

        $rootScope.$on('$routeChangeStart', function (evt, absNewUrl, absOldUrl) {
            if ($route.current) {
//console.log("start = ", $route.current.$$route, $routeParams);
            }
            $window.scrollTo(0, 0);
        });

        $rootScope.$on('$routeChangeSuccess', function () {
//console.log("success = ", $route, $routeParams);
            if ($route.current && $route.current.$$route) {
                var routeParam = $route.current.$$route.name;
//console.log("go select menu from route = ", routeParam);
                $rootScope.$broadcast('navigate', routeParam);
            }
        });


        $scope.$on('menu:select', function (event, data) {
            $scope.menu = data.menu;
//console.log("app selected menu = ", $scope.menu);
        });
    }
}