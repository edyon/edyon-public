import angular from 'angular';

import configService from './config/config.service';
import dataService from './data/data.service';
import answerService from './rest/rest.service';

const servicesModule = angular.module('cms.services', [
    'cms.services.config',
    'cms.services.data',
    'cms.services.rest',
]);

export default servicesModule;
