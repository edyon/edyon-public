class ConfigService {

    constructor($rootScope) {
        this._client = Config.client;
    }

    checkShowNewCampaignButton() {
        return this._client == "coen" || this._client == "roc";
    };

    checkShowAgeResults() {
        return this._client == "coen";
    };

    checkShowCodeResults() {
        return this._client == "spov";
    };

    checkShowScoreResults() {
        return this._client == "hardenberg";
    };

    checkShowCoen() {
        return this._client == "coen" || this._client == "roc" || this._client == "hardenberg";
    };

    checkShowBeeldenstorm() {
        return this._client == "spov";
    };

    getCSVHeader() {
        let csvHeader = [];
        let defaultHeader = ['Naam', 'Email', 'Leeftijd', 'Geslacht'];
        let schoolHeader = this._client == "roc" ? ['Opleiding', 'Niveau', 'Jaar'] : [];
        return csvHeader.concat(defaultHeader).concat(schoolHeader);
    };

    parsePersonData(person) {
        let personObj = {};
        if (person && this._client == "roc") {
            personObj["Naam"] = person.name || "onbekend";
            personObj["Email"] = person.email || "onbekend";
            personObj["Leeftijd"] = person.age || "onbekend";
            personObj["Geslacht"] = person.gender || "onbekend";
            personObj["Opleiding"] = person.education || "onbekend";
            personObj["Niveau"] = person.level || "onbekend";
            personObj["Jaar"] = person.year || "onbekend";
        } else if (person) {
            personObj["Naam"] = person.name || "onbekend";
            personObj["Email"] = person.email || "onbekend";
            personObj["Leeftijd"] = person.age || "onbekend";
            personObj["Geslacht"] = person.gender || "onbekend";
        } else {
            personObj["Naam"] = "onbekend";
            personObj["Email"] = "onbekend";
            personObj["Leeftijd"] = "onbekend";
            personObj["Geslacht"] = "onbekend";
        }
        return personObj;
    };

    correctOldQuestions() {
        return this._client == "hardenberg";
    };


    static ConfigFactory($rootScope) {
        return new ConfigService($rootScope);
    }
}

ConfigService.ConfigFactory.$inject = ['$rootScope'];

angular.module('cms.services.config', [])
    .factory('ConfigService', ConfigService.ConfigFactory);

export default ConfigService;
