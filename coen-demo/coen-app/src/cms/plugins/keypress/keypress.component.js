import angular from 'angular';

class KeypressController {

    constructor($document, $rootScope) {

        $document.bind('keypress', (e) => {
console.log('Got keypress:', e.which, ' || ', e.keyCode);

            if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || e.keyCode == 13) {
                e.preventDefault();

                if (e.keyCode == 39) { // right key pressed
                    $rootScope.$broadcast('keypress:right-key', {keyCode: e.keyCode});
                } else if (e.keyCode == 37) { // left key pressed
                    $rootScope.$broadcast('keypress:left-key', {keyCode: e.keyCode});
                } else if (e.keyCode == 40) { // right key pressed
                    $rootScope.$broadcast('keypress:up-key', {keyCode: e.keyCode});
                } else if (e.keyCode == 38) { // left key pressed
                    $rootScope.$broadcast('keypress:down-key', {keyCode: e.keyCode});
                } else if (e.keyCode == 13) {
                    $rootScope.$broadcast('keypress:enter', {keyCode: e.keyCode});
                }
            }
        });
    }
}

var keypressModule = angular.module('coen.plugins.keypress', []);

keypressModule.component('keypressEvents', {
    controller: KeypressController
});

export default keypressModule;