import angular from 'angular';

import fonts from './cms-font-library/cms-font-library.component';
import iconFont from './icon-font/icon-font.component';
import keypress from './keypress/keypress.component';


const PluginsModule = angular.module('cms.plugins', [
    fonts.name,
    iconFont.name,
    keypress.name
]);

export default PluginsModule;
