import angular from 'angular';

import template from './icon-font.template.html';

var iconFontModule = angular.module('cms.plugins.icon', []);

class IconFontController {

    constructor($scope) {
        $scope.$watch('$ctrl.iconClass', () => {
            this.iconClass = this.iconClass || "";
            this.iconLink = "#" + this.iconClass;
        });
    }
}

iconFontModule.component('iconFont', {
    template: template,
    controller: IconFontController,
    bindings: {
        iconClass: "@"
    }
});

export default iconFontModule;
