import angular from 'angular';

import template from './cms-font-library.template.html';

var cmsFontModule = angular.module('cms.plugins.fonts', []);

cmsFontModule.component('cmsFontLibrary', {
    template: template
});

export default cmsFontModule;