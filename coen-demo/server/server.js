import bodyParser from 'body-parser';
import compression from 'compression';
import express from 'express';
import path from 'path';

const app = express();
const PORT = process.env.E2E_PORT || process.env.PORT || 8000;

app.use(compression());
app.use(express.static(path.join(__dirname, '../')));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded

const server = app.listen(PORT, () => {
	const host = server.address().address;
	const port = process.env.E2E_PORT || server.address().port;
	
	console.log('Server started at http://' + host + ':' + port);
});
