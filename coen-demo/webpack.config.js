var path = require('path');
var webpack = require('webpack');

/**
 * @param options The build configuration.
 * @param options.app Build demo app.
 * @param options.istanbul Build with instanbul instrumentation.
 * @param options.lib Build library.
 * @param options.lite Build without Angular.
 * @param options.min Build minified version.
 * @param options.prod Build production version (with accurate sourcemaps).
 */
function createConfig(options) {

	options = options || {lib: true};

	/*
	 * SHARED
	 */
	var config = {
		devtool: options.prod ? 'hidden-source-map' : 'cheap-module-source-map',
		context: path.join(__dirname, ''),
		entry: {},
		output: {
			path: path.join(__dirname, './dist/js')
		},
		externals: {},
		module: {
			loaders: [
				{
					test: /\.html$/,
					loader: 'raw'
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					loader: 'babel',
					query: {
						babelrc: false,
						cacheDirectory: true,
						presets: ['es2015-native-modules']
					}
				},
                {
                    test: /\.(jpe?g|png|gif)$/i,
                    loader: 'url-loader?name=js/[name].[ext]'
                },
                {
                    test: /\.svg$/,
                    loader: 'svg-url-loader'
                }
			]
		},
		plugins: [
			new webpack.LoaderOptionsPlugin({
				minimize: options.min,
				debug: !options.min
			}),
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: options.prod ? '"production"' : '"development"'
				}
		  	}),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            }),
            new webpack.ProvidePlugin({
                _: "underscore"
            })
		]
	};

	/*
	 * APP
	 */
	if (options.app) {
		config.output.filename = 'default.js';
		config.entry.js = './coen-app/src/app/module.js';
		config.module.preLoaders = [
			{ test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap", "themed-sass"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
		];
	}

    if (options.prod) {
        config.output.filename = 'default.js';
        config.entry.js = './coen-app/src/app/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css", "sass?sourceMap", "themed-sass"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    if (options.hardenberg) {
        config.output.filename = 'default.js';
        config.entry.js = './coen-app/src/app/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap", "themed-sass?theme=hardenberg"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    if (options.roc) {
        config.output.filename = 'default.js';
        config.entry.js = './coen-app/src/app/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap", "themed-sass?theme=roc"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    if (options.verkiezingen) {
        config.output.filename = 'default.js';
        config.entry.js = './coen-app/src/app/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap", "themed-sass?theme=verkiezingen"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    if (options.spov) {
        config.output.filename = 'default.js';
        config.entry.js = './coen-app/src/app/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap", "themed-sass?theme=spov"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    if (options.spovProd) {
        config.output.filename = 'default.js';
        config.entry.js = './coen-app/src/app/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css", "sass?sourceMap", "themed-sass?theme=spov"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    /*
     * CMS
     */
    if (options.cms) {
        config.output.filename = 'default-cms.js';
        config.entry.js = './coen-app/src/cms/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

    if (options.cmsProd) {
        config.output.filename = 'default-cms.js';
        config.entry.js = './coen-app/src/cms/module.js';
        config.module.preLoaders = [
            { test: /\.scss$/, loaders: ["style", "css", "sass?sourceMap"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

	/*
	 * ISTANBUL
	 */
	if (options.istanbul) {
        config.output.filename = config.output.filename.replace('.js', '-istanbul.js');
		config.module.preLoaders = [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-istanbul' },
            { test: /\.scss$/, loaders: ["style", "css?sourceMap", "sass?sourceMap"]},
            { test: /\.css$/, loaders: ["style", "css?sourceMap"]}
        ];
    }

	/*
	 * MINIFICATION
	 */
	if (options.min) {
		config.output.filename = config.output.filename.replace('.js', '.min.js');
		config.plugins.push(
			new webpack.optimize.UglifyJsPlugin({
				compress: {
					warnings: false
				},
				output: {
					comments: false
				}
			})
		);
	}

	return config;
}

module.exports = createConfig;
