import gulp from 'gulp';
import util from 'gulp-util';

/*
 ** Run App
 */
gulp.task('run:verkiezingen', ['verkiezingen:synced'], function () {
    gulp.watch(['coen-app/src/**'], ['clean:js', 'js:build:verkiezingen', 'js:build:cms']);
    gulp.watch(['coen-app/index/verkiezingen.html', 'coen-app/index/verkiezingen-cms.html'], ['html:verkiezingen']);
    gulp.watch(['locale/nl/verkiezingen.json'], ['copy:translations:verkiezingen']);
});

/*
 ** Build App
 */
gulp.task('verkiezingen:synced', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('verkiezingen:build');
});

gulp.task('verkiezingen:build', ['html:verkiezingen', 'js:build:verkiezingen', 'js:build:cms'], function () {
    util.log('--> build task executed.');
    gulp.start('verkiezingen:copy');
});

gulp.task('verkiezingen:copy', ['copy', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
    gulp.start('verkiezingen:run');
});

gulp.task('verkiezingen:run', ['connect'], function () {
    util.log('--> connect task executed.');
});

/*
 ** Build App Release (called from Jenkins)
 */
gulp.task('verkiezingen:release', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('verkiezingen:build:prod');
});

gulp.task('verkiezingen:build:prod', ['html:verkiezingen', 'js:build:verkiezingen:prod', 'js:build:cms:prod'], function () {
    util.log('--> build task executed.');
    gulp.start('verkiezingen:copy:prod');
});

gulp.task('verkiezingen:copy:prod', ['copy', 'copy:translations:verkiezingen', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
});