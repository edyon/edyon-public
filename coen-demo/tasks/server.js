var gulp = require('gulp'),
	nodemon = require('gulp-nodemon'),
	plugins = require('gulp-load-plugins')();

gulp.task('server', function () {
	nodemon({
		script: 'mocks/server.js',
		ext: 'js html',
		env: { 'NODE_ENV': 'development' }
	})
})