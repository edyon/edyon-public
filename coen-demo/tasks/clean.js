import gulp from 'gulp';
import del from 'del';

gulp.task('clean', ['clean:all']);

gulp.task('clean:all', function () {
	del.sync(['dist/*'], { read: false });
});

gulp.task('clean:js', function () {
	del.sync(['dist/js/*'], { read: false });
});

gulp.task('clean:style', function () {
	del.sync(['dist/css/*'], { read: false });
});
