import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import webpack from 'webpack';

const $ = gulpLoadPlugins({
    pattern: ['gulp-*', 'webpack-stream'],
    rename: {
        'webpack-stream': 'webpack'
    }
});

/**
 * Build dev and app
 * @returns {Stream} Gulp stream
 */
gulp.task('js:dev', ['js:build:app', 'js:build:cms']);
gulp.task('js:prod', ['js:build:prod', 'js:build:prod:min']);

/**
 * Build dev version
 * @returns {Stream} Gulp stream
 */
gulp.task('js:build:app', () => {
    return build({app: true});
});

gulp.task('js:build:prod', () => {
    return build({prod: true, min:false});
});


gulp.task('js:build:hardenberg', () => {
    return build({hardenberg: true});
});

gulp.task('js:build:hardenberg:min', () => {
    return build({hardenberg: true, min: true});
});

gulp.task('js:build:roc', () => {
    return build({roc: true});
});

gulp.task('js:build:verkiezingen', () => {
    return build({verkiezingen: true});
});

gulp.task('js:build:spov', () => {
    return build({spov: true});
});

gulp.task('js:build:spov:min', () => {
    return build({spov: true, min: false});
});

gulp.task('js:build:spov:prod', () => {
    return build({spovProd: true, min: false});
});

gulp.task('js:build:cms', () => {
    return build({cms: true});
});

gulp.task('js:build:cms:min', () => {
    return build({cms: true, min: false});
});

gulp.task('js:build:cms:prod', () => {
    return build({cmsProd: true, min: false});
});

/**
 * Build js with instanbul instrumentation.
 * @returns {Stream} Gulp stream
 */
gulp.task('js:build:istanbul', () => {
    return build({spov: true, istanbul: true});
});

/**
 * Build js with instanbul instrumentation for local.
 * @returns {Stream} Gulp stream
 */
gulp.task('js:build:test', () => {
    return build({spov: true, istanbul: true});
});

function build(config) {
    const webpackConfig = require('../webpack.config')(config),
        filter = $.filter(['**/*.js'], {restore: true});
    return $.webpack(webpackConfig, webpack)
        .pipe(filter)
        .pipe($.insert.prepend(`/* Build Time: ${new Date()} */\n`))
        .pipe(filter.restore)
        .pipe(gulp.dest('dist/js'));
}
