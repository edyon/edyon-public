import gulp from 'gulp';
import util from 'gulp-util';

/*
 ** Gulp Run App
 */
gulp.task('run', ['development:synced'], function () {
	gulp.watch(['coen-app/src/**'], ['clean:js', 'js:build:app', 'js:build:cms', 'js:build:istanbul']);
	gulp.watch(['coen-app/index.html', 'coen-app/cms.html'], ['html']);
	gulp.watch(['locale/**/*.json'], ['copy']);
});

/*
 ** Gulp Run App without tests
 */
gulp.task('run:app', ['development:synced'], function () {
    gulp.watch(['coen-app/src/**'], ['clean:js', 'js:build:app', 'js:build:cms']);
    gulp.watch(['coen-app/index.html'], ['html']);
    gulp.watch(['locale/**/*.json'], ['copy']);
});

/*
 ** Gulp Run CMS
 */
gulp.task('run:cms', ['development:synced'], function () {
    gulp.watch(['coen-app/src/**'], ['clean:js', 'js:build:cms']);
    gulp.watch(['coen-app/cms.html'], ['html']);
    gulp.watch(['locale/nl/cms.json'], ['copy:translations:cms']);
});

/*
 ** Build App
 */
gulp.task('development:synced', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('development:build');
});

gulp.task('development:build', ['html', 'js:build:app', 'js:build:cms', 'js:build:istanbul'], function () {
    util.log('--> build task executed.');
    gulp.start('development:copy');
});

gulp.task('development:copy', ['copy', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
    gulp.start('development:run');
});

gulp.task('development:run', ['connect'], function () {
    util.log('--> connect task executed.');
});

/*
 ** Build App Release (called from Jenkins)
 */
gulp.task('coen:release', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('coen:build:prod');
});

gulp.task('coen:build:prod', ['html:coen', 'js:build:prod', 'js:build:cms:prod'], function () {
    util.log('--> build task executed.');
    gulp.start('coen:copy:prod');
});

gulp.task('coen:copy:prod', ['copy', 'copy:translations:coen', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
});

/*
 ** Gulp Run for testing
 */
gulp.task('run:test', ['development:test'], function () {
    gulp.watch(['coen-app/src/**'], ['development:test']);
    gulp.watch(['locale/**/*.json'], ['copy']);
});

/*
 ** Build App for testing istanbul
 */
gulp.task('development:test', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('development:build:test');
});

gulp.task('development:build:test', ['js:build:istanbul', 'copy:testLib'], function () {
    util.log('--> build istanbul task executed.');
});