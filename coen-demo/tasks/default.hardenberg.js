import gulp from 'gulp';
import util from 'gulp-util';

/*
 ** Run App
 */
gulp.task('run:hardenberg', ['hardenberg:synced'], function () {
    gulp.watch(['coen-app/src/**'], ['clean:js', 'js:build:hardenberg', 'js:build:cms']);
    gulp.watch(['coen-app/index/hardenberg.html', 'coen-app/index/hardenberg-cms.html'], ['html:hardenberg']);
    gulp.watch(['locale/nl/hardenberg.json'], ['copy:translations:hardenberg']);
});

/*
 ** Build App
 */
gulp.task('hardenberg:synced', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('hardenberg:build');
});

gulp.task('hardenberg:build', ['html:hardenberg', 'js:build:hardenberg', 'js:build:cms'], function () {
    util.log('--> build task executed.');
    gulp.start('hardenberg:copy');
});

gulp.task('hardenberg:copy', ['copy', 'copy:images:profiles', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
    gulp.start('hardenberg:run');
});

gulp.task('hardenberg:run', ['connect'], function () {
    util.log('--> connect task executed.');
});

/*
 ** Build App Release (called from Jenkins)
 */
gulp.task('hardenberg:release', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('hardenberg:build:prod');
});

gulp.task('hardenberg:build:prod', ['html:hardenberg', 'js:build:hardenberg:prod', 'js:build:cms:prod'], function () {
    util.log('--> build task executed.');
    gulp.start('hardenberg:copy:prod');
});

gulp.task('hardenberg:copy:prod', ['copy', 'copy:translations:hardenberg', 'copy:translations:cms'], function () {
    util.log('--> copy task executed.');
});

/*
 ** Build Mobile App
 */
gulp.task('hardenberg:mobile', ['clean'], function () {
    util.log('--> clean task executed.');
    gulp.start('hardenberg:build:mobile');
});

gulp.task('hardenberg:build:mobile', ['html:hardenberg', 'js:build:hardenberg:min'], function () {
    util.log('--> build task executed.');
    gulp.start('hardenberg:copy:mobile');
});

gulp.task('hardenberg:copy:mobile', ['copy:translations:hardenberg', 'copy:images:profiles'], function () {
    util.log('--> copy task executed.');
    gulp.start('hardenberg:www');
});

gulp.task('hardenberg:www', ['copy:mobile:hardenberg'], function () {
    util.log('--> www task executed.');
});

/*
 ** Copy Mobile App
 */
gulp.task('copy:mobile:spov', ['copy:app:mobile', 'copy:images:spov', 'copy:config:spov']);

gulp.task('copy:config:hardenberg', function () {
    gulp.src(['coen-app/assets/mobile/hardenberg/config.xml'])
        .pipe(gulp.dest('./coen-mobile'));
});

gulp.task('copy:images:hardenberg', function () {
    gulp.src(['coen-app/assets/img/logos/hardenberg/icon.png'])
        .pipe(gulp.dest('./coen-mobile/www'));
    gulp.src(['coen-app/assets/img/mobile/res-hardenberg/**'])
        .pipe(gulp.dest('./coen-mobile/www/img/res'));
    gulp.src(['coen-app/assets/img/profiles/**'])
        .pipe(gulp.dest('./coen-mobile/www/img/profiles'));
});