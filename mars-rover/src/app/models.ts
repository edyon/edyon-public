export interface Route {
  position: Position;
  steps: Array<Action>;
}

export interface DriverInfo {
  driver: number;
  position: Position;
}

export interface Position {
  x?: number;
  y?: number;
  orientation?: Orientation;
  className?: string;
}

export enum Orientation {
  N = 0,
  E = 90,
  S = 180,
  W = 270
}

export enum Action {
  L,
  R,
  M
}
