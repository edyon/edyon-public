import {AfterViewInit, Component, Input} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

import {RouteService} from '../../services/route.service';
import {DriverInfo} from '../../models';

@Component({
  selector: 'app-output',
  templateUrl: './output.template.html',
  styleUrls: ['./_output.css']
})

export class OutputComponent implements AfterViewInit {
  @Input() driver: number;
  private _driverSubscription: Subscription;
  driverInfo: DriverInfo;

  constructor(private _routeService: RouteService) {

  }

  ngAfterViewInit() {
    this._driverSubscription = this._routeService.driverState
      .subscribe(driverInfo => {
console.log("driverInfo ", driverInfo);
        if (this.driver === driverInfo.driver) {
          this.driverInfo = driverInfo;
        }
      });
  }
}
