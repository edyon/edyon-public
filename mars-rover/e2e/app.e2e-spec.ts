import { RoverPage } from './app.po';

describe('rover App', function() {
  let page: RoverPage;

  beforeEach(() => {
    page = new RoverPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
